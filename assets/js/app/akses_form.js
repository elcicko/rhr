$(document).ready(function() {
  $("#form").validate({
      rules: {
          level: "required",
          menu: "required"
      },
      messages: {
          level: "Kolom Level Harus Dipilih",
          menu: "Kolom Menu Harus Dipilih",
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});