$(document).ready(function() {
  $("#form_user").validate({
      rules: {
          username: "required",
          level: "required"
      },
      messages: {
          username: "Kolom Ini Harus Diisi",
          level: "Kolom Ini Harus Diisi"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});