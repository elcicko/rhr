$(document).ready(function() {
  
  // VALIDASI FORM
  $("#form_detail_submain_doc_add").validate({
      rules: {
          kode_detail_submain_doc: "required",
          kode_submain_doc: "required",
          tgl_dokumen: "required",
          no_dokumen: "required",
          jenis_dokumen: "required",
          jml_hal: "required",
          header: "required",
          privasi: "required",
          tgl_awal_po: "required",
          tgl_akhir_po: "required",
          reminder: "required",
          user_kontrak: "required",
          file_dok: "required",
          gudang: "required",
          no_lemari: "required",
          no_rak: "required"
      },
      messages: {
          kode_detail_submain_doc: "Kolom Ini Harus Diisi",
          kode_submain_doc: "Kolom Ini Harus Diisi",
          tgl_dokumen: "Kolom Ini Harus Diisi",
          no_dokumen: "Kolom Ini Harus Diisi",
          jenis_dokumen: "Kolom Ini Harus Dipilih",
          jml_hal: "Kolom Ini Harus Diisi",
          header: "Kolom Ini Harus Diisi",
          privasi: "Kolom Ini Harus Diisi",
          tgl_awal_po: "Kolom Ini Harus Diisi",
          tgl_akhir_po: "Kolom Ini Harus Diisi",
          reminder: "Kolom Ini Harus Diisi",
          user_kontrak: "Kolom Ini Harus Diisi",
          file_dok: "Kolom Ini Harus Diisi",
          gudang: "Kolom Ini Harus Diisi",
          no_lemari: "Kolom Ini Harus Diisi",
          no_rak: "Kolom Ini Harus Diisi"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });

  // VALIDASI FORM
  $("#form_detail_submain_doc_edit").validate({
      rules: {
          kode_detail_submain_doc: "required",
          kode_submain_doc: "required",
          tgl_dokumen: "required",
          no_dokumen: "required",
          jenis_dokumen: "required",
          jml_hal: "required",
          header: "required",
          privasi: "required",
          tgl_awal_po: "required",
          tgl_akhir_po: "required",
          reminder: "required",
          user_kontrak: "required",
          gudang: "required",
          no_lemari: "required",
          no_rak: "required"
      },
      messages: {
          kode_detail_submain_doc: "Kolom Ini Harus Diisi",
          kode_main_doc: "Kolom Ini Harus Diisi",
          tgl_dokumen: "Kolom Ini Harus Diisi",
          no_dokumen: "Kolom Ini Harus Diisi",
          jenis_dokumen: "Kolom Ini Harus Dipilih",
          jml_hal: "Kolom Ini Harus Diisi",
          header: "Kolom Ini Harus Diisi",
          privasi: "Kolom Ini Harus Diisi",
          tgl_awal_po: "Kolom Ini Harus Diisi",
          tgl_akhir_po: "Kolom Ini Harus Diisi",
          reminder: "Kolom Ini Harus Diisi",
          user_kontrak: "Kolom Ini Harus Diisi",
          gudang: "Kolom Ini Harus Diisi",
          no_lemari: "Kolom Ini Harus Diisi",
          no_rak: "Kolom Ini Harus Diisi"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
  
  // DATEPICKER UNTUK TANGGAL
  $( "#tgl_awal_po" ).datepicker({changeMonth: true,changeYear: true});
  $( "#format" ).change(function() { $("#tgl_awal_po").datepicker("option", "dateFormat", $(this).val()); });
  $( "#tgl_akhir_po" ).datepicker({changeMonth: true,changeYear: true});
  $( "#format2" ).change(function() { $("#tgl_akhir_po").datepicker("option", "dateFormat", $(this).val()); });
  $( "#tgl_dokumen" ).datepicker({changeMonth: true,changeYear: true});
  $( "#format3" ).change(function() { $("#tgl_dokumen").datepicker("option", "dateFormat", $(this).val()); });

  // VALIDASI UNTUK FIELD ANGKA
  $('#jml_hal').keyup(function(){this.value=this.value.replace(/[^0-9.]/g,'');});
  $('#reminder').keyup(function(){this.value=this.value.replace(/[^0-9.]/g,'');});
  $('#no_lemari').keyup(function(){this.value=this.value.replace(/[^0-9.]/g,'');});
  $('#no_rak').keyup(function(){this.value=this.value.replace(/[^0-9.]/g,'');});

});