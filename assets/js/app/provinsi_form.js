$(document).ready(function() {
  $("#form").validate({
      rules: {
          nama_provinsi: "required",
          latitude: "required",
          longitude: "required"
      },
      messages: {
          nama_provinsi: "Kolom Nomor Prefix Harus Diisi",
          latitude: "Kolom Garis Lintang Harus Diisi",
          longitude: "Kolom Garis Bujur Harus Diisi"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});