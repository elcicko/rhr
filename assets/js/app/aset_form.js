$(document).ready(function() {
  $("#form").validate({
      rules: {
          kabkota: "required",
          provinsi: "required",
          nama_aset: "required",
          nomor_aset: "required",
          latitude: "required",
          longitude: "required"
      },
      messages: {
          kabkota: "Kolom Kab / Kota Harus Dipilih",
          provinsi: "Kolom Provinsi Harus Dipilih",
          nama_aset: "Kolom Nama Aset Harus Diisi",
          nomor_aset: "Kolom Nomor Aset Harus Diisi",
          latitude: "Kolom Garis Lintang Harus Diisi",
          longitude: "Kolom Garis Bujur Harus Diisi",
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});