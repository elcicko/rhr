$(document).ready(function() {
  $("#form").validate({
      rules: {
          oldpassword: "required",
          password: "required",
          repassword: {
            equalTo: "#password"
          }
      },
      messages: {
          oldpassword: "Kolom Password Lama Harus Diisi",
          password: "Kolom Password Baru Harus Diisi",
          repassword: "Konfirmasi Password Tidak Sama Dengan Password Baru"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});