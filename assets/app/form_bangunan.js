var ajaxku;

function ajaxkota(id) {
    ajaxku = buatajax();
    var url = base_url+"getkabkota";
    url = url + "/" + id;
    ajaxku.onreadystatechange = cityChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function ajaxkecamatan(id) {
    ajaxku = buatajax();
    var url = base_url+"getkecamatanbykota";
    url = url + "/" + id;
    ajaxku.onreadystatechange = districtChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function ajaxkelurahan(id) {
    ajaxku = buatajax();
    var url = base_url+"getkelurahanbykecamatan";
    url = url + "/" + id;
    ajaxku.onreadystatechange = villageChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function buatajax() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  }

  if (window.ActiveXObject) {
    return new ActiveXObject("Microsoft.XMLHTTP");
  }

  return null;
}

function cityChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kota").innerHTML = data
        } else {
            document.getElementById("kota").value = "<option selected>-- Kota / Kabupaten --</option>";
        }
    }
}

function districtChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kecamatan").innerHTML = data
        } else {
            document.getElementById("kecamatan").value = "<option selected>-- Kecamatan --</option>";
        }
    }
}

function villageChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("desa").innerHTML = data
        } else {
            document.getElementById("desa").value = "<option selected>-- Kelurahan / Desa --</option>";
        }
    }
}
$(document).ready(function(){
  $('#tanggal_penilaian').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tahun_selesai_dibangun').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tahun_renovasi').datepicker({format: "yyyy-mm-dd", autoclose: true});

  $('#listrik').on('change', function () {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == "Lainnya") {
            $('#listrik_lainnya').show();
        } else {
            $('#listrik_lainnya').hide();
        }
    });

    $('#air').on('change', function () {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == "Lainnya") {
            $('#air_lainnya').show();
        } else {
            $('#air_lainnya').hide();
        }
    });

    $('#ac').on('change', function () {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == "Lainnya") {
            $('#ac_lainnya').show();
        } else {
            $('#ac_lainnya').hide();
        }
    });

    $('#pembuangan_limbah').on('change', function () {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == "Lainnya") {
            $('#pembuangan_limbah_lainnya').show();
        } else {
            $('#pembuangan_limbah_lainnya').hide();
        }
    });

    $('#pemadam_kebakaran').on('change', function () {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected == "Lainnya") {
            $('#pemadam_kebakaran_lainnya').show();
        } else {
            $('#pemadam_kebakaran_lainnya').hide();
        }
    });
});