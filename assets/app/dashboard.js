var ajaxku;
var map;
var markers;
var marker;
var contentString;

$(document).ready(function(){
    var url = base_url+"getaset";
    $.ajax({
        url: url,
        dataType: 'json',
        cache: true,
        success: function(msg){
            map = L.map('map').setView([-2.548926,118.0148634], 5);
            // add an OpenStreetMap tile layer
            /*         
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
            */

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZWxjaWNrbyIsImEiOiJQOFVPUFBrIn0.SYrEN9dSd23jmioBB6NAvQ'
            }).addTo(map);
            
            markers = L.markerClusterGroup();
            for (var i = 0; i < msg.length; i++) {
                contentString = [
                                  '<div style="padding:10px; width:450px; height:auto;">',
                                    '<table width="100%" cellpadding="5" cellspacing="5">',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Kode Aset',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;<a href="'+base_url+'aset/detail/'+msg[i].id+'">'+msg[i].kodeaset+'</a>',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Nama Aset',
                                            '</td>',
                                            '<td width="50%">',
                                            '&nbsp;'+msg[i].nama_aset+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Provinsi',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].provinsi+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Kabupaten / Kota',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].kabkota+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Keterangan Lokasi',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].kodelokasi+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Business Area',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].businessarea+'',
                                            '</td>',
                                        '</tr>',
                                    '</table>',
                                  '</div>'
                                ].join('');

                marker = L.marker(new L.LatLng(msg[i].latitude, msg[i].longitude));
                marker.bindPopup(contentString, { maxWidth:800 });
                markers.addLayer(marker);
            }
            map.addLayer(markers);
        }
    });
});

$(document).ready(function(){
	$("#caripeta").click(function(){
	var atl = $("#atl").val();
    var url = base_url+"searchallaset";
	$.ajax({
    url: url,
    data: "atl="+atl,
    type: 'POST',
    dataType: 'json',
    cache: false,
		success: function(msg){
        	map.remove();
            map = L.map('map').setView([-2.548926,118.0148634], 5);
            /*			
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
			}).addTo(map);
            */
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZWxjaWNrbyIsImEiOiJQOFVPUFBrIn0.SYrEN9dSd23jmioBB6NAvQ'
            }).addTo(map);
    		markers = L.markerClusterGroup();
        	for (var i = 0; i < msg.length; i++) {
				  contentString = [
                                  '<div style="padding:10px; width:450px; height:auto;">',
                                    '<table width="100%" cellpadding="3" cellspacing="3">',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Kode Aset',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;<a href="'+base_url+'aset/detail/'+msg[i].id+'">'+msg[i].kodeaset+'</a>',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Nama Aset',
                                            '</td>',
                                            '<td width="50%">',
                                            '&nbsp;'+msg[i].nama_aset+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Provinsi',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].provinsi+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Kabupaten / Kota',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].kabkota+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Keterangan Lokasi',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].kodelokasi+'',
                                            '</td>',
                                        '</tr>',
                                        '<tr>',
                                            '<td width="20%">',
                                            'Business Area',
                                            '</td>',
                                            '<td>',
                                            '&nbsp;'+msg[i].businessarea+'',
                                            '</td>',
                                        '</tr>',
                                    '</table>',
                                  '</div>'
                                ].join('');
				marker = L.marker(new L.LatLng(msg[i].latitude, msg[i].longitude));
                marker.bindPopup(contentString, { maxWidth:800 });
                markers.addLayer(marker);
			}
			map.addLayer(markers);
			}
		});
	});
});