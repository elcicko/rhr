var ajaxku;

function ajaxkota(id) {
    ajaxku = buatajax();
    var url = base_url+"getkabkota";
    url = url + "/" + id;
    ajaxku.onreadystatechange = cityChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function ajaxkecamatan(id) {
    ajaxku = buatajax();
    var url = base_url+"getkecamatanbykota";
    url = url + "/" + id;
    ajaxku.onreadystatechange = districtChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function ajaxkelurahan(id) {
    ajaxku = buatajax();
    var url = base_url+"getkelurahanbykecamatan";
    url = url + "/" + id;
    ajaxku.onreadystatechange = villageChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function buatajax() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  }

  if (window.ActiveXObject) {
    return new ActiveXObject("Microsoft.XMLHTTP");
  }

  return null;
}

function cityChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kota").innerHTML = data
        } else {
            document.getElementById("kota").value = "<option selected>-- Kota / Kabupaten --</option>";
        }
    }
}

function districtChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kecamatan").innerHTML = data
        } else {
            document.getElementById("kecamatan").value = "<option selected>-- Kecamatan --</option>";
        }
    }
}

function villageChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("desa").innerHTML = data
        } else {
            document.getElementById("desa").value = "<option selected>-- Kelurahan / Desa --</option>";
        }
    }
}
$(document).ready(function(){
  $('#tanggal_inspeksi').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tanggal_penilaian').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tanggal_dikeluarkan').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tanggal_berakhir').datepicker({format: "yyyy-mm-dd", autoclose: true});
  $('#tanggal_gambar_situasi').datepicker({format: "yyyy-mm-dd", autoclose: true});
});