<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><a href="{{ url('menus') }}"><i class="fa fa-gear"></i> Menu</a></li>
  <li><i class="fa fa-gear"></i> {{ $action_title }}</li>
</ol>
<script src="{{ asset('js_app/validation/menu_form.js') }}"></script>
<form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
<fieldset>
    <legend><?php echo $action_title; ?></legend>
    <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Nama Menu</label>  
          <input id="name" name="name" value="{{ isset($menu->menu_name) ? $menu->menu_name : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Route</label>  
            <input id="route" name="route" value="{{ isset($menu->menu_route) ? $menu->menu_route : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
            <button id="" name="" class="btn btn-success"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
</fieldset>
</form>