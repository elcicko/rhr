<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/leaflet.css') }}">
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/MarkerCluster.css') }}" />
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/MarkerCluster.Default.css') }}" />
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/leaflet.markercluster-src.js') }}"></script>
<ol class="breadcrumb">
  <li><i class="fa fa-file-o"></i> Data Pembanding</li>
</ol>
<br>
<ul class="nav nav-tabs">
    <li><a href="{{ url('pembanding') }}">Table Format</a></li>
    <li class="active"><a href="#">Map Format</a></li>
</ul>
<br>
<div class="row"> 
    <div class="col-lg-12">
    @if ($write == 1)
      <a class="btn btn-success" href="{{ url('pembanding/create') }}"><i class="fa fa-plus"></i> Tambah Data Pembanding</a>
      &nbsp;&nbsp;
    @else 
    @endif
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
        <div class="col-lg-12" style="background:#ccc;padding-top: 25px; margin-bottom:20px; margin-top: 15px; margin-left: 20px; padding-right: 20px; width: 95%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Kontak Nama" name="name" id="name">
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Kontak Telepon" name="phone" id="phone">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="search"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Alamat" name="address" id="address">
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Harga Penawaran" name="offering_jual" id="offering_jual">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div id="map" style="width:100%; height:700px"></div>
        <script>
          
            var map;
            var markers;
            var marker;
            var contentString;

            $(document).ready(function(){
                var url = "{{ url('getallpembanding') }}";
                $.ajax({
                    url: url,
                    dataType: 'json',
                    cache: true,
                    success: function(msg){
                        map = L.map('map').setView([-2.548926,118.0148634], 5);
                        // add an OpenStreetMap tile layer
                        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                        }).addTo(map);
                        
                        markers = L.markerClusterGroup();
                        for (var i = 0; i < msg.length; i++) {
                            contentString = [
                                              '<div style="padding:10px; width:450px; height:auto;">',
                                                '<table width="100%" cellpadding="5" cellspacing="5">',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'ID Pembanding',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;<a href="{{ url('pembanding/detail/')}}/'+msg[i].id+'">'+msg[i].id+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kontak Nama',
                                                        '</td>',
                                                        '<td width="50%">',
                                                        '&nbsp;'+msg[i].name+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kontak Telepon',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].phone+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Alamat',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].address+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Jenis Properti',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].type+'',
                                                        '</td>',
                                                    '</tr>',
                                                '</table>',
                                              '</div>'
                                            ].join('');

                            marker = L.marker(new L.LatLng(msg[i].latitude, msg[i].longitude));
                            marker.bindPopup(contentString, { maxWidth:800 });
                            markers.addLayer(marker);
                        }
                        map.addLayer(markers);
                    }
                });
            });

    		$(document).ready(function(){
        		$("#search").click(function(){
                var name           = $("#name").val();
                var phone          = $("#phone").val();
    	    	var address        = $("#address").val();
    	        /*var type 	       = $("#type").val();*/
                var offering_jual  = $("#offering_jual").val();
                
                var url = "{{ url('searchallpembanding') }}";
    			$.ajax({
    	        url: url,
    	        data: "name="+name+"&phone="+phone+"&address="+address+"&offering_jual="+offering_jual,
                type: 'POST',
    	        dataType: 'json',
    	        cache: false,
            		success: function(msg){
    		        	map.remove();
    		            map = L.map('map').setView([-2.548926,118.0148634], 5);
    					L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    					    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    					}).addTo(map);
                		markers = L.markerClusterGroup();
    	            	for (var i = 0; i < msg.length; i++) {
    						  contentString = [
                                              '<div style="padding:10px; width:450px; height:auto;">',
                                                '<table width="100%" cellpadding="5" cellspacing="5">',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'ID Pembanding',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;<a href="{{ url('pembanding/detail/')}}/'+msg[i].id+'">'+msg[i].id+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kontak Nama',
                                                        '</td>',
                                                        '<td width="50%">',
                                                        '&nbsp;'+msg[i].name+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kontak Telepon',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].phone+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Alamat',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].address+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Jenis Properti',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].type+'',
                                                        '</td>',
                                                    '</tr>',
                                                '</table>',
                                              '</div>'
                                            ].join('');
    						marker = L.marker(new L.LatLng(msg[i].latitude, msg[i].longitude));
                            marker.bindPopup(contentString, { maxWidth:800 });
                            markers.addLayer(marker);
    					}
    					map.addLayer(markers);
            			}
        			});
        		});
    		});
        </script>
    </div>
</div>