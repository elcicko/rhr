<link rel="stylesheet" href="<?php echo asset('assets/lib/js/leafletjs/leaflet.css'); ?>">
<script src="<?php echo asset('assets/lib/js/leafletjs/leaflet.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="{{ url('assets/libjs/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<ol class="breadcrumb">
  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><a href="{{ url('pembanding') }}"><i class="fa fa-leaf"></i> Data Pembanding</a></li>
  <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}" style="padding-right: 10px;padding-left: 5px;">
  <fieldset>
    <div class="row">
      <div class="col-lg-12">
        <div id="map" style="width:100%; height:450px;"></div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">ID Aset</label>
          @if ($form_title == "edit")  
            <input id="id_asset" name="id_asset" value="{{ isset($pembanding->id_asset) ? $pembanding->id_asset : ''; }}" type="text" placeholder="" class="form-control input-md">
          @elseif ($form_title == "add")
            <input id="id_asset" name="id_asset" value="{{ isset($id_asset) ? $id_asset : ''; }}" type="text" placeholder="" class="form-control input-md">
          @endif
        </div>

        <div class="form-group">
          <label for="order">Nama Kontak Person</label>  
          <input id="name" name="name" value="{{ isset($pembanding->name) ? $pembanding->name : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Telepon</label>  
          <input id="phone" name="phone" value="{{ isset($pembanding->phone) ? $pembanding->phone : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Alamat</label>  
          <input id="address" name="address" value="{{ isset($pembanding->address) ? $pembanding->address : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Properti Yang Dianalisis</label>  
          {{ Form::select('type', $type, isset($pembanding->type) ? $pembanding->type : '', $attributes = array('class'=>'form-control', 'id'=>'type')) }}
        </div>
          
        <div class="form-group">
          <label for="order">Jenis Data</label>  
          {{ Form::select('type_data', $type_data, isset($pembanding->type_data) ? $pembanding->type_data : '', $attributes = array('class'=>'form-control', 'id'=>'type_data')) }}
        </div>

        <div class="form-group">
          <label for="order">Koordinat Latitude</label>  
          <input id="latitude" name="latitude" value="{{ isset($pembanding->latitude) ? $pembanding->latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Koordinat Longitude</label>  
          <input id="longitude" name="longitude" value="{{ isset($pembanding->longitude) ? $pembanding->longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Karakteristik Properti</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Status Dokumen Kepemilikan</label>  
          {{ Form::select('title_particular', $title_particular, isset($pembanding->title_particular) ? $pembanding->title_particular : '', $attributes = array('class'=>'form-control', 'id'=>'provinsi')) }}
        </div>

        <div class="form-group">
          <label for="order">Syarat Pembayaran</label>  
          {{ Form::select('term', $term, isset($pembanding->term) ? $pembanding->term : '', $attributes = array('class'=>'form-control', 'id'=>'term')) }}
        </div>

        <div class="form-group">
          <label for="order">Kondisi Penjualan</label>  
          {{ Form::select('sales_condition', $sales_condition, isset($pembanding->sales_condition) ? $pembanding->sales_condition : '', $attributes = array('class'=>'form-control', 'id'=>'sales_condition')) }}
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Pengeluaran Setelah Pembelian</label>  
          {{ Form::select('other_expenses', $other_expenses, isset($pembanding->other_expenses) ? $pembanding->other_expenses : '', $attributes = array('class'=>'form-control', 'id'=>'other_expenses')) }}
        </div>

        <div class="form-group">
          <label for="order">Kondisi Pasar</label>  
          {{ Form::select('market_condition', $market_condition, isset($pembanding->market_condition) ? $pembanding->market_condition : '', $attributes = array('class'=>'form-control', 'id'=>'market_condition')) }}
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Tanah</legend>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Luas Area (m2)</label>  
          <input id="land_area" name="land_area" value="{{ isset($pembanding->land_area) ? $pembanding->land_area : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Posisi Tanah</label>  
          {{ Form::select('land_position', $land_position, isset($pembanding->land_position) ? $pembanding->land_position : '', $attributes = array('class'=>'form-control', 'id'=>'land_position')) }}
        </div>

        <div class="form-group">
          <label for="order">Bentuk Tanah</label>  
          {{ Form::select('land_shape', $land_shape, isset($pembanding->land_shape) ? $pembanding->land_shape : '', $attributes = array('class'=>'form-control', 'id'=>'land_shape')) }}
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Kondisi Topografi</label>  
          {{ Form::select('land_topography', $land_topography, isset($pembanding->land_topography) ? $pembanding->land_topography : '', $attributes = array('class'=>'form-control', 'id'=>'land_topography')) }}
        </div>

        <div class="form-group">
          <label for="order">Orientasi (Hadap)</label>  
          {{ Form::select('land_orientation', $land_orientation, isset($pembanding->land_orientation) ? $pembanding->land_orientation : '', $attributes = array('class'=>'form-control', 'id'=>'land_orientation')) }}
        </div>

        <div class="form-group">
          <label for="order">Lebar Muka (frontage) - (m)</label>  
          <input id="land_frontage" name="land_frontage" value="{{ isset($pembanding->land_frontage) ? $pembanding->land_frontage : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Zoning</label>  
          {{ Form::select('land_zoning', $land_zoning, isset($pembanding->land_zoning) ? $pembanding->land_zoning : '', $attributes = array('class'=>'form-control', 'id'=>'land_zoning')) }}
        </div>

        <div class="form-group">
          <label for="order">Perkiraan Harga Properti per m2 (Rp)</label>  
          <input id="land_perkiraan" name="land_perkiraan" value="{{ isset($pembanding->land_perkiraan) ? $pembanding->land_perkiraan : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Indikasi Transaksi Bangunan per m2 (Rp)</label>  
          <input id="land_indikasi" name="land_indikasi" value="{{ isset($pembanding->land_indikasi) ? $pembanding->land_indikasi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Bangunan</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Luas Area (m2)</label>  
          <input id="building_area" name="building_area" value="{{ isset($pembanding->building_area) ? $pembanding->building_area : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Jumlah Lantai</label>  
          <input id="building_storey" name="building_storey" value="{{ isset($pembanding->building_storey) ? $pembanding->building_storey : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Tahun Pembangunan</label>  
          <input id="building_year_constructed" name="building_year_constructed" value="{{ isset($pembanding->building_year_constructed) ? $pembanding->building_year_constructed : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tahun Renovasi</label>  
          <input id="building_year_renovated" name="building_year_renovated" value="{{ isset($pembanding->building_year_renovated) ? $pembanding->building_year_renovated : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Spesifikasi Bangunan</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Pondasi</label>  
          {{ Form::select('pondasi', $pondasi, isset($pembanding->pondasi) ? $pembanding->pondasi : '', $attributes = array('class'=>'form-control', 'id'=>'pondasi')) }}
        </div>

        <div class="form-group">
          <label for="order">Struktur</label>  
          {{ Form::select('struktur', $struktur, isset($pembanding->struktur) ? $pembanding->struktur : '', $attributes = array('class'=>'form-control', 'id'=>'struktur')) }}
        </div>

        <div class="form-group">
          <label for="order">Lantai</label>  
          {{ Form::select('lantai', $lantai, isset($pembanding->lantai) ? $pembanding->lantai : '', $attributes = array('class'=>'form-control', 'id'=>'lantai')) }}
        </div>

        <div class="form-group">
          <label for="order">Dinding</label>  
          {{ Form::select('dinding', $dinding, isset($pembanding->dinding) ? $pembanding->dinding : '', $attributes = array('class'=>'form-control', 'id'=>'dinding')) }}
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Pintu dan Jendela</label>  
          {{ Form::select('pintu', $pintu, isset($pembanding->pintu) ? $pembanding->pintu : '', $attributes = array('class'=>'form-control', 'id'=>'pintu')) }}
        </div>

        <div class="form-group">
          <label for="order">Langit-langit</label>  
          {{ Form::select('langit', $langit, isset($pembanding->langit) ? $pembanding->langit : '', $attributes = array('class'=>'form-control', 'id'=>'langit')) }}
        </div>

        <div class="form-group">
          <label for="order">Rangka Atap</label>  
          {{ Form::select('rangka_atap', $rangka_atap, isset($pembanding->rangka_atap) ? $pembanding->rangka_atap : '', $attributes = array('class'=>'form-control', 'id'=>'rangka_atap')) }}
        </div>

        <div class="form-group">
          <label for="order">Penutup Atap</label>  
          {{ Form::select('penutup_atap', $penutup_atap, isset($pembanding->penutup_atap) ? $pembanding->penutup_atap : '', $attributes = array('class'=>'form-control', 'id'=>'penutup_atap')) }}
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Peralatan Bangunan</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Listrik (Watt)</label>  
          <input id="equipment_electricity" name="equipment_electricity" value="{{ isset($pembanding->equipment_electricity) ? $pembanding->equipment_electricity : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">AC (Unit)</label>  
          <input id="equipment_ac" name="equipment_ac" value="{{ isset($pembanding->equipment_ac) ? $pembanding->equipment_ac : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Air</label>  
          <input id="equipment_water" name="equipment_water" value="{{ isset($pembanding->equipment_water) ? $pembanding->equipment_water : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Telepon (Saluran)</label>  
          <input id="equipment_telephone" name="equipment_telephone" value="{{ isset($pembanding->equipment_telephone) ? $pembanding->equipment_telephone : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Lainnya</label>  
         <input id="equipment_others" name="equipment_others" value="{{ isset($pembanding->equipment_others) ? $pembanding->equipment_others : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Karakteristik Ekonomi</label>  
          <input id="equipment_economic" name="equipment_economic" value="{{ isset($pembanding->equipment_economic) ? $pembanding->equipment_economic : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
          <label for="order">Penjualan Komponen Non-Reality</label>  
          <input id="equipment_non" name="equipment_non" value="{{ isset($pembanding->equipment_non) ? $pembanding->equipment_non : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>Penawaran/Data Transaksi</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Tahun / Bulan Penjualan</label>  
          <input id="offering_year" name="offering_year" value="{{ isset($pembanding->offering_year) ? $pembanding->offering_year : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Indikasi Transaksi Properti (Rp)</label>  
           <input id="offering_transacted_price" name="offering_transacted_price" value="{{ isset($pembanding->offering_transacted_price) ? $pembanding->offering_transacted_price : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Penawaran Tertinggi (Rp)</label>  
          <input id="offering_highest" name="offering_highest" value="{{ isset($pembanding->offering_highest) ? $pembanding->offering_highest : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Potongan Harga (%)</label>  
          <input id="offering_discount" name="offering_discount" value="{{ isset($pembanding->offering_discount) ? $pembanding->offering_discount : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Indikasi Transaksi Tanah dan Bangunan (Rp)</label>  
          <input id="offering_indicative_land_building" name="offering_indicative_land_building" value="{{ isset($pembanding->offering_indicative_land_building) ? $pembanding->offering_indicative_land_building : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Indikasi Transaksi Bangunan (Rp)</label>  
          <input id="offering_indicative_building" name="offering_indicative_building" value="{{ isset($pembanding->offering_indicative_building) ? $pembanding->offering_indicative_building : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Indikasi Transaksi Tanah (Rp)</label>  
          <input id="offering_indicative_land" name="offering_indicative_land" value="{{ isset($pembanding->offering_indicative_land) ? $pembanding->offering_indicative_land : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Indikasi Transaksi Tanah per m2 (Rp)</label>  
          <input id="offering_indicative_land_qsm" name="offering_indicative_land_qsm" value="{{ isset($pembanding->offering_indicative_land_qsm) ? $pembanding->offering_indicative_land_qsm : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>GIM</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Harga Penawaran (jual) (Rp)</label>  
          <input id="offering_jual" name="offering_jual" value="{{ isset($pembanding->offering_jual) ? $pembanding->offering_jual : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Harga Penawaran (sewa) (Rp)</label>  
          <input id="offering_sewa" name="offering_sewa" value="{{ isset($pembanding->offering_sewa) ? $pembanding->offering_sewa : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">GIM</label>  
          <input id="offering_gim" name="offering_gim" value="{{ isset($pembanding->offering_gim) ? $pembanding->offering_gim : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Properti Yang Dinilai</label>  
          <input id="property" name="property" value="{{ isset($pembanding->property) ? $pembanding->property : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>        
      </div>
    </div> 
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
            <button id="" name="" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
      </div>
    </div>
  </fieldset>
</form>
<script>
  var map;
  var marker;
  $(document).ready(function(){
    <?php
    if ($form_title == "add") {
    ?>
      map = L.map('map').setView([-2.548926,118.0148634], 5);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);   
      marker = L.marker([-2.548926,118.0148634], { draggable :true }).addTo(map) 
    <?php 
    } elseif ($form_title == "edit") {
    ?>
      map = L.map('map').setView([<?php echo isset($pembanding->koordinat_latitude) ? $pembanding->koordinat_latitude : '-2.548926'; ?>,<?php echo isset($pembanding->koordinat_longitude) ? $pembanding->koordinat_longitude : '118.0148634'; ?>], 15);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);   
      marker = L.marker([<?php echo isset($pembanding->koordinat_latitude) ? $pembanding->koordinat_latitude : '-2.548926'; ?>,<?php echo isset($pembanding->koordinat_longitude) ? $pembanding->koordinat_longitude : '118.0148634'; ?>], { draggable :true }).addTo(map) 
    <?php 
    }
    ?>

    marker.on('dragend', function(event){
        var marker = event.target;
        var lat = marker.getLatLng().lat;
        var lng = marker.getLatLng().lng;
        document.getElementById('latitude').value = lat;
        document.getElementById('longitude').value = lng;
    });
    map.addLayer(marker);
  });
</script>