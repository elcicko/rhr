<link rel="stylesheet" href="<?php echo asset('assets/js/leafletjs/leaflet.css'); ?>">
<script src="<?php echo asset('assets/js/leafletjs/leaflet.js'); ?>" type="text/javascript"></script>
<ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('aset') }}"><i class="fa fa-leaf"></i> Manajemen Aset</a></li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
<fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="col-lg-8">
      <div id="map" style="width:100%; height:500px"></div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Business Area</label>  
          {{ Form::select('businessarea', $businessarea, isset($survey->business_area_id) ? $survey->business_area_id : '', $attributes = array('class'=>'form-control', 'id'=>'businessarea')) }}
        </div>

        <div class="form-group">
          <label for="order">Keterangan Lokasi</label>  
          <textarea id="kodelokasi" name="kodelokasi" class="form-control input-md" rows="4">{{ isset($survey->kodelokasi) ? $survey->kodelokasi : ''; }}</textarea>
        </div>

        <div class="form-group">
          <label for="order">Garis Lintang</label>  
          <input id="latitude" name="latitude" value="{{ isset($survey->latitude) ? $survey->latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Garis Bujur</label>  
          <input id="longitude" name="longitude" value="{{ isset($survey->longitude) ? $survey->longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
            <button id="" name="" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
</fieldset>
</form>
<script>
var map;
var marker;

$(document).ready(function(){
  <?php
  if ($form_title == "add") {
  ?>
    map = L.map('map').setView([{{ $aset->kabkota->latitude }},{{ $aset->kabkota->longitude }}], 5);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);   
    marker = L.marker([{{ $aset->kabkota->latitude }},{{ $aset->kabkota->longitude }}], { draggable :true }).addTo(map) 
  <?php 
  } elseif ($form_title == "edit") {
  ?>
    map = L.map('map').setView([<?php echo isset($survey->latitude) ? $survey->latitude : '-2.548926'; ?>,<?php echo isset($survey->longitude) ? $survey->longitude : '118.0148634'; ?>], 15);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);   
    marker = L.marker([<?php echo isset($survey->latitude) ? $survey->latitude : '-2.548926'; ?>,<?php echo isset($survey->longitude) ? $survey->longitude : '118.0148634'; ?>], { draggable :true }).addTo(map) 
  <?php 
  }
  ?>

  marker.on('dragend', function(event){
      var marker = event.target;
      var lat = marker.getLatLng().lat;
      var lng = marker.getLatLng().lng;
      document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
  });
  map.addLayer(marker);
});
</script>