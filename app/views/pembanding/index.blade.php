<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>

<ol class="breadcrumb">
  <li><i class="fa fa-file-o"></i> Data Pembanding</li>
</ol>
<br>
<ul class="nav nav-tabs">
  <li class="active"><a href="#">Table Format</a></li>
  <li><a href="{{ url('pembanding/index_peta') }}">Map Format</a></li>
</ul>
<br>
<div class="row"> 
    <div class="col-lg-12">
    @if ($write == 1)
      <a class="btn btn-success" href="{{ url('pembanding/create') }}"><i class="fa fa-plus"></i> Tambah Data Pembanding</a>
      &nbsp;&nbsp;
    @else 
    @endif
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
        <div class="col-lg-12" style="background:#ccc;padding-top: 25px; margin-bottom:20px; margin-top: 15px; margin-left: 20px; padding-right: 20px; width: 95%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Kontak Nama" name="name" id="name">
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Kontak Telepon" name="phone" id="phone">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="search"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Alamat" name="address" id="address">
                    </div>

                   <!--  <div class="form-group">
                     {{ Form::select('type_data', $type_data, '', $attributes = array('style'=>'width:100%', 'id'=>'type_data')) }}
                   </div> -->

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Harga Penawaran" name="offering_jual" id="offering_jual">
                    </div>
                </div>
               <!--  <div class="col-lg-4">
                   <div class="form-group">
                     <input type="text" class="form-control" placeholder="Harga Penawaran Transaksi" name="offering_transacted_price" id="offering_transacted_price">
                   </div>
               </div> -->
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    @if(Session::has('message'))
      <br><br>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <center>{{ Session::get('message') }}</center>
        </div>
    @else
    @endif
    <script>
    $(document).ready(function(){
        $('#search').click(function() { oTable.fnReloadAjax (); });
    });
    </script>
    {{
        Datatable::table()
        ->addColumn('#','Kontak Nama','Kontak Telepon', 'Alamat', 'Jenis Properti','Harga Penawaran','Harga Penawaran Transaksi','Action')
        ->setUrl(route('datatable.pembanding',array('update'=>$update,'delete'=> $delete)))
        ->setCallbacks(["fnServerParams" => 'function ( aoData ) {           
                          aoData.push({ "name": "name", "value": $(\'#name\').val() });
                          aoData.push({ "name": "phone", "value": $(\'#phone\').val() });
                          aoData.push({ "name": "address", "value": $(\'#address\').val() });
                          <!-- aoData.push({ "name": "type", "value": $(\'#type\').val() }); -->
                          <!-- aoData.push({ "name": "type_data", "value": $(\'#type_data\').val() }); -->
                          aoData.push({ "name": "offering_jual", "value": $(\'#offering_jual\').val() });
                          <!-- aoData.push({ "name": "offering_transacted_price", "value": $(\'#offering_transacted_price\').val() }); -->
                        }'
                      ])
        ->render()
    }}
  </div>
</div>