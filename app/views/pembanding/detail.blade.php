<link rel="stylesheet" href="{{ asset('assets/lib/js/leafletjs/leaflet.css') }}">
<script src="{{ asset('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<br>
<ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('pembanding') }}"><i class="fa fa-leaf"></i> Data Pembanding</a></li>
    <li><i class="fa fa-leaf"></i> Detail Pembanding</li>
</ol>
<br><br>
<fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="row">
      <br>
      <div class="col-lg-4">
        <table class="table" width="100%" cellpadding="5" cellspacing="5" style="font-size:13px;">
          <tr>
            <td width="35%">Nama</td><td>{{ $pembanding->name }}</td>
          </tr>
          <tr>
            <td>Telepon</td><td>{{ $pembanding->phone }}</td>
          </tr>
          <tr>
            <td>Alamat</td><td>{{ $pembanding->address }}</td>
          </tr>
          <tr>
            <td>Jenis Aset</td><td>{{ $pembanding->type }}</td>
          </tr>
          <tr>
            <td>Koordinat Latitude</td><td>{{ $pembanding->latitude }}</td>
          </tr>
           <tr>
            <td>Koordinat Longitude</td><td>{{ $pembanding->longitude }}</td>
          </tr>
        </table>
      </div>
      <div class="col-lg-8">
        <div class="well" id="map" style="width:100%; height:380px"></div>
        <script>
        var map;
        var marker;
        $(document).ready(function(){
          map = L.map('map').setView([{{ isset($pembanding->latitude) ? $pembanding->latitude : '' }},{{ isset($pembanding->longitude) ? $pembanding->longitude : ''}}], 16);
          L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
              attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
          }).addTo(map);   
          marker = L.marker([{{ isset($pembanding->latitude) ? $pembanding->latitude : '' }},{{ isset($pembanding->longitude) ? $pembanding->longitude : ''}} ], { draggable :false }).addTo(map)
        });
        </script>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>ALAMAT PROPERTI</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr>
              <th width="20%"></th>
              <th><center>Nilai</center></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Nama Kontak Person</b></td>
              <td>{{ isset($pembanding->name) ? $pembanding->name : '' }}</td>
            </tr>
            <tr>
              <td><b>Telepon</b></td>
              <td>{{ isset($pembanding->phone) ? $pembanding->phone : '' }}</td>
            </tr>
            <tr>
              <td><b>Properti Yang Dianalisis</b></td>
              <td>{{ isset($pembanding->type) ? $pembanding->type : '' }}</td>
            </tr>
            <tr>
              <td><b>Jenis Data</b></td>
              <td>{{ isset($pembanding->source) ? $pembanding->source : '' }}</td>
            </tr>
            <tr>
              <td><b>Provinsi</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Kota / Kabupaten</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Kecamatan</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Kelurahan / Desa</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Alamat Properti</b></td>
              <td>{{ isset($pembanding->address) ? $pembanding->address : '' }}</td>
            </tr>
            <tr>
              <td><b>Koordinat Latitude</b></td>
              <td>{{ isset($pembanding->latitude) ? $pembanding->latitude : '' }}</td>
            </tr>
            <tr>
              <td><b>Koordinat Longitude</b></td>
              <td>{{ isset($pembanding->longitude) ? $pembanding->longitude : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>KARAKTERISTIK PROPERTI</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr>
              <th width="20%"></th>
              <th><center>Nilai</center></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Status Dokumen Kepemilikan</b></td>
              <td>{{ isset($pembanding->title_particular) ? $pembanding->title_particular : '' }}</td>
            </tr>
            <tr>
              <td><b>Syarat Pembiayaan</b></td>
              <td>{{ isset($pembanding->term) ? $pembanding->term : '' }}</td>
            </tr>
            <tr>
              <td><b>Kondisi Penjualan</b></td>
              <td>{{ isset($pembanding->sales_condition) ? $pembanding->sales_condition : '' }}</td>
            </tr>
            <tr>
              <td><b>Pengeluaran Setelah Pembelian</b></td>
              <td>{{ isset($pembanding->other_expenses) ? $pembanding->other_expenses : '' }}</td>
            </tr>
            <tr>
              <td><b>Kondisi Pasar</b></td>
              <td>{{ isset($pembanding->market_condition) ? $pembanding->market_condition : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>TANAH</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr>
              <th width="20%"></th>
              <th><center>Nilai</center></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Luas (m2)</b></td>
              <td>{{ isset($pembanding->land_area) ? $pembanding->land_area : '' }}</td>
            </tr>
            <tr>
              <td><b>Posisi Tanah</b></td>
              <td>{{ isset($pembanding->land_position) ? $pembanding->land_position : '' }}</td>
            </tr>
            <tr>
              <td><b>Bentuk Tanah</b></td>
              <td>{{ isset($pembanding->land_shape) ? $pembanding->land_shape : '' }}</td>
            </tr>
            <tr>
              <td><b>Orientasi (Hadap)</b></td>
              <td>{{ isset($pembanding->land_orientation) ? $pembanding->land_orientation : '' }}</td>
            </tr>
            <tr>
              <td><b>Jenis Perkerasan / Accessibility</b></td>
              <td>{{ isset($pembanding->land_frontage1) ? $pembanding->land_frontage1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Lebar Jalan</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Lokasi</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Kondisi Lingkungan</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Potensi Banjir</b></td>
              <td></td>
            </tr>
            <tr>
              <td><b>Peruntukan / Zoning</b></td>
              <td>{{ isset($pembanding->land_zoning) ? $pembanding->land_zoning : '' }}</td>
            </tr>
            <tr>
              <td><b>Kondisi Topografi</b></td>
              <td>{{ isset($pembanding->land_topography) ? $pembanding->land_topography : '' }}</td>
            </tr>
            <tr>
              <td><b>Lebar Muka (frontage) - (m2)</b></td>
              <td>{{ isset($pembanding->land_frontage) ? $pembanding->land_frontage : '' }}</td>
            </tr>
            <tr>
              <td><b>Perkiraan Harga Properti per m2 (Rp)</b></td>
              <td>{{ isset($pembanding->land_perkiraan) ? $pembanding->land_perkiraan : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Bangunan per m2 (Rp)</b></td>
              <td>{{ isset($pembanding->land_indikasi) ? $pembanding->land_indikasi : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>BANGUNAN</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr>
              <th width="20%"></th>
              <th><center>Nilai</center></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Luas Area (m2)</b></td>
              <td>{{ isset($pembanding->building_area) ? $pembanding->building_area : '' }}</td>
            </tr>
            <tr>
              <td><b>Jumlah Lantai</b></td>
              <td>{{ isset($pembanding->building_storey) ? $pembanding->building_storey : '' }}</td>
            </tr>
            <tr>
              <td><b>Tahun Pembangunan</b></td>
              <td>{{ isset($pembanding->building_year_constructed) ? $pembanding->building_year_constructed : '' }}</td>
            </tr>
            <tr>
              <td><b>Tahun Renovasi</b></td>
              <td>{{ isset($pembanding->building_year_renovated) ? $pembanding->building_year_renovated : '' }}</td>
            </tr>
            <tr>
              <td colspan="2" style="background-color:#999;"><b>SPESIFIKASI BANGUNAN</b></td>
            </tr>
            <tr>
              <td><b>Pondasi</b></td>
              <td>{{ isset($pembanding->pondasi) ? $pembanding->pondasi : '' }}</td>
            </tr>
            <tr>
              <td><b>Struktur</b></td>
              <td>{{ isset($pembanding->struktur) ? $pembanding->struktur : '' }}</td>
            </tr>
            <tr>
              <td><b>Lantai</b></td>
              <td>{{ isset($pembanding->lantai) ? $pembanding->lantai : '' }}</td>
            </tr>
            <tr>
              <td><b>Dinding</b></td>
              <td>{{ isset($pembanding->dinding) ? $pembanding->dinding : '' }}</td>
            </tr>
            <tr>
              <td><b>Pintu & Jendela</b></td>
              <td>{{ isset($pembanding->pintu) ? $pembanding->pintu : '' }}</td>
            </tr>
            <tr>
              <td><b>Langit-langit</b></td>
              <td>{{ isset($pembanding->langit) ? $pembanding->langit : '' }}</td>
            </tr>
            <tr>
              <td><b>Rangka Atap</b></td>
              <td>{{ isset($pembanding->rangka_atap) ? $pembanding->rangka_atap : '' }}</td>
            </tr>
            <tr>
              <td><b>Penutup Atap</b></td>
              <td>{{ isset($pembanding->penutup_atap) ? $pembanding->penutup_atap : '' }}</td>
            </tr>
            <tr>
              <td colspan="2" style="background-color:#999;"><b>PERALATAN BANGUNAN</b></td>
            </tr>
            <tr>
              <td><b>Listrik (Watt)</b></td>
              <td>{{ isset($pembanding->equipment_electricity) ? $pembanding->equipment_electricity : '' }}</td>
            </tr>
            <tr>
              <td><b>AC (Unit)</b></td>
              <td>{{ isset($pembanding->equipment_ac) ? $pembanding->equipment_ac : '' }}</td>
            </tr>
            <tr>
              <td><b>Air</b></td>
              <td>{{ isset($pembanding->equipment_water) ? $pembanding->equipment_water : '' }}</td>
            </tr>
            <tr>
              <td><b>Telepon (Saluran)</b></td>
              <td>{{ isset($pembanding->equipment_telephone) ? $pembanding->equipment_telephone : '' }}</td>
            </tr>
            <tr>
              <td><b>Lainnya</b></td>
              <td>{{ isset($pembanding->equipment_others) ? $pembanding->equipment_others : '' }}</td>
            </tr>
            <tr>
              <td><b>Karakteristik Ekonomi</b></td>
              <td>{{ isset($pembanding->equipment_economy) ? $pembanding->equipment_economy : '' }}</td>
            </tr>
            <tr>
              <td><b>Penjualan Komponen Non-Reality</b></td>
              <td>{{ isset($pembanding->equipment_non) ? $pembanding->equipment_non : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</div>
</fieldset>