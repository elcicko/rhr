<ol class="breadcrumb">
  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><i class="fa fa-leaf"></i> Manajemen Aset</li>
</ol>
<script type="text/javascript">
function ajaxkota(id) {
    ajaxku = buatajax();
    var url = "{{ url('getkabkota') }}";
    url = url + "/" + id;
    ajaxku.onreadystatechange = stateChanged;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
}

function buatajax() {
  if (window.XMLHttpRequest) {
  	return new XMLHttpRequest();
  }

  if (window.ActiveXObject) {
  	return new ActiveXObject("Microsoft.XMLHTTP");
  }

  return null;
}

function stateChanged() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kabkota").innerHTML = data
        } else {
            document.getElementById("kabkota").value = "<option selected>-- Silahkan Pilih Kabupaten / Kota --</option>";
        }
    }
}
</script>
<div class="row"> 
  <div class="col-lg-12">
		<a class="btn btn-primary" href="{{ url('aset/create') }}"><i class="fa fa-plus"></i> Tambah Aset</a>
  		<?php 
  		if ($count > 0) {
  		?>
  			<br><br>
			<form class="form search" method="POST" action="{{ $form_action }}">
		    <div class="col-lg-4">
		        <div class="form-group">
		          {{ Form::select('provinsi', $provinsi, '', $attributes = array('class'=>'form-control input-md', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
		        </div>
		    </div>
		    
            <div class="col-lg-4">
		        <div class="form-group">
		          {{ Form::select('kabkota', $kabkota, '', $attributes = array('class'=>'form-control input-md', 'id'=>'kabkota')) }}
		        </div>
		    </div>

            <div class="col-lg-4">
                <div class="form-group">
                  {{ Form::select('company', $company, '', $attributes = array('class'=>'form-control input-md', 'id'=>'company')) }}
                </div>
            </div>

             <div class="col-lg-4">
                <div class="form-group">
                  {{ Form::select('businessarea', $businessarea, '', $attributes = array('class'=>'form-control input-md', 'id'=>'businessarea')) }}
                </div>
            </div>

		    <div class="col-lg-2">
		        <div class="form-group">
		            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> CARI DATA</button>
		        </div>
			</div>
		</form>
			@if(Session::has('message'))
				<br><br><br>
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
	        <table class="tablesorter">
	            <thead>
	            <tr>
	                <th width="5%"><center>#</center></th>
	                <th width="10%"><center>Kode Aset</center></th>
	                <th width="40%"><center>Nama Aset</center></th>
	                <th width="12%"><center>Company</center></th>
	                <th width="20%"><center>Business Area</center></th>
	                <th colspan="3"><center>Action</center></th>
	            </tr>
	            </thead>
	            <tbody>
	            <?php 
	            $page = $asets->getCurrentPage();
	            if ($page == 1) {
	            	$i = 1;
	            } else {
	            	$i = $asets->getFrom();
	            } 

	            ?>
	            @foreach ($asets as $aset)
	            <tr>
	            	<td><center>{{ $i }}</center></td>
	                <td>{{ $aset->kode_aset }}</td>
	                <td>{{ $aset->nama_asset_1 }}</td>
	                <td>{{ $aset->kode_company }}</td>
	                <td>{{ $aset->kode_bisnis }}</td>
	                <td><center><a href="{{ url('aset/detail/'.$aset->id.'') }}"><i class="fa fa-search"></i></a></center></td>
	                <td><center><a href="{{ url('aset/edit/'.$aset->id.'') }}"><i class="fa fa-wrench"></i></a></center></td>
	                <td><center><a data-toggle="modal" href="#confirm{{ $i }}"><i class="fa fa-trash-o"></i></a></center></td>
	            </tr>
	            <div class="modal fade" id="confirm{{ $i }}" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Konfirmasi</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-primary" href="<?php echo url('aset/destroy/'.$aset->id.''); ?>" >Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>
	            <?php $i++; ?>
	            @endforeach
	            </tbody>
	        </table>
	        <ul class="pagination">
			@if(isset($search) AND ($search) == TRUE)
				{{ $asets->appends(Request::all())->links() }}
			@elseif (isset($search) AND ($search) == FALSE)
				{{ $asets->links() }}
			@endif
			</ul>
	    <?php
		} else {
	    ?>
	    <br><br>
	    <div class="alert alert-danger"><center>DATA ASET KOSONG</center></div>
	    <?php
		}
	    ?>
  </div>
</div>