<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link href="{{ url('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('assets/js/leafletjs/leaflet.css') }}">
		<script src="{{ asset('assets/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
		<title>Laporan Aset</title>
	</head>
	<body>
		<div class="container">
		<h2>Detail Aset</h2>
		<br>
		<div class="row">
			<div class="col-lg-8">
				<table border="0" cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td>Nomor Aset</td>
						<td>: {{ isset($aset->nomor_aset) ? $aset->nomor_aset : '' }}</td>
					</tr>
					<tr>
						<td>Nama Aset</td>
						<td>: {{ isset($aset->nama_aset) ? $aset->nama_aset : '' }}</td>
					</tr>
					<tr>
						<td>Provinsi</td>
						<td>: {{ isset($aset->provinsi->nama_provinsi) ? $aset->provinsi->nama_provinsi : '' }}</td>
					</tr>
					<tr>
						<td>Kabupaten / Kota</td>
						<td>: {{ isset($aset->kabkota->nama_kabkota) ? $aset->kabkota->nama_kabkota : '' }}</td>
					</tr>
					<tr>
						<td>Kabupaten / Kota</td>
						<td>: {{ isset($aset->kabkota->nama_kabkota) ? $aset->kabkota->nama_kabkota : '' }}</td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>: {{ isset($aset->alamat) ? $aset->alamat : '' }}</td>
					</tr>
					<tr>
						<td>Garis Lintang</td>
						<td>: {{ isset($aset->latitude) ? $aset->latitude : '' }}</td>
					</tr>
					<tr>
						<td>Garis Bujur</td>
						<td>: {{ isset($aset->longitude) ? $aset->longitude : '' }}</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>