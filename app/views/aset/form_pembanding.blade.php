<ol class="breadcrumb">
  <li><a href="{{ url('aset') }}"><i class="fa fa-file-o"></i> Data Aset</a></li>
  <li><i class="fa fa-file-o"></i> {{ $action_title }}</li>
</ol>
<br>
<form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
  <fieldset>
    <legend>{{ $action_title }}</legend>
    @if(Session::has('message'))
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <center>{{ Session::get('message') }}</center>
      </div>
    @endif
    <div class="col-lg-6">        
        <div class="form-group">
          <label for="order">Data Pembanding</label>
          <?php 
          $tipe_data_pembanding = ''; 
          if ($tipe_data == "pembanding_1") {
            $tipe_data_pembanding = $aset->pembanding_1;
          } elseif ($tipe_data == "pembanding_2") {  
            $tipe_data_pembanding = $aset->pembanding_2;
          } elseif ($tipe_data == "pembanding_3") {
            $tipe_data_pembanding = $aset->pembanding_3;
          }
          ?>

          {{ Form::select('id_pembanding', $pembanding, isset($tipe_data_pembanding) ? $tipe_data_pembanding : '', $attributes = array('class'=>'form-control', 'id'=>'id_pembanding')) }}
        </div>
       
        <div class="form-group">
          <input type="hidden" name="tipe_data" value="{{ $tipe_data }}">
          <button id="" name="" class="btn btn-success"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
  </fieldset>
</form>