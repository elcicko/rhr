<ol class="breadcrumb">
  <li><i class="fa fa-file-o"></i> Data Tanah</li>
</ol>
<br>
<ul class="nav nav-tabs">
    <li class="active"><a href="#">Table Format</a></li>
    <li><a href="{{ url('aset/index_peta') }}">Map Format</a></li>
</ul>

<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  var ajaxku;

  function ajaxkota(id) {
    ajaxku = buatajax();
    var url = "{{ url('getkabkota') }}";
    url = url + "/" + id;
    ajaxku.onreadystatechange = cityChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
  }

  function ajaxkecamatan(id) {
    ajaxku = buatajax();
    var url = "{{ url('getkecamatanbykabkota') }}";
    url = url + "/" + id;
    ajaxku.onreadystatechange = districtChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
  }

  function ajaxkelurahan(id) {
    ajaxku = buatajax();
    var url = "{{ url('getkelurahanbykecamatan') }}";
    url = url + "/" + id;
    ajaxku.onreadystatechange = villageChange;
    ajaxku.open("GET", url, true);
    ajaxku.send(null);
  }

  function buatajax() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      }

      if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }

      return null;
  }

  function cityChange() {
      var data;
      if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kabkota").innerHTML = data
        } else {
            document.getElementById("kabkota").value = "<option selected>-- Silahkan Pilih Kota / Kabupaten --</option>";
        }
      }
  }

  function districtChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kecamatan").innerHTML = data
        } else {
            document.getElementById("kecamatan").value = "<option selected>-- Silahkan Pilih Kecamatan --</option>";
        }
    }
  }

  function villageChange() {
    var data;
    if (ajaxku.readyState == 4) {
        data = ajaxku.responseText;
        if (data.length >= 0) {
            document.getElementById("kelurahan").innerHTML = data
        } else {
            document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan --</option>";
        }
    }
  }
</script>
<br><br>
<div class="row"> 
    <div class="col-lg-12">
    @if ($write == 1)
      <!-- <a class="btn btn-success" href="{{ url('aset/create') }}"><i class="fa fa-plus"></i> Tambah Data</a> -->
    @else
    @endif
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
      	<div class="col-lg-12" style="background:#ccc; padding-top: 25px; margin-bottom:20px; margin-top:15px; margin-left: 20px; padding-right: 20px; width: 95%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-4">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="ID Aset / Nama Aset" name="q" id="q">
                    </div>
                    <div class="form-group">
                      {{ Form::select('company', $company, '', $attributes = array('style'=>'width:100%;', 'id'=>'company')) }}
                    </div>

                     <div class="form-group">
                      {{ Form::select('tipe_pembangkit', $tipe_pembangkit, '', $attributes = array('style'=>'width:100%;', 'id'=>'tipe_pembangkit')) }}
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="search"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                   
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      {{ Form::select('atl', $atl, '', $attributes = array('style'=>'width:100%;', 'id'=>'atl')) }}
                    </div>

                    <div class="form-group">
                      {{ Form::select('provinsi', $provinsi, '', $attributes = array('style'=>'width:100%;', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
                    </div>

                    <div class="form-group">
                      {{ Form::select('kabkota', $kabkota, '', $attributes = array('style'=>'width:100%;', 'id'=>'kabkota', 'onchange' => 'ajaxkecamatan(this.options[this.selectedIndex].value)')) }}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      {{ Form::select('kecamatan', $kecamatan, '', $attributes = array('style'=>'width:100%;', 'id'=>'kecamatan', 'onchange' => 'ajaxkelurahan(this.options[this.selectedIndex].value)')) }}
                    </div>
                    
                    <div class="form-group">
                      {{ Form::select('kelurahan', $kelurahan, '', $attributes = array('style'=>'width:100%;', 'id'=>'kelurahan')) }}
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="IMV PSM" name="imv_psm" id="imv_psm">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
    @if(Session::has('message'))
      <br><br>
      <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <center>{{ Session::get('message') }}</center>
      </div>
    @else
    @endif
    <script>
    $(document).ready(function(){
        $('#search').click(function() { oTable.fnReloadAjax (); });
    });
    </script>
    {{
        Datatable::table()
        ->addColumn('#','ID Aset','Nama Aset','Provinsi', 'Kab/Kota', 'Kecamatan', 'Kelurahan','ATL','Kode Company','Tipe Pembangkit','IMV PSM','Action')
        ->setUrl(route('datatable.aset',array('session_user'=>$session_user,'update'=>$update,'delete'=>$delete)))
        ->setCallbacks(["fnServerParams" => 'function ( aoData ) {           
                          aoData.push({ "name": "provinsi", "value": $(\'#provinsi\').val() });
                          aoData.push({ "name": "kabkota", "value": $(\'#kabkota\').val() });
                          aoData.push({ "name": "kecamatan", "value": $(\'#kecamatan\').val() });
                          aoData.push({ "name": "kelurahan", "value": $(\'#kelurahan\').val() });
                          aoData.push({ "name": "company", "value": $(\'#company\').val() });
                          aoData.push({ "name": "atl", "value": $(\'#atl\').val() });
                          aoData.push({ "name": "tipe_pembangkit", "value": $(\'#tipe_pembangkit\').val() });
                          aoData.push({ "name": "imv_psm", "value": $(\'#imv_psm\').val() });
                          aoData.push({ "name": "q", "value": $(\'#q\').val() });
                        }'
                      ])
        ->render()
    }}
  </div>
</div>