<link rel="stylesheet" href="{{ asset('assets/lib/js/leafletjs/leaflet.css') }}">
<script src="{{ asset('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<ol class="breadcrumb">
  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><a href="{{ url('aset') }}"><i class="fa fa-leaf"></i> Data Aset</a></li>
  <li><i class="fa fa-leaf"></i> Detail Aset</li>
</ol>
<br>
<fieldset>
	<div class="row">
	    <div class="col-lg-7">
	      <div class="well" id="map" style="width:100%; height:380px"></div>
	    </div>
	    <div class="col-lg-5">
	      <table class="table" width="100%" cellpadding="5" cellspacing="5" style="font-size:12px;">
	        <tr>
	          <td width="20%">ID Aset</td><td>{{ $aset->id }}</td>
	        </tr>
	        <tr>
	          <td>Kode Aset</td><td>{{ $aset->kode_aset }}</td>
	        </tr>
	        <tr>
	          <td>Nama Aset 1</td><td>{{ $aset->nama_asset_1 }}</td>
	        </tr>
	        <tr>
	          <td>Nama Aset 2</td><td>{{ $aset->nama_asset_2 }}</td>
	        </tr>
	        <tr>
	          <td>ATL</td><td>ATL {{ $aset->atl }}</td>
	        </tr>
	        <tr>
	          <td>Kode Bisnis</td><td>{{ $aset->kode_bisnis }}</td>
	        </tr>
	        <tr>
	          <td>Kode Company</td><td>{{ $aset->kode_company }}</td>
	        </tr>
	        <tr>
	          <td>Keterangan Lokasi</td><td>{{ $aset->keterangan_lokasi }}</td>
	        </tr>
	      </table>
	    </div>
  	</div>
  	<div class="row">
	    <div class="col-lg-9">
	      <div class="btn-group">
	        <button type="button" class="btn btn-danger">Action</button>
	        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          <span class="caret"></span>
	          <span class="sr-only">Toggle Dropdown</span>
	        </button>
	        <ul class="dropdown-menu">
	          <li><a href="<?php echo url('aset/survey/'.$aset->id.''); ?>"><i class="fa fa-wrench"></i> Perbaharui Data Aset</a></li>
	          @if (isset($pembanding1) && count($pembanding1) > 0)
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_1'); ?>" ><i class="fa fa-wrench"></i> Ubah Data Pembanding 1</a></li>
	          @else
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_1'); ?>" ><i class="fa fa-plus"></i> Tambahkan Data Pembanding 1</a></li>
	          @endif
	          @if (isset($pembanding2) && count($pembanding2) > 0)
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_2'); ?>" ><i class="fa fa-wrench"></i> Ubah Data Pembanding 2</a></li>
	          @else
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_2'); ?>" ><i class="fa fa-plus"></i> Tambahkan Data Pembanding 2</a></li>
	          @endif
	          @if (isset($pembanding3) && count($pembanding3) > 0)
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_3'); ?>" ><i class="fa fa-wrench"></i> Ubah Data Pembanding 3</a></li>
	          @else
	            <li><a href="<?php echo url('aset/cari_pembanding/'.$aset->id.'/pembanding_3'); ?>" ><i class="fa fa-plus"></i> Tambahkan Data Pembanding 3</a></li>
	          @endif
	        </ul>
	      </div>
	    </div>
  	</div>
  	<div class="row">
	    <div class="col-lg-12">
	    </div>
  	</div>
  	<div class="row">
  		<div class="col-lg-12">
  			<legend>ALAMAT PROPERTI</legend>
  			<table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
  				<thead>
	  				<tr bgcolor="#ccc">
	  					<th width="20%"></th>
	  					<th width="20%"><center>Data Properti</center></th>
	  					<th width="20%"><center>Pembanding 1</center></th>
	  					<th width="20%"><center>Pembanding 2</center></th>
	  					<th width="20%"><center>Pembanding 3</center></th>
	  				</tr>
  				</thead>
  				<tbody>
	  				<tr>
	  					<td><b>Nama Kontak Person</b></td>
	  					<td>{{ $aset->kontak_nama}}</td>
	  					<td>{{ isset($pembanding1->name) ? $pembanding1->name : '' }}</td>
	  					<td>{{ isset($pembanding2->name) ? $pembanding2->name : '' }}</td>
	  					<td>{{ isset($pembanding3->name) ? $pembanding3->name : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Telepon</b></td>
	  					<td>{{ $aset->kontak_telepon}}</td>
	  					<td>{{ isset($pembanding1->phone) ? $pembanding1->phone : '' }}</td>
	  					<td>{{ isset($pembanding2->phone) ? $pembanding2->phone : '' }}</td>
	  					<td>{{ isset($pembanding3->phone) ? $pembanding3->phone : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Properti Yang Dianalisis</b></td>
	  					<td>{{ $aset->jenis_properti}}</td>
	  					<td>{{ isset($pembanding1->type) ? $pembanding1->type : '' }}</td>
	  					<td>{{ isset($pembanding2->type) ? $pembanding2->type : '' }}</td>
	  					<td>{{ isset($pembanding3->type) ? $pembanding3->type : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Jenis Data</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->source) ? $pembanding1->source : '' }}</td>
	  					<td>{{ isset($pembanding2->source) ? $pembanding2->source : '' }}</td>
	  					<td>{{ isset($pembanding3->source) ? $pembanding3->source : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Provinsi</b></td>
	  					<td>{{ $aset->provinsi }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Kota / Kabupaten</b></td>
	  					<td>{{ $aset->kota }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Kecamatan</b></td>
	  					<td>{{ $aset->kecamatan }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Kelurahan / Desa</b></td>
	  					<td>{{ $aset->desa }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Alamat Properti</b></td>
	  					<td>{{ $aset->alamat }}</td>
	  					<td>{{ isset($pembanding1->address) ? $pembanding1->address : '' }}</td>
	  					<td>{{ isset($pembanding2->address) ? $pembanding2->address : '' }}</td>
	  					<td>{{ isset($pembanding3->address) ? $pembanding3->address : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Koordinat Latitude</b></td>
	  					<td>{{ $aset->koordinat_latitude }}</td>
	  					<td>{{ isset($pembanding1->latitude) ? $pembanding1->latitude : '' }}</td>
	  					<td>{{ isset($pembanding2->latitude) ? $pembanding2->latitude : '' }}</td>
	  					<td>{{ isset($pembanding3->latitude) ? $pembanding3->latitude : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Koordinat Longitude</b></td>
	  					<td>{{ $aset->koordinat_longitude }}</td>
	  					<td>{{ isset($pembanding1->longitude) ? $pembanding1->longitude : '' }}</td>
	  					<td>{{ isset($pembanding2->longitude) ? $pembanding2->longitude : '' }}</td>
	  					<td>{{ isset($pembanding3->longitude) ? $pembanding3->longitude : '' }}</td>
	  				</tr>
  				</tbody>
  			</table>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-lg-12">
  			<legend>KARAKTERISTIK PROPERTI</legend>
  			<table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
  				<thead>
	  				<tr bgcolor="#ccc">
	  					<th width="20%"></th>
	  					<th width="20%"><center>Data Properti</center></th>
	  					<th width="20%"><center>Pembanding 1</center></th>
	  					<th width="20%"><center>Pembanding 2</center></th>
	  					<th width="20%"><center>Pembanding 3</center></th>
	  				</tr>
  				</thead>
  				<tbody>
	  				<tr>
	  					<td><b>Status Dokumen Kepemilikan</b></td>
	  					<td>{{ $aset->status_dokumen_tanah}}</td>
	  					<td>{{ isset($pembanding1->title_particular) ? $pembanding1->title_particular : '' }}</td>
	  					<td>{{ isset($pembanding2->title_particular) ? $pembanding2->title_particular : '' }}</td>
	  					<td>{{ isset($pembanding3->title_particular) ? $pembanding3->title_particular : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Syarat Pembiayaan</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->term) ? $pembanding1->term : '' }}</td>
	  					<td>{{ isset($pembanding2->term) ? $pembanding2->term : '' }}</td>
	  					<td>{{ isset($pembanding3->term) ? $pembanding3->term : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Kondisi Penjualan</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->sales_condition) ? $pembanding1->sales_condition : '' }}</td>
	  					<td>{{ isset($pembanding2->sales_condition) ? $pembanding2->sales_condition : '' }}</td>
	  					<td>{{ isset($pembanding3->sales_condition) ? $pembanding3->sales_condition : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Pengeluaran Setelah Pembelian</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->other_expenses) ? $pembanding1->other_expenses : '' }}</td>
	  					<td>{{ isset($pembanding2->other_expenses) ? $pembanding2->other_expenses : '' }}</td>
	  					<td>{{ isset($pembanding3->other_expenses) ? $pembanding3->other_expenses : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Kondisi Pasar</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->market_condition) ? $pembanding1->market_condition : '' }}</td>
	  					<td>{{ isset($pembanding2->market_condition) ? $pembanding2->market_condition : '' }}</td>
	  					<td>{{ isset($pembanding3->market_condition) ? $pembanding3->market_condition : '' }}</td>
	  				</tr>
  				</tbody>
  			</table>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-lg-12">
  			<legend>TANAH</legend>
  			<table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
  				<thead>
	  				<tr bgcolor="#ccc">
	  					<th width="20%"></th>
	  					<th width="20%"><center>Data Properti</center></th>
	  					<th width="20%"><center>Pembanding 1</center></th>
	  					<th width="20%"><center>Pembanding 2</center></th>
	  					<th width="20%"><center>Pembanding 3</center></th>
	  				</tr>
  				</thead>
  				<tbody>
	  				<tr>
	  					<td><b>Luas (m2)</b></td>
	  					<td>{{ $aset->luas}}</td>
	  					<td>{{ isset($pembanding1->land_area) ? $pembanding1->land_area : '' }}</td>
	  					<td>{{ isset($pembanding2->land_area) ? $pembanding2->land_area : '' }}</td>
	  					<td>{{ isset($pembanding3->land_area) ? $pembanding3->land_area : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Posisi Tanah</b></td>
	  					<td>{{ $aset->posisi_tanah }}</td>
	  					<td>{{ isset($pembanding1->land_position) ? $pembanding1->land_position : '' }}</td>
	  					<td>{{ isset($pembanding2->land_position) ? $pembanding2->land_position : '' }}</td>
	  					<td>{{ isset($pembanding3->land_position) ? $pembanding3->land_position : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Bentuk Tanah</b></td>
	  					<td>{{ $aset->bentuk_tapak }}</td>
	  					<td>{{ isset($pembanding1->land_shape) ? $pembanding1->land_shape : '' }}</td>
	  					<td>{{ isset($pembanding2->land_shape) ? $pembanding2->land_shape : '' }}</td>
	  					<td>{{ isset($pembanding3->land_shape) ? $pembanding3->land_shape : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Orientasi (Hadap)</b></td>
	  					<td>{{ $aset->orientasi }}</td>
	  					<td>{{ isset($pembanding1->land_orientation) ? $pembanding1->land_orientation : '' }}</td>
	  					<td>{{ isset($pembanding2->land_orientation) ? $pembanding2->land_orientation : '' }}</td>
	  					<td>{{ isset($pembanding3->land_orientation) ? $pembanding3->land_orientation : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Jenis Perkerasan / Accessibility</b></td>
	  					<td>{{ $aset->jenis_perkerasan }}</td>
	  					<td>{{ isset($pembanding1->land_frontage1) ? $pembanding1->land_frontage1 : '' }}</td>
	  					<td>{{ isset($pembanding2->land_frontage1) ? $pembanding2->land_frontage1 : '' }}</td>
	  					<td>{{ isset($pembanding3->land_frontage1) ? $pembanding3->land_frontage1 : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Lebar Jalan</b></td>
	  					<td>{{ $aset->lebar_jalan }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Lokasi</b></td>
	  					<td>{{ $aset->lokasi }}</td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Kondisi Lingkungan</b></td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Potensi Banjir</b></td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  					<td></td>
	  				</tr>
	  				<tr>
	  					<td><b>Peruntukan / Zoning</b></td>
	  					<td>{{ $aset->penggunaan_properti }}</td>
	  					<td>{{ isset($pembanding1->land_zoning) ? $pembanding1->land_zoning : '' }}</td>
	  					<td>{{ isset($pembanding2->land_zoning) ? $pembanding2->land_zoning : '' }}</td>
	  					<td>{{ isset($pembanding3->land_zoning) ? $pembanding3->land_zoning : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Kondisi Topografi</b></td>
	  					<td>{{ $aset->topografi }}</td>
	  					<td>{{ isset($pembanding1->land_topography) ? $pembanding1->land_topography : '' }}</td>
	  					<td>{{ isset($pembanding2->land_topography) ? $pembanding2->land_topography : '' }}</td>
	  					<td>{{ isset($pembanding3->land_topography) ? $pembanding3->land_topography : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Lebar Muka (frontage) - (m2)</b></td>
	  					<td>{{ $aset->lebar }}</td>
	  					<td>{{ isset($pembanding1->land_frontage) ? $pembanding1->land_frontage : '' }}</td>
	  					<td>{{ isset($pembanding2->land_frontage) ? $pembanding2->land_frontage : '' }}</td>
	  					<td>{{ isset($pembanding3->land_frontage) ? $pembanding3->land_frontage : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Perkiraan Harga Properti per m2 (Rp)</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->land_perkiraan) ? $pembanding1->land_perkiraan : '' }}</td>
	  					<td>{{ isset($pembanding2->land_perkiraan) ? $pembanding2->land_perkiraan : '' }}</td>
	  					<td>{{ isset($pembanding3->land_perkiraan) ? $pembanding3->land_perkiraan : '' }}</td>
	  				</tr>
	  				<tr>
	  					<td><b>Indikasi Transaksi Bangunan per m2 (Rp)</b></td>
	  					<td></td>
	  					<td>{{ isset($pembanding1->land_indikasi) ? $pembanding1->land_indikasi : '' }}</td>
	  					<td>{{ isset($pembanding2->land_indikasi) ? $pembanding2->land_indikasi : '' }}</td>
	  					<td>{{ isset($pembanding3->land_indikasi) ? $pembanding3->land_indikasi : '' }}</td>
	  				</tr>
  				</tbody>
  			</table>
  		</div>
  	</div>
  	<div class="row">
      <div class="col-lg-12">
        <legend>BANGUNAN</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr bgcolor="#ccc">
				<th width="20%"></th>
				<th width="20%"><center>Data Properti</center></th>
				<th width="20%"><center>Pembanding 1</center></th>
				<th width="20%"><center>Pembanding 2</center></th>
				<th width="20%"><center>Pembanding 3</center></th>
			</tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Luas Area (m2)</b></td>
              <td>{{ $aset->status_dokumen_tanah}}</td>
              <td>{{ isset($pembanding1->building_area) ? $pembanding1->building_area : '' }}</td>
              <td>{{ isset($pembanding2->building_area) ? $pembanding2->building_area : '' }}</td>
              <td>{{ isset($pembanding3->building_area) ? $pembanding3->building_area : '' }}</td>
            </tr>
            <tr>
              <td><b>Jumlah Lantai</b></td>
              <td>{{ $aset->status_dokumen_tanah}}</td>
              <td>{{ isset($pembanding1->building_storey) ? $pembanding1->building_storey : '' }}</td>
              <td>{{ isset($pembanding2->building_storey) ? $pembanding2->building_storey : '' }}</td>
              <td>{{ isset($pembanding3->building_storey) ? $pembanding3->building_storey : '' }}</td>
            </tr>
            <tr>
              <td><b>Tahun Pembangunan</b></td>
              <td>{{ $aset->status_dokumen_tanah}}</td>
              <td>{{ isset($pembanding1->building_year_constructed) ? $pembanding1->building_year_constructed : '' }}</td>
              <td>{{ isset($pembanding2->building_year_constructed) ? $pembanding2->building_year_constructed : '' }}</td>
              <td>{{ isset($pembanding3->building_year_constructed) ? $pembanding3->building_year_constructed : '' }}</td>
            </tr>
            <tr>
              <td><b>Tahun Renovasi</b></td>
              <td>{{ $aset->status_dokumen_tanah}}</td>
              <td>{{ isset($pembanding1->building_year_renovated) ? $pembanding1->building_year_renovated : '' }}</td>
              <td>{{ isset($pembanding2->building_year_renovated) ? $pembanding2->building_year_renovated : '' }}</td>
              <td>{{ isset($pembanding3->building_year_renovated) ? $pembanding3->building_year_renovated : '' }}</td>
            </tr>
            <tr>
              <td colspan="5" style="background-color:#999;"><b>SPESIFIKASI BANGUNAN</b></td>
            </tr>
            <tr>
              <td><b>Pondasi</b></td>
              <td>{{ $aset->pondasi}}</td>
              <td>{{ isset($pembanding1->pondasi) ? $pembanding1->pondasi : '' }}</td>
              <td>{{ isset($pembanding2->pondasi) ? $pembanding2->pondasi : '' }}</td>
              <td>{{ isset($pembanding3->pondasi) ? $pembanding3->pondasi : '' }}</td>
            </tr>
            <tr>
              <td><b>Struktur</b></td>
              <td>{{ $aset->struktur}}</td>
              <td>{{ isset($pembanding1->struktur) ? $pembanding1->struktur : '' }}</td>
              <td>{{ isset($pembanding2->struktur) ? $pembanding2->struktur : '' }}</td>
              <td>{{ isset($pembanding3->struktur) ? $pembanding3->struktur : '' }}</td>
            </tr>
            <tr>
              <td><b>Lantai</b></td>
              <td>{{ $aset->lantai}}</td>
              <td>{{ isset($pembanding1->lantai) ? $pembanding1->lantai : '' }}</td>
              <td>{{ isset($pembanding2->lantai) ? $pembanding2->lantai : '' }}</td>
              <td>{{ isset($pembanding3->lantai) ? $pembanding3->lantai : '' }}</td>
            </tr>
            <tr>
              <td><b>Dinding</b></td>
              <td>{{ $aset->dinding}}</td>
              <td>{{ isset($pembanding1->dinding) ? $pembanding1->dinding : '' }}</td>
              <td>{{ isset($pembanding2->dinding) ? $pembanding2->dinding : '' }}</td>
              <td>{{ isset($pembanding3->dinding) ? $pembanding3->dinding : '' }}</td>
            </tr>
            <tr>
              <td><b>Pintu & Jendela</b></td>
              <td>{{ $aset->pintu}}</td>
              <td>{{ isset($pembanding1->pintu) ? $pembanding1->pintu : '' }}</td>
              <td>{{ isset($pembanding2->pintu) ? $pembanding2->pintu : '' }}</td>
              <td>{{ isset($pembanding3->pintu) ? $pembanding3->pintu : '' }}</td>
            </tr>
            <tr>
              <td><b>Langit-langit</b></td>
              <td>{{ $aset->langit}}</td>
              <td>{{ isset($pembanding1->langit) ? $pembanding1->langit : '' }}</td>
              <td>{{ isset($pembanding2->langit) ? $pembanding2->langit : '' }}</td>
              <td>{{ isset($pembanding3->langit) ? $pembanding3->langit : '' }}</td>
            </tr>
            <tr>
              <td><b>Rangka Atap</b></td>
              <td>{{ $aset->rangka_atap}}</td>
              <td>{{ isset($pembanding1->rangka_atap) ? $pembanding1->rangka_atap : '' }}</td>
              <td>{{ isset($pembanding2->rangka_atap) ? $pembanding2->rangka_atap : '' }}</td>
              <td>{{ isset($pembanding3->rangka_atap) ? $pembanding3->rangka_atap : '' }}</td>
            </tr>
            <tr>
              <td><b>Penutup Atap</b></td>
              <td>{{ $aset->tutup_atap}}</td>
              <td>{{ isset($pembanding1->penutup_atap) ? $pembanding1->penutup_atap : '' }}</td>
              <td>{{ isset($pembanding2->penutup_atap) ? $pembanding2->penutup_atap : '' }}</td>
              <td>{{ isset($pembanding3->penutup_atap) ? $pembanding3->penutup_atap : '' }}</td>
            </tr>
            <tr>
              <td colspan="5" style="background-color:#999;"><b>PERALATAN BANGUNAN</b></td>
            </tr>
            <tr>
              <td><b>Listrik (Watt)</b></td>
              <td>{{ $aset->listrik}}</td>
              <td>{{ isset($pembanding1->equipment_electricity) ? $pembanding1->equipment_electricity : '' }}</td>
              <td>{{ isset($pembanding2->equipment_electricity) ? $pembanding2->equipment_electricity : '' }}</td>
              <td>{{ isset($pembanding3->equipment_electricity) ? $pembanding3->equipment_electricity : '' }}</td>
            </tr>
            <tr>
              <td><b>AC (Unit)</b></td>
              <td>{{ $aset->ac}}</td>
              <td>{{ isset($pembanding1->equipment_ac) ? $pembanding1->equipment_ac : '' }}</td>
              <td>{{ isset($pembanding2->equipment_ac) ? $pembanding2->equipment_ac : '' }}</td>
              <td>{{ isset($pembanding3->equipment_ac) ? $pembanding3->equipment_ac : '' }}</td>
            </tr>
            <tr>
              <td><b>Air</b></td>
              <td>{{ $aset->air}}</td>
              <td>{{ isset($pembanding1->equipment_water) ? $pembanding1->equipment_water : '' }}</td>
              <td>{{ isset($pembanding2->equipment_water) ? $pembanding2->equipment_water : '' }}</td>
              <td>{{ isset($pembanding3->equipment_water) ? $pembanding3->equipment_water : '' }}</td>
            </tr>
            <tr>
              <td><b>Telepon (Saluran)</b></td>
              <td>{{ $aset->telepon}}</td>
              <td>{{ isset($pembanding1->equipment_telephone) ? $pembanding1->equipment_telephone : '' }}</td>
              <td>{{ isset($pembanding2->equipment_telephone) ? $pembanding2->equipment_telephone : '' }}</td>
              <td>{{ isset($pembanding3->equipment_telephone) ? $pembanding3->equipment_telephone : '' }}</td>
            </tr>
            <tr>
              <td><b>Lainnya</b></td>
              <td>{{ $aset->lainnya}}</td>
              <td>{{ isset($pembanding1->equipment_others) ? $pembanding1->equipment_others : '' }}</td>
              <td>{{ isset($pembanding2->equipment_others) ? $pembanding2->equipment_others : '' }}</td>
              <td>{{ isset($pembanding3->equipment_others) ? $pembanding3->equipment_others : '' }}</td>
            </tr>
            <tr>
              <td><b>Karakteristik Ekonomi</b></td>
              <td></td>
              <td>{{ isset($pembanding1->equipment_economy) ? $pembanding1->equipment_economy : '' }}</td>
              <td>{{ isset($pembanding2->equipment_economy) ? $pembanding2->equipment_economy : '' }}</td>
              <td>{{ isset($pembanding3->equipment_economy) ? $pembanding3->equipment_economy : '' }}</td>
            </tr>
            <tr>
              <td><b>Penjualan Komponen Non-Reality</b></td>
              <td></td>
              <td>{{ isset($pembanding1->equipment_non) ? $pembanding1->equipment_non : '' }}</td>
              <td>{{ isset($pembanding2->equipment_non) ? $pembanding2->equipment_non : '' }}</td>
              <td>{{ isset($pembanding3->equipment_non) ? $pembanding3->equipment_non : '' }}</td>
            </tr>
            <tr>
              <td colspan="5" style="background-color:#999;"><b>PENAWARAN / DATA TRANSAKSI</b></td>
            </tr>
            <tr>
              <td><b>Tahun / Bulan Penjualan</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_year) ? $pembanding1->offering_year : '' }}</td>
              <td>{{ isset($pembanding2->offering_year) ? $pembanding2->offering_year : '' }}</td>
              <td>{{ isset($pembanding3->offering_year) ? $pembanding3->offering_year : '' }}</td>
            </tr>
            <tr>
              <td><b>Syarat Pembayaran</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_terms) ? $pembanding1->offering_terms : '' }}</td>
              <td>{{ isset($pembanding2->offering_terms) ? $pembanding2->offering_terms : '' }}</td>
              <td>{{ isset($pembanding3->offering_terms) ? $pembanding3->offering_terms : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Properti (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_transacted_price) ? $pembanding1->offering_transacted_price : '' }}</td>
              <td>{{ isset($pembanding2->offering_transacted_price) ? $pembanding2->offering_transacted_price : '' }}</td>
              <td>{{ isset($pembanding3->offering_transacted_price) ? $pembanding3->offering_transacted_price : '' }}</td>
            </tr>
            <tr>
              <td><b>Penawaran Tertinggi</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_highest) ? $pembanding1->offering_highest : '' }}</td>
              <td>{{ isset($pembanding2->offering_highest) ? $pembanding2->offering_highest : '' }}</td>
              <td>{{ isset($pembanding3->offering_highest) ? $pembanding3->offering_highest : '' }}</td>
            </tr>
            <tr>
              <td><b>Potongan Harga (%)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_discount) ? $pembanding1->offering_discount : '' }}</td>
              <td>{{ isset($pembanding2->offering_discount) ? $pembanding2->offering_discount : '' }}</td>
              <td>{{ isset($pembanding3->offering_discount) ? $pembanding3->offering_discount : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Tanah & Bangunan (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_indicative_land_building) ? $pembanding1->offering_indicative_land_building : '' }}</td>
              <td>{{ isset($pembanding2->offering_indicative_land_building) ? $pembanding2->offering_indicative_land_building : '' }}</td>
              <td>{{ isset($pembanding3->offering_indicative_land_building) ? $pembanding3->offering_indicative_land_building : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Bangunan per m2 (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_indicative_building) ? $pembanding1->offering_indicative_building : '' }}</td>
              <td>{{ isset($pembanding2->offering_indicative_building) ? $pembanding2->offering_indicative_building : '' }}</td>
              <td>{{ isset($pembanding3->offering_indicative_building) ? $pembanding3->offering_indicative_building : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Tanah</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_indicative_land) ? $pembanding1->offering_indicative_land : '' }}</td>
              <td>{{ isset($pembanding2->offering_indicative_land) ? $pembanding2->offering_indicative_land : '' }}</td>
              <td>{{ isset($pembanding3->offering_indicative_land) ? $pembanding3->offering_indicative_land : '' }}</td>
            </tr>
            <tr>
              <td><b>Indikasi Transaksi Tanah per m2 (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_indicative_land_qsm) ? $pembanding1->offering_indicative_land_qsm : '' }}</td>
              <td>{{ isset($pembanding2->offering_indicative_land_qsm) ? $pembanding2->offering_indicative_land_qsm : '' }}</td>
              <td>{{ isset($pembanding3->offering_indicative_land_qsm) ? $pembanding3->offering_indicative_land_qsm : '' }}</td>
            </tr>
            <tr>
              <td><b>GIM</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_gim) ? $pembanding1->offering_gim : '' }}</td>
              <td>{{ isset($pembanding2->offering_gim) ? $pembanding2->offering_gim : '' }}</td>
              <td>{{ isset($pembanding3->offering_gim) ? $pembanding3->offering_gim : '' }}</td>
            </tr>
            <tr>
              <td><b>Harga Penawaran (Jual) (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_jual) ? $pembanding1->offering_jual : '' }}</td>
              <td>{{ isset($pembanding2->offering_jual) ? $pembanding2->offering_jual : '' }}</td>
              <td>{{ isset($pembanding3->offering_jual) ? $pembanding3->offering_jual : '' }}</td>
            </tr>
            <tr>
              <td><b>Harga Penawaran (Sewa) (Rp)</b></td>
              <td></td>
              <td>{{ isset($pembanding1->offering_sewa) ? $pembanding1->offering_sewa : '' }}</td>
              <td>{{ isset($pembanding2->offering_sewa) ? $pembanding2->offering_sewa : '' }}</td>
              <td>{{ isset($pembanding3->offering_sewa) ? $pembanding3->offering_sewa : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>PROPERTI YANG DINILAI</legend>
        <table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
          <thead>
            <tr bgcolor="#ccc">
				<th width="20%"></th>
				<th width="20%" colspan="2"><center>Data Properti</center></th>
				<th width="20%" colspan="2"><center>Pembanding 1</center></th>
				<th width="20%" colspan="2"><center>Pembanding 2</center></th>
				<th width="20%" colspan="2"><center>Pembanding 3</center></th>
			</tr>
          </thead>
          <tbody>
          	<tr style="background-color:#999;">
				<td><b>PENYESUAIAN TRANSAKSI</b></td>
				<td colspan="2"></td>
				<td width="10%"><center><b>Persen</b></center></td>
				<td width="10%"><center><b>Nilai</b></center></td>
				<td width="10%"><center><b>Persen</b></center></td>
				<td width="10%"><center><b>Nilai</b></center></td>
				<td width="10%"><center><b>Persen</b></center></td>
				<td width="10%"><center><b>Nilai</b></center></td>
			</tr>
            <tr>
              <td><b>Status Dokumen Kepemilikan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_land_title) ? $pembanding1->adjustment_land_title : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_land_title1) ? $pembanding1->adjustment_land_title1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_title) ? $pembanding2->adjustment_land_title : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_title1) ? $pembanding2->adjustment_land_title1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_title) ? $pembanding3->adjustment_land_title : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_title1) ? $pembanding3->adjustment_land_title1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Syarat Pembiayaan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_syarat_pembiayaan) ? $pembanding1->adjustment_syarat_pembiayaan : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_syarat_pembiayaan1) ? $pembanding1->adjustment_syarat_pembiayaan1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_syarat_pembiayaan) ? $pembanding2->adjustment_syarat_pembiayaan : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_syarat_pembiayaan1) ? $pembanding2->adjustment_syarat_pembiayaan1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_syarat_pembiayaan) ? $pembanding3->adjustment_syarat_pembiayaan : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_syarat_pembiayaan1) ? $pembanding3->adjustment_syarat_pembiayaan1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Kondisi Penjualan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_kondisi_penjualan) ? $pembanding1->adjustment_kondisi_penjualan : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_kondisi_penjualan1) ? $pembanding1->adjustment_kondisi_penjualan1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_kondisi_penjualan) ? $pembanding2->adjustment_kondisi_penjualan : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_kondisi_penjualan1) ? $pembanding2->adjustment_kondisi_penjualan1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_kondisi_penjualan) ? $pembanding3->adjustment_kondisi_penjualan : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_kondisi_penjualan1) ? $pembanding3->adjustment_kondisi_penjualan1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Pengeluaran Setelah Pembelian</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_pengeluaran) ? $pembanding1->adjustment_pengeluaran : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_pengeluaran1) ? $pembanding1->adjustment_pengeluaran1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_pengeluaran) ? $pembanding2->adjustment_pengeluaran : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_pengeluaran1) ? $pembanding2->adjustment_pengeluaran1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_pengeluaran) ? $pembanding3->adjustment_pengeluaran : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_pengeluaran1) ? $pembanding3->adjustment_pengeluaran1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Market Condition</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_market_condition) ? $pembanding1->adjustment_market_condition : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_market_condition1) ? $pembanding1->adjustment_market_condition1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_market_condition) ? $pembanding2->adjustment_market_condition : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_market_condition1) ? $pembanding2->adjustment_market_condition1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_market_condition) ? $pembanding3->adjustment_market_condition : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_market_condition1) ? $pembanding3->adjustment_market_condition1 : '' }}</td>
            </tr>
            <tr>
              <td colspan="9" style="background-color:#999;"><b>PENYESUAIAN PROPERTI</b></td>
            </tr>
            <tr>
              <td><b>Luas Tanah</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_land_area) ? $pembanding1->adjustment_land_area : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_land_area1) ? $pembanding1->adjustment_land_area1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_area) ? $pembanding2->adjustment_land_area : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_area1) ? $pembanding2->adjustment_land_area1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_area) ? $pembanding3->adjustment_land_area : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_area1) ? $pembanding3->adjustment_land_area1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Posisi Tanah</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_posisi_tanah) ? $pembanding1->adjustment_posisi_tanah : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_posisi_tanah1) ? $pembanding1->adjustment_posisi_tanah1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_posisi_tanah) ? $pembanding2->adjustment_posisi_tanah : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_posisi_tanah1) ? $pembanding2->adjustment_posisi_tanah1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_posisi_tanah) ? $pembanding3->adjustment_posisi_tanah : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_posisi_tanah1) ? $pembanding3->adjustment_posisi_tanah1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Bentuk Tanah</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_land_shape) ? $pembanding1->adjustment_land_shape : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_land_shape1) ? $pembanding1->adjustment_land_shape1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_shape) ? $pembanding2->adjustment_land_shape : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_land_shape1) ? $pembanding2->adjustment_land_shape1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_shape) ? $pembanding3->adjustment_land_shape : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_land_shape1) ? $pembanding3->adjustment_land_shape1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Orientasi</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_orientasi) ? $pembanding1->adjustment_orientasi : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_orientasi1) ? $pembanding1->adjustment_orientasi1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_orientasi) ? $pembanding2->adjustment_orientasi : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_orientasi1) ? $pembanding2->adjustment_orientasi1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_orientasi) ? $pembanding3->adjustment_orientasi : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_orientasi1) ? $pembanding3->adjustment_orientasi1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Akses</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_accessibility) ? $pembanding1->adjustment_accessibility : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_accessibility1) ? $pembanding1->adjustment_accessibility1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_accessibility) ? $pembanding2->adjustment_accessibility : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_accessibility1) ? $pembanding2->adjustment_accessibility1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_accessibility) ? $pembanding3->adjustment_accessibility : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_accessibility1) ? $pembanding3->adjustment_accessibility1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Lebar Jalan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_road_width) ? $pembanding1->adjustment_road_width : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_road_width1) ? $pembanding1->adjustment_road_width1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_road_width) ? $pembanding2->adjustment_road_width : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_road_width1) ? $pembanding2->adjustment_road_width1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_road_width) ? $pembanding3->adjustment_road_width : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_road_width1) ? $pembanding3->adjustment_road_width1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Lokasi</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_location) ? $pembanding1->adjustment_location : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_location1) ? $pembanding1->adjustment_location1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_location) ? $pembanding2->adjustment_location : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_location1) ? $pembanding2->adjustment_location1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_location) ? $pembanding3->adjustment_location : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_location1) ? $pembanding3->adjustment_location1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Kondisi Lingkungan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_kondisi_lingkungan) ? $pembanding1->adjustment_kondisi_lingkungan : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_kondisi_lingkungan1) ? $pembanding1->adjustment_kondisi_lingkungan1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_kondisi_lingkungan) ? $pembanding2->adjustment_kondisi_lingkungan : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_kondisi_lingkungan1) ? $pembanding2->adjustment_kondisi_lingkungan1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_kondisi_lingkungan) ? $pembanding3->adjustment_kondisi_lingkungan : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_kondisi_lingkungan1) ? $pembanding3->adjustment_kondisi_lingkungan1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Potensi Banjir</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_potensi_banjir) ? $pembanding1->adjustment_potensi_banjir : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_potensi_banjir1) ? $pembanding1->adjustment_potensi_banjir1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_potensi_banjir) ? $pembanding2->adjustment_potensi_banjir : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_potensi_banjir1) ? $pembanding2->adjustment_potensi_banjir1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_potensi_banjir) ? $pembanding3->adjustment_potensi_banjir : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_potensi_banjir1) ? $pembanding3->adjustment_potensi_banjir1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Peruntukan</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_peruntukan) ? $pembanding1->adjustment_peruntukan : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_peruntukan1) ? $pembanding1->adjustment_peruntukan1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_peruntukan) ? $pembanding2->adjustment_peruntukan : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_peruntukan1) ? $pembanding2->adjustment_peruntukan1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_peruntukan) ? $pembanding3->adjustment_peruntukan : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_peruntukan1) ? $pembanding3->adjustment_peruntukan1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Topografi</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_topography) ? $pembanding1->adjustment_topography : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_topography1) ? $pembanding1->adjustment_topography1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_topography) ? $pembanding2->adjustment_topography : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_topography1) ? $pembanding2->adjustment_topography1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_topography) ? $pembanding3->adjustment_topography : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_topography1) ? $pembanding3->adjustment_topography1 : '' }}</td>
            </tr>
            <tr>
              <td><b>Frontage</b></td>
              <td></td>
              <td></td>
              <td>{{ isset($pembanding1->adjustment_frontage) ? $pembanding1->adjustment_frontage : '' }}</td>
              <td>{{ isset($pembanding1->adjustment_frontage1) ? $pembanding1->adjustment_frontage1 : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_frontage) ? $pembanding2->adjustment_frontage : '' }}</td>
              <td>{{ isset($pembanding2->adjustment_frontage1) ? $pembanding2->adjustment_frontage1 : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_frontage) ? $pembanding3->adjustment_frontage : '' }}</td>
              <td>{{ isset($pembanding3->adjustment_frontage1) ? $pembanding3->adjustment_frontage1 : '' }}</td>
            </tr>
            <tr bgcolor="#ccc">
              <td colspan="3"><b>Total Penyesuaian</b></td>
              <td>{{ isset($pembanding1->total_adjusment) ? $pembanding1->total_adjusment : '' }}</td>
              <td>{{ isset($pembanding1->total_adjusment1) ? $pembanding1->total_adjusment1 : '' }}</td>
              <td>{{ isset($pembanding2->total_adjusment) ? $pembanding2->total_adjusment : '' }}</td>
              <td>{{ isset($pembanding2->total_adjusment1) ? $pembanding2->total_adjusment1 : '' }}</td>
              <td>{{ isset($pembanding3->total_adjusment) ? $pembanding3->total_adjusment : '' }}</td>
              <td>{{ isset($pembanding3->total_adjusment1) ? $pembanding3->total_adjusment1 : '' }}</td>
            </tr>
            <tr bgcolor="#ccc">
              <td colspan="3"><b>Nilai Yang Setara Yang Disesuaikan (per m2)</b></td>
              <td colspan="2">{{ isset($pembanding1->adjustment_value) ? $pembanding1->adjustment_value : '' }}</td>
              <td colspan="2">{{ isset($pembanding2->adjustment_value) ? $pembanding2->adjustment_value : '' }}</td>
              <td colspan="2">{{ isset($pembanding3->adjustment_value) ? $pembanding3->adjustment_value : '' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
      	<table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
      		<tr>
      			<td width="20%"><b>Indikasi Nilai Pasar</b></td><td width="45%">{{ isset($aset->imv) ? $aset->imv : '' }}</td>
      		</tr>
      		<tr>
      			<td width="20%"><b>Indikasi Nilai Pasar Dibulatkan</b></td><td width="45%">{{ isset($aset->imv_bulat) ? $aset->imv_bulat : '' }}</td>
      		</tr>
      		<tr>
      			<td width="20%"><b>Indikasi Nilai Pasar/m2</b></td><td width="45%">{{ isset($aset->imv_psm) ? $aset->imv_psm : '' }}</td>
      		</tr>
      	</table>
      	</div>
      	<div class="col-lg-6">
      	<table class="table table-hovered table-bordered" width="100%" cellpadding="5" cellspacing="5">
      		<tr>
      			<td width="20%"><b>Nomor Objek Pajak</b></td><td width="45%">{{ isset($aset->nop) ? $aset->nop : '' }}</td>
      		</tr>
      		<tr>
      			<td width="20%"><b>Nilai Jual Objek Pajak</b></td><td width="45%">{{ isset($aset->njop) ? $aset->njop : '' }}</td>
      		</tr>
      		<tr>
      			<td width="20%"><b>HBU Terbangun</b></td><td width="45%">{{ isset($aset->hbu_terbangun) ? $aset->hbu_terbangun : '' }}</td>
      		</tr>
      		<tr>
      			<td width="20%"><b>HBU Kosong</b></td><td width="45%">{{ isset($aset->hbu_kosong) ? $aset->hbu_kosong : '' }}</td>
      		</tr>
      	</table>
      </div>
      </div>
    </div>
</fieldset>
<script>
var map;
var marker;
$(document).ready(function(){
	map = L.map('map').setView([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : ''}}], 15);
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
	}).addTo(map);   
	marker = L.marker([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : ''}} ], { draggable :false }).addTo(map)

	var iconpembanding = L.icon({iconUrl: '{{ url("assets/img/marker_location.png") }}'});

	@if (isset($pembanding) AND !empty($pembanding))
	  <?php $i = 0; ?>
	  @foreach($pembanding as $val)
	    var marker{{ $i }} = L.marker([{{ isset($val->latitude) ? $val->latitude : '' }},{{ isset($val->longitude) ? $val->longitude : ''}} ], { icon :iconpembanding, draggable :false }).addTo(map)
	    var contentString{{ $i }} = [
	                                  '<div">',
	                                    '<table width="100%" cellpadding="3" cellspacing="3">',
	                                        '<tr>',
	                                            '<td>',
	                                            '<?php echo $val->type_data; ?>',
	                                            '</td>',
	                                        '</tr>',
	                                    '</table>',
	                                  '</div>'
	                                ].join('');

	    marker{{ $i }}.bindPopup(contentString{{ $i }});                         
	    <?php $i++; ?>
	  @endforeach
	@else
	@endif
});
</script>