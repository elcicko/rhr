<link rel="stylesheet" href="<?php echo asset('assets/lib/js/leafletjs/leaflet.css'); ?>">
<script src="<?php echo asset('assets/lib/js/leafletjs/leaflet.js'); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="{{ url('assets/libjs/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<style type="text/css">
  #map {
    width: 100%;
    height: 100%;
    position: relative;
    top: 0;
    left: 0;
    transition: height 0.5s ease-in-out;
}
</style>
<ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('aset') }}"><i class="fa fa-leaf"></i> Manajemen Aset</a></li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<script type="text/javascript">
  var ajaxku;

  function ajaxkota(id) {
      ajaxku = buatajax();
      var url = "{{ url('getkabkota') }}";
      url = url + "/" + id;
      ajaxku.onreadystatechange = cityChange;
      ajaxku.open("GET", url, true);
      ajaxku.send(null);
  }

  function ajaxkecamatan(id) {
      ajaxku = buatajax();
      var url = "{{ url('getkecamatanbykabkota2') }}";
      url = url + "/" + id;
      ajaxku.onreadystatechange = districtChange;
      ajaxku.open("GET", url, true);
      ajaxku.send(null);
  }

  function ajaxkelurahan(id) {
      ajaxku = buatajax();
      var url = "{{ url('getkelurahanbykecamatan') }}";
      url = url + "/" + id;
      ajaxku.onreadystatechange = villageChange;
      ajaxku.open("GET", url, true);
      ajaxku.send(null);
  }

  function buatajax() {
    if (window.XMLHttpRequest) {
      return new XMLHttpRequest();
    }

    if (window.ActiveXObject) {
      return new ActiveXObject("Microsoft.XMLHTTP");
    }

    return null;
  }

  function cityChange() {
      var data;
      if (ajaxku.readyState == 4) {
          data = ajaxku.responseText;
          if (data.length >= 0) {
              document.getElementById("kota").innerHTML = data
          } else {
              document.getElementById("kota").value = "<option selected>-- Pilih Kota / Kabupaten --</option>";
          }
      }
  }

  function districtChange() {
      var data;
      if (ajaxku.readyState == 4) {
          data = ajaxku.responseText;
          if (data.length >= 0) {
              document.getElementById("kecamatan").innerHTML = data
          } else {
              document.getElementById("kecamatan").value = "<option selected>-- Pilih Kecamatan --</option>";
          }
      }
  }

  function villageChange() {
      var data;
      if (ajaxku.readyState == 4) {
          data = ajaxku.responseText;
          if (data.length >= 0) {
              document.getElementById("desa").innerHTML = data
          } else {
              document.getElementById("desa").value = "<option selected>-- Pilih Kelurahan / Desa --</option>";
          }
      }
  }
</script>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
  <fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="row">
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Aset 1</label>  
          <input id="nama_asset_1" name="nama_asset_1" value="{{ isset($aset->nama_asset_1) ? $aset->nama_asset_1 : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Nama Aset 2</label>  
          <input id="nama_asset_2" name="nama_asset_2" value="{{ isset($aset->nama_asset_2) ? $aset->nama_asset_2 : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Kode Bisnis</label>  
          {{ Form::select('kode_bisnis', $kodebisnis, isset($aset->kode_bisnis) ? $aset->kode_bisnis : '', $attributes = array('class'=>'form-control', 'id'=>'kode_bisnis')) }}
        </div>

        <div class="form-group">
          <label for="order">Kode Company</label>  
          {{ Form::select('company', $company, isset($aset->kode_company) ? $aset->kode_company : '', $attributes = array('class'=>'form-control', 'id'=>'kode_company')) }}
        </div>

        <div class="form-group">
          <label for="order">Kode Asset</label>  
          <input id="kode_aset" name="kode_aset" value="{{ isset($aset->kode_aset) ? $aset->kode_aset : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">ATL</label>  
          {{ Form::select('atl', $atl, isset($aset->atl) ? $aset->atl : '', $attributes = array('class'=>'form-control', 'id'=>'atl')) }}
        </div>

        <div class="form-group">
          <label for="order">Keterangan Lokasi</label>  
          <input id="keterangan_lokasi" name="keterangan_lokasi" value="{{ isset($aset->keterangan_lokasi) ? $aset->keterangan_lokasi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Alamat</label>  
          <input id="alamat" name="alamat" value="{{ isset($aset->alamat) ? $aset->alamat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
         <label for="order">Provinsi</label>  
         {{ Form::select('provinsi', $provinsi, isset($aset->provinsi) ? $aset->provinsi : '', $attributes = array('class'=>'form-control', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
        </div>
     
        <div class="form-group">
         <label for="order">Kabupaten / Kota</label>  
         {{ Form::select('kota', $kabkota, isset($aset->kota) ? $aset->kota : '', $attributes = array('class'=>'form-control', 'id'=>'kota', 'onchange' => 'ajaxkecamatan(this.options[this.selectedIndex].value)')) }}
        </div>

        <div class="form-group">
         <label for="order">Kecamatan</label>  
         {{ Form::select('kecamatan', $kecamatan, isset($aset->kecamatan) ? $aset->kecamatan : '', $attributes = array('class'=>'form-control', 'id'=>'kecamatan', 'onchange' => 'ajaxkelurahan(this.options[this.selectedIndex].value)')) }}
        </div>

        <div class="form-group">
          <label for="order">Desa</label>  
          {{ Form::select('desa', $desa, isset($aset->desa) ? $aset->desa : '', $attributes = array('class'=>'form-control', 'id'=>'desa')) }}
        </div>

        <div class="form-group">
          <label for="order">Koordinat Latitude</label>  
          <input id="latitude" name="latitude" value="{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
     
        <div class="form-group">
         <label for="order">Koordinat Longitude</label>  
         <input id="longitude" name="longitude" value="{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Data</button>
        </div>
      </div>
      <div class="col-lg-8">
        <div id="map" style="width:100%; height:890px;"></div>
      </div>
    </div>
  </fieldset>
</form>
<script>
var map;
var marker;

$(document).ready(function(){
  <?php
  if ($form_title == "add") {
  ?>
    map = L.map('map').setView([-2.548926,118.0148634], 5);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);   
    marker = L.marker([-2.548926,118.0148634], { draggable :true }).addTo(map) 
  <?php 
  } elseif ($form_title == "edit") {
  ?>
    map = L.map('map').setView([<?php echo isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926'; ?>,<?php echo isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634'; ?>], 15);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);   
    marker = L.marker([<?php echo isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926'; ?>,<?php echo isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634'; ?>], { draggable :true }).addTo(map) 
  <?php 
  }
  ?>

  marker.on('dragend', function(event){
      var marker = event.target;
      var lat = marker.getLatLng().lat;
      var lng = marker.getLatLng().lng;
      document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
  });
  map.addLayer(marker);

});
</script>