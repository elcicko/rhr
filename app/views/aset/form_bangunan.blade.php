<link rel="stylesheet" href="{{ asset('assets/lib/js/leafletjs/leaflet.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lib/js/datepicker/bootstrap-datepicker.css') }}">
<script src="{{ asset('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/lib/js/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/form_bangunan.js') }}" type="text/javascript"></script>
<style type="text/css">
    #map {
        width: 100%;
        height: 100%;
        position: relative;
        top: 0;
        left: 0;
        transition: height 0.5s ease-in-out;
    }
</style>
<ol class="breadcrumb">
    <li><a href="{{ url('aset') }}"><i class="fa fa-leaf"></i> Data Aset</a></li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
    <fieldset>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="order">Tanggal Penilaian</label>  
                    <input id="tanggal_penilaian" name="tanggal_penilaian" value="{{ isset($aset->tanggal_penilaian) ? $aset->tanggal_penilaian : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Kode Bisnis</label>  
                    {{ Form::select('kode_bisnis', $kodebisnis, isset($aset->kode_bisnis) ? $aset->kode_bisnis : '', $attributes = array('class'=>'form-control', 'id'=>'kode_bisnis')) }}
                </div>
                
                <div class="form-group">
                    <label for="order">Kode Aset</label>  
                    <input id="kode_aset" name="kode_aset" value="{{ isset($aset->kode_aset) ? $aset->kode_aset : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Kode Lokasi</label>  
                    <input id="keterangan_lokasi" name="keterangan_lokasi" value="{{ isset($aset->keterangan_lokasi) ? $aset->keterangan_lokasi : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">ATL</label>  
                    {{ Form::select('atl', $atl, isset($aset->atl) ? $aset->atl : '', $attributes = array('class'=>'form-control', 'id'=>'atl')) }}
                </div>

                <div class="form-group">
                    <label for="order">Tipe Aset</label>
                    {{ Form::select('tipe_aset', $jenis_asset, isset($aset->tipe_aset) ? $aset->tipe_aset : '', $attributes = array('class'=>'form-control', 'id'=>'tipe_aset')) }}
                </div>

                <div class="form-group">
                  <label for="order">Koordinat Latitude</label>  
                  <input id="latitude" name="latitude" value="{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>
             
                <div class="form-group">
                 <label for="order">Koordinat Longitude</label>  
                 <input id="longitude" name="longitude" value="{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>
            
            <div class="col-lg-8">
                <div id="map" style="width:100%; height:450px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <legend>DATA UMUM</legend>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="order">Nama Bangunan</label>  
                    <input id="nama_asset_1" name="nama_asset_1" value="{{ isset($aset->nama_asset_1) ? $aset->nama_asset_1 : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Luas</label>  
                    <input id="luas" name="luas" value="{{ isset($aset->luas) ? $aset->luas : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Tahun Selesai Di Bangun</label>  
                    <input id="tahun_selesai_dibangun" name="tahun_selesai_dibangun" value="{{ isset($aset->tahun_selesai_dibangun) ? $aset->tahun_selesai_dibangun : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Tahun Renovasi</label>  
                    <input id="tahun_renovasi" name="tahun_renovasi" value="{{ isset($aset->tahun_renovasi) ? $aset->tahun_renovasi : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Jumlah Lantai</label>  
                    {{ Form::select('jumlah_lantai', $jumlah_lantai, isset($aset->jumlah_lantai) ? $aset->jumlah_lantai : '', $attributes = array('class'=>'form-control', 'id'=>'jumlah_lantai')) }}
                </div>

                <div class="form-group">
                    <label for="order">Jenis Bangunan</label>
                    {{ Form::select('jenis_bangunan', $jenis_bangunan, isset($aset->jenis_bangunan) ? $aset->jenis_bangunan : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_bangunan')) }}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="order">Alamat</label>  
                    <input id="alamat" name="alamat" value="{{ isset($aset->alamat) ? $aset->alamat : ''; }}" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Provinsi</label>  
                    {{ Form::select('provinsi', $provinsi, isset($aset->provinsi) ? $aset->provinsi : '', $attributes = array('class'=>'form-control', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
                </div>

                <div class="form-group">
                    <label for="order">Kabupaten / Kota</label>  
                    {{ Form::select('kota', $kabkota, isset($aset->kota) ? $aset->kota : '', $attributes = array('class'=>'form-control', 'id'=>'kota', 'onchange' => 'ajaxkecamatan(this.options[this.selectedIndex].value)')) }}
                </div>

                <div class="form-group">
                    <label for="order">Kecamatan</label>  
                    {{ Form::select('kecamatan', $kecamatan, isset($aset->kecamatan) ? $aset->kecamatan : '', $attributes = array('class'=>'form-control', 'id'=>'kecamatan', 'onchange' => 'ajaxkelurahan(this.options[this.selectedIndex].value)')) }}
                </div>

                <div class="form-group">
                    <label for="order">Kelurahan / Desa</label>  
                    {{ Form::select('desa', $desa, isset($aset->desa) ? $aset->desa : '', $attributes = array('class'=>'form-control', 'id'=>'desa')) }}
                </div>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-lg-12">
                <legend>SPESIFIKASI</legend>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="order">Pondasi</label>  
                    {{ Form::select('pondasi', $pondasi, isset($aset->pondasi) ? $aset->pondasi : '', $attributes = array('class'=>'form-control', 'id'=>'pondasi')) }}
                </div>

                <div class="form-group">
                    <label for="order">Struktur</label>  
                    {{ Form::select('struktur', $struktur, isset($aset->struktur) ? $aset->struktur : '', $attributes = array('class'=>'form-control', 'id'=>'struktur')) }}
                </div>

                <div class="form-group">
                    <label for="order">Lantai</label>  
                    {{ Form::select('lantai', $lantai, isset($aset->lantai) ? $aset->lantai : '', $attributes = array('class'=>'form-control', 'id'=>'lantai')) }}
                </div>

                <div class="form-group">
                    <label for="order">Dinding</label>  
                    {{ Form::select('dinding', $dinding, isset($aset->dinding) ? $aset->dinding : '', $attributes = array('class'=>'form-control', 'id'=>'dinding')) }}
                </div>

                <div class="form-group">
                    <label for="order">Pintu & Jendela</label>  
                    {{ Form::select('pintu', $pintu, isset($aset->pintu) ? $aset->pintu : '', $attributes = array('class'=>'form-control', 'id'=>'pintu')) }}
                </div>

                <div class="form-group">
                    <label for="order">Langit-langit</label>  
                    {{ Form::select('langit', $langit, isset($aset->langit) ? $aset->langit : '', $attributes = array('class'=>'form-control', 'id'=>'langit')) }}
                </div>

                <div class="form-group">
                    <label for="order">Rangka Atap</label>  
                    {{ Form::select('rangka_atap', $rangka_atap, isset($aset->rangka_atap) ? $aset->rangka_atap : '', $attributes = array('class'=>'form-control', 'id'=>'rangka_atap')) }}
                </div>

                <div class="form-group">
                    <label for="order">Penutup Atap</label>  
                    {{ Form::select('tutup_atap', $tutup_atap, isset($aset->tutup_atap) ? $aset->tutup_atap : '', $attributes = array('class'=>'form-control', 'id'=>'tutup_atap')) }}
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="pondasi_kondisi" name="pondasi_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="struktur_kondisi" name="struktur_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="lantai_kondisi" name="lantai_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="dinding_kondisi" name="dinding_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="pintu_kondisi" name="pintu_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="langit_kondisi" name="langit_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="rangka_atap_kondisi" name="rangka_atap_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="order">Kondisi</label>
                    <div class="input-group">
                        <input type="text" id="tutup_atap_kondisi" name="tutup_atap_kondisi" class="form-control" placeholder="" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1">%</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="pondasi_nilai" name="pondasi_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="struktur_nilai" name="struktur_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="lantai_nilai" name="lantai_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="dinding_nilai" name="dinding_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="pintu_nilai" name="pintu_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="langit_nilai" name="langit_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="rangka_atap_nilai" name="rangka_atap_nilai" type="text" placeholder="" class="form-control input-md">
                </div>

                <div class="form-group">
                    <label for="order">Nilai</label>  
                    <input id="tutup_atap_nilai" name="tutup_atap_nilai" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label for="order">Kondisi</label>  
                    <input id="kondisi" name="kondisi" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label for="order">Utilitas</label>  
                    <input id="utilitas" name="utilitas" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label for="order">Dep. Fisik</label>  
                    <input id="dep_fisik" name="dep_fisik" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label for="order">Dep. Fungsi</label>  
                    <input id="dep_fungsi" name="dep_fungsi" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-group">
                    <label for="order">Dep. External</label>  
                    <input id="dep_external" name="dep_external" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="order">Indikasi Nilai Pasar</label>  
                    <input id="indikasi_nilai_pasar" name="indikasi_nilai_pasar" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="order">Sisa Umur Ekonomi</label>  
                    <input id="sisa_umur_ekonomi" name="sisa_umur_ekonomi" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

        </div>
        <br><br>
        <div class="row">
            <div class="col-lg-12">
                <legend>FASILITAS GEDUNG</legend>
            </div>
            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Listrik</label>
                    {{ Form::select('listrik', $listrik, isset($aset->listrik) ? $aset->listrik : '', $attributes = array('class'=>'form-control', 'id'=>'listrik')) }}
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <input id="listrik_lainnya" style="margin-top: 22px;" name="listrik_lainnya" value="{{ isset($aset->listrik_lainnya) ? $aset->listrik_lainnya : '' }}" type="text" placeholder="Isi disini jika jenis listrik adalah lainnya" class="form-control input-md" readonly="readonly"> 
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Kapasitas Listrik</label>  
                    <input id="kapasitas_listrik" name="kapasitas_listrik" value="{{ isset($aset->kapasitas_listrik) ? $aset->kapasitas_listrik : '' }}" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Total Saluran Telepon</label>
                    <input id="total_saluran_telepon" name="total_saluran_telepon" value="{{ isset($aset->total_saluran_telepon) ? $aset->total_saluran_telepon : '' }}" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Saluran Fax</label>  
                    <input id="saluran_fax" name="saluran_fax" value="{{ isset($aset->saluran_fax) ? $aset->saluran_fax : '' }}" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">PBAX</label>  
                    <input id="pbax" name="pbax" value="{{ isset($aset->pbax) ? $aset->pbax : '' }}" type="text" placeholder="" class="form-control input-md">
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Air</label>  
                    {{ Form::select('air', $air, isset($aset->air) ? $aset->air : '', $attributes = array('class'=>'form-control', 'id'=>'air')) }}
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <input id="air_lainnya" style="margin-top: 22px;" name="air_lainnya" value="{{ isset($aset->air_lainnya) ? $aset->air_lainnya : '' }}" type="text" placeholder="Isi disini jika sumber air adalah lainnya" class="form-control input-md" readonly="readonly"> 
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Air Conditioner</label>  
                    {{ Form::select('ac', $ac, isset($aset->ac) ? $aset->ac : '', $attributes = array('class'=>'form-control', 'id'=>'ac')) }}
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <input id="ac_lainnya" style="margin-top: 22px;" name="ac_lainnya" value="{{ isset($aset->ac_lainnya) ? $aset->ac_lainnya : '' }}" type="text" placeholder="Isi disini jika jenis AC adalah lainnya" class="form-control input-md" readonly="readonly"> 
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Saluran Pembuangan Limbah</label>  
                    {{ Form::select('limbah', $limbah, isset($aset->limbah) ? $aset->limbah : '', $attributes = array('class'=>'form-control', 'id'=>'limbah')) }}
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <input id="limbah_lainnya" style="margin-top: 22px;" name="limbah_lainnya" value="{{ isset($aset->limbah_lainnya) ? $aset->limbah_lainnya : '' }}" type="text" placeholder="Isi disini jika jenis AC adalah lainnya" class="form-control input-md" readonly="readonly"> 
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Sistem Pemadam Kebakaran</label>  
                    {{ Form::select('pemadam_kebakaran', $pemadam_kebakaran, isset($aset->pemadam_kebakaran) ? $aset->pemadam_kebakaran : '', $attributes = array('class'=>'form-control', 'id'=>'pemadam_kebakaran')) }}
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <input id="pemadam_kebakaran_lainnya" style="margin-top: 22px;" name="pemadam_kebakaran_lainnya" value="{{ isset($aset->pemadam_kebakaran_lainnya) ? $aset->pemadam_kebakaran_lainnya : '' }}" type="text" placeholder="Isi disini jika jenis AC adalah lainnya" class="form-control input-md" readonly="readonly"> 
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Penumpang (Merk)</label>  
                    <input id="lift_penumpang_merk" name="lift_penumpang_merk" value="{{ isset($aset->lift_penumpang_merk) ? $aset->lift_penumpang_merk : '' }}" type="text" placeholder="" class="form-control input-md">   
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Penumpang (Kapasitas Orang)</label>  
                    <input id="lift_penumpang_kap_org" name="lift_penumpang_kap_org" value="{{ isset($aset->lift_penumpang_kap_org) ? $aset->lift_penumpang_kap_org : '' }}" type="text" placeholder="" class="form-control input-md">   
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Penumpang (Kapasitas Kilogram)</label>  
                    <input id="lift_penumpang_kap_kg" name="lift_penumpang_kap_kg" value="{{ isset($aset->lift_penumpang_kap_kg) ? $aset->lift_penumpang_kap_kg : '' }}" type="text" placeholder="" class="form-control input-md"> 
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Penumpang (Unit)</label>  
                    <input id="lift_penumpang_unit" name="lift_penumpang_unit" value="{{ isset($aset->lift_penumpang_unit) ? $aset->lift_penumpang_unit : '' }}" type="text" placeholder="" class="form-control input-md">  
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Barang (Merk)</label>  
                    <input id="lift_barang_merk" name="lift_barang_merk" value="{{ isset($aset->lift_barang_merk) ? $aset->lift_barang_merk : '' }}" type="text" placeholder="" class="form-control input-md">   
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Barang (Kapasitas Orang)</label>  
                    <input id="lift_barang_kap_org" name="lift_barang_kap_org" value="{{ isset($aset->lift_barang_kap_org) ? $aset->lift_barang_kap_org : '' }}" type="text" placeholder="" class="form-control input-md">   
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Barang (Kapasitas Kilogram)</label>  
                    <input id="lift_barang_kap_org" name="lift_barang_kap_org" value="{{ isset($aset->lift_barang_kap_org) ? $aset->lift_barang_kap_org : '' }}" type="text" placeholder="" class="form-control input-md"> 
                </div>
            </div>

            <div class="col-lg-3" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Lift Barang (Unit)</label>  
                    <input id="lift_barang_kap_org" name="lift_barang_kap_org" value="{{ isset($aset->lift_barang_kap_org) ? $aset->lift_barang_kap_org : '' }}" type="text" placeholder="" class="form-control input-md">  
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Building Automation System (Type / Model)</label>  
                    <input id="bas_type" name="bas_type" value="{{ isset($aset->bas_type) ? $aset->bas_type : '' }}" type="text" placeholder="" class="form-control input-md"> 
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Building Automation System (Type / Model)</label>  
                    <input id="bas_merk" name="bas_merk" value="{{ isset($aset->bas_merk) ? $aset->bas_merk : '' }}" type="text" placeholder="" class="form-control input-md">  
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">CCTV (unit)</label>  
                    {{ Form::select('cctv', $cctv, isset($aset->cctv) ? $aset->cctv : '', $attributes = array('class'=>'form-control', 'id'=>'cctv')) }}  
                </div>
            </div>

            <div class="col-lg-6" style="margin-bottom: 0px;">
                <div class="form-group">
                    <label for="order">Servis Lainnya</label>  
                    <input id="lainnya" name="lainnya" value="{{ isset($aset->lainnya) ? $aset->lainnya : '' }}" type="text" placeholder="" class="form-control input-md">  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Data</button>
            </div>
        </div>
    </fieldset>
</form>
<script>
var map;
var marker;
$(document).ready(function () {
    @if ($form_title == "add"))
        map = L.map('map').setView([-2.548926, 118.0148634], 5);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        marker = L.marker([-2.548926, 118.0148634], {draggable: true}).addTo(map)
    @elseif ($form_title == "edit")
        map = L.map('map').setView([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634' }}], 15);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        marker = L.marker([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634' }}], {draggable: true}).addTo(map)
    @endif

    marker.on('dragend', function (event) {
        var marker = event.target;
        var lat = marker.getLatLng().lat;
        var lng = marker.getLatLng().lng;
        document.getElementById('latitude').value = lat;
        document.getElementById('longitude').value = lng;
    });
    map.addLayer(marker);
});
</script>