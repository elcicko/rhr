<link rel="stylesheet" href="{{ asset('assets/lib/js/leafletjs/leaflet.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lib/js/datepicker/bootstrap-datepicker.css') }}">
<script src="{{ asset('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/lib/js/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/form_tanah.js') }}" type="text/javascript"></script>
<style type="text/css">
  #map {
    width: 100%;
    height: 100%;
    position: relative;
    top: 0;
    left: 0;
    transition: height 0.5s ease-in-out;
}
</style>
<ol class="breadcrumb">
    <li><a href="{{ url('aset') }}"><i class="fa fa-leaf"></i> Data Aset</a></li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
<fieldset>
    <div class="row">
      <div class="col-lg-12">
        <div id="map" style="width:100%; height:500px;"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>DATA UMUM</legend>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Aset 1</label>  
          <input id="nama_asset_1" name="nama_asset_1" value="{{ isset($aset->nama_asset_1) ? $aset->nama_asset_1 : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Aset 2</label>  
          <input id="nama_asset_2" name="nama_asset_2" value="{{ isset($aset->nama_asset_2) ? $aset->nama_asset_2 : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">ATL</label>  
          {{ Form::select('atl', $atl, isset($aset->atl) ? $aset->atl : '', $attributes = array('class'=>'form-control', 'id'=>'atl')) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <legend>REFERENSI PENILAIAN</legend>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Kode Bisnis</label>  
          {{ Form::select('kode_bisnis', $kodebisnis, isset($aset->kode_bisnis) ? $aset->kode_bisnis : '', $attributes = array('class'=>'form-control', 'id'=>'kode_bisnis')) }}
        </div>

        <div class="form-group">
          <label for="order">Kode Aset</label>  
          <input id="kode_aset" name="kode_aset" value="{{ isset($aset->kode_aset) ? $aset->kode_aset : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Kode Lokasi</label>  
          <input id="keterangan_lokasi" name="keterangan_lokasi" value="{{ isset($aset->keterangan_lokasi) ? $aset->keterangan_lokasi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Klien</label>  
          <input id="kontak_nama" name="kontak_nama" value="{{ isset($aset->kontak_nama) ? $aset->kontak_nama : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
          <label for="order">Tanggal Inspeksi</label>  
          <input id="tanggal_inspeksi" name="tanggal_inspeksi" value="{{ isset($aset->tanggal_inspeksi) ? $aset->tanggal_inspeksi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Jenis Aset</label>  
          {{ Form::select('jenis_asset', $jenis_asset, isset($aset->jenis_asset) ? $aset->jenis_asset : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_asset')) }}
        </div>

      </div>
      
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Telepon Klien</label>  
          <input id="kontak_telepon" name="kontak_telepon" value="{{ isset($aset->kontak_telepon) ? $aset->kontak_telepon : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tanggal Penilaian</label>  
          <input id="tanggal_penilaian" name="tanggal_penilaian" value="{{ isset($aset->tanggal_penilaian) ? $aset->tanggal_penilaian : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Jenis Properti</label>  
          {{ Form::select('jenis_properti', $jenis_properti, isset($aset->jenis_properti) ? $aset->jenis_properti : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_properti')) }}
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>IDENTIFIKASI DAN LOKASI PROPERTI</legend>
      </div>
      <div class="col-lg-4">
          <div class="form-group">
            <label for="order">Alamat Properti</label>  
            <input id="alamat" name="alamat" value="{{ isset($aset->alamat) ? $aset->alamat : ''; }}" type="text" placeholder="" class="form-control input-md">
          </div>

          <div class="form-group">
           <label for="order">Provinsi</label>  
           {{ Form::select('provinsi', $provinsi, isset($aset->provinsi) ? $aset->provinsi : '', $attributes = array('class'=>'form-control', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
         </div>
     
          <div class="form-group">
           <label for="order">Kabupaten / Kota</label>  
           {{ Form::select('kota', $kabkota, isset($aset->kota) ? $aset->kota : '', $attributes = array('class'=>'form-control', 'id'=>'kota', 'onchange' => 'ajaxkecamatan(this.options[this.selectedIndex].value)')) }}
          </div>
      </div>
      <div class="col-lg-4">
          <div class="form-group">
           <label for="order">Kecamatan</label>  
           {{ Form::select('kecamatan', $kecamatan, isset($aset->kecamatan) ? $aset->kecamatan : '', $attributes = array('class'=>'form-control', 'id'=>'kecamatan', 'onchange' => 'ajaxkelurahan(this.options[this.selectedIndex].value)')) }}
          </div>

          <div class="form-group">
            <label for="order">Desa</label>  
            {{ Form::select('desa', $desa, isset($aset->desa) ? $aset->desa : '', $attributes = array('class'=>'form-control', 'id'=>'desa')) }}
          </div>

          <div class="form-group">
            <label for="order">Penggunaan Properti</label>  
            {{ Form::select('penggunaan_properti', $penggunaan_properti, isset($aset->penggunaan_properti) ? $aset->penggunaan_properti : '', $attributes = array('class'=>'form-control', 'id'=>'penggunaan_properti')) }}
          </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Lokasi</label>  
          {{ Form::select('lokasi_aset', $lokasi_aset, isset($aset->lokasi_aset) ? $aset->lokasi_aset : '', $attributes = array('class'=>'form-control', 'id'=>'lokasi_aset')) }}
        </div>

        <div class="form-group">
          <label for="order">Koordinat Latitude</label>  
          <input id="latitude" name="latitude" value="{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
     
        <div class="form-group">
         <label for="order">Koordinat Longitude</label>  
         <input id="longitude" name="longitude" value="{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>AKSES</legend>
      </div>
      <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Lebar Jalan</label>  
            <input id="lebar_jalan" name="lebar_jalan" value="{{ isset($aset->lebar_jalan) ? $aset->lebar_jalan : ''; }}" type="text" placeholder="" class="form-control input-md">
          </div>

          <div class="form-group">
           <label for="order">Jenis Perkerasan</label>  
           {{ Form::select('jenis_perkerasan', $jenis_perkerasan, isset($aset->jenis_perkerasan) ? $aset->jenis_perkerasan : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_perkerasan')) }}
         </div>
     
          <div class="form-group">
           <label for="order">Jumlah Jalur</label>  
           {{ Form::select('jumlah_jalur', $jumlah_jalur, isset($aset->jumlah_jalur) ? $aset->jumlah_jalur : '', $attributes = array('class'=>'form-control', 'id'=>'jumlah_jalur')) }}
          </div>

          <div class="form-group">
           <label for="order">Jumlah Lajur</label>  
           {{ Form::select('jumlah_lajur', $jumlah_lajur, isset($aset->jumlah_lajur) ? $aset->jumlah_lajur : '', $attributes = array('class'=>'form-control', 'id'=>'jumlah_lajur')) }}
          </div>
      </div>
      <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Keberadaan Median</label>  
            {{ Form::select('keberadaan_median', $keberadaan_median, isset($aset->keberadaan_median) ? $aset->keberadaan_median : '', $attributes = array('class'=>'form-control', 'id'=>'keberadaan_median')) }}
          </div>

          <div class="form-group">
            <label for="order">Kepadatan Lalu Lintas</label>  
            {{ Form::select('kepadatan_lalu_lintas', $kepadatan_lalu_lintas, isset($aset->kepadatan_lalu_lintas) ? $aset->kepadatan_lalu_lintas : '', $attributes = array('class'=>'form-control', 'id'=>'kepadatan_lalu_lintas')) }}
          </div>

          <div class="form-group">
            <label for="order">Dilalui Angkutan Umum</label>  
            {{ Form::select('dilalui_angkutan_umum', $dilalui_angkutan_umum, isset($aset->dilalui_angkutan_umum) ? $aset->dilalui_angkutan_umum : '', $attributes = array('class'=>'form-control', 'id'=>'dilalui_angkutan_umum')) }}
          </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>DOKUMEN KEPEMILIKAN</legend>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
         <label for="order">Status Dokumen Tanah</label>  
         {{ Form::select('status_dokumen_tanah', $status_dokumen_tanah, isset($aset->status_dokumen_tanah) ? $aset->status_dokumen_tanah : '', $attributes = array('class'=>'form-control', 'id'=>'status_dokumen_tanah')) }}
        </div>

        <div class="form-group">
          <label for="order">No. Sertifikat</label>  
          <input id="no_sertifikat" name="no_sertifikat" value="{{ isset($aset->no_sertifikat) ? $aset->no_sertifikat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Luas</label>  
          <input id="luas" name="luas" value="{{ isset($aset->luas) ? $aset->luas : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Pemegang</label>  
          <input id="nama_pemegang" name="nama_pemegang" value="{{ isset($aset->nama_pemegang) ? $aset->nama_pemegang : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tanggal Dikeluarkan</label>  
          <input id="tanggal_dikeluarkan" name="tanggal_dikeluarkan" value="{{ isset($aset->tanggal_dikeluarkan) ? $aset->tanggal_dikeluarkan : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tanggal Berakhir</label>  
          <input id="tanggal_berakhir" name="tanggal_berakhir" value="{{ isset($aset->tanggal_berakhir) ? $aset->tanggal_berakhir : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nomor Gambar Situasi</label>  
          <input id="nomor_gambar_situasi" name="nomor_gambar_situasi" value="{{ isset($aset->nomor_gambar_situasi) ? $aset->nomor_gambar_situasi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tanggal Gambar Situasi</label>  
          <input id="tanggal_gambar_situasi" name="tanggal_gambar_situasi" value="{{ isset($aset->tanggal_gambar_situasi) ? $aset->tanggal_gambar_situasi : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Periode Sertifikat (Tahun)</label>  
          <input id="periode_sertifikat" name="periode_sertifikat" value="{{ isset($aset->periode_sertifikat) ? $aset->periode_sertifikat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>DESKRIPSI TAPAK</legend>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
        <label for="order">Orientasi (Hadap)</label>  
         {{ Form::select('orientasi', $orientasi, isset($aset->orientasi) ? $aset->orientasi : '', $attributes = array('class'=>'form-control', 'id'=>'orientasi')) }}
        </div>

        <div class="form-group">
          <label for="order">Lebar (Frontage)</label>  
          <input id="lebar" name="lebar" value="{{ isset($aset->lebar) ? $aset->lebar : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Panjang ke Belakang</label>  
          <input id="panjang_belakang" name="panjang_belakang" value="{{ isset($aset->panjang_belakang) ? $aset->panjang_belakang : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Posisi Tanah</label>  
          {{ Form::select('posisi_tanah', $posisi_tanah, isset($aset->posisi_tanah) ? $aset->posisi_tanah : '', $attributes = array('class'=>'form-control', 'id'=>'posisi_tanah')) }}
        </div>

        <div class="form-group">
          <label for="order">Topografi (Kontur Tanah)</label>  
          {{ Form::select('topografi', $topografi, isset($aset->topografi) ? $aset->topografi : '', $attributes = array('class'=>'form-control', 'id'=>'topografi')) }}
        </div>

      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Bentuk Tapak</label>  
          {{ Form::select('bentuk_tapak', $bentuk_tapak, isset($aset->bentuk_tapak) ? $aset->bentuk_tapak : '', $attributes = array('class'=>'form-control', 'id'=>'bentuk_tapak')) }}
        </div>
        <div class="form-group">
          <label for="order">Kedudukan Tapak</label>  
          {{ Form::select('kedudukan_tapak', $kedudukan_tapak, isset($aset->kedudukan_tapak) ? $aset->kedudukan_tapak : '', $attributes = array('class'=>'form-control', 'id'=>'kedudukan_tapak')) }}
        </div>

        <div class="form-group">
          <label for="order">Listrik</label>  
          {{ Form::select('listrik', $listrik, isset($aset->listrik) ? $aset->listrik : '', $attributes = array('class'=>'form-control', 'id'=>'listrik')) }}
        </div>

        <div class="form-group">
          <label for="order">Air</label>  
         {{ Form::select('air', $air, isset($aset->air) ? $aset->air : '', $attributes = array('class'=>'form-control', 'id'=>'air')) }}
        </div>

        <div class="form-group">
          <label for="order">Telepon</label>  
         {{ Form::select('telepon', $telepon, isset($aset->telepon) ? $aset->telepon : '', $attributes = array('class'=>'form-control', 'id'=>'telepon')) }}
        </div>

      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Lainnya</label>  
          <input id="lainnya" name="lainnya" value="{{ isset($aset->lainnya) ? $aset->lainnya : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        <div class="form-group">
          <label for="order">Batas Utara</label>  
          <input id="batas_utara" name="batas_utara" value="{{ isset($aset->batas_utara) ? $aset->batas_utara : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        <div class="form-group">
          <label for="order">Batas Barat</label>  
          <input id="batas_barat" name="batas_barat" value="{{ isset($aset->batas_barat) ? $aset->batas_barat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Batas Selatan</label>  
          <input id="batas_selatan" name="batas_selatan" value="{{ isset($aset->batas_selatan) ? $aset->batas_selatan : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Batas Timur</label>  
          <input id="batas_timur" name="batas_timur" value="{{ isset($aset->batas_timur) ? $aset->batas_timur : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div>
    <br><br>
    <!-- <div class="row">
      <div class="col-lg-12">
        <legend>ANALISA HBU</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Penggunaan Tertinggi dan Terbaik (HBU) Keadaan Terbangun</label>  
          {{ Form::select('hbu_terbangun', $hbu_terbangun, isset($aset->hbu_terbangun) ? $aset->hbu_terbangun : '', $attributes = array('class'=>'form-control', 'id'=>'hbu_terbangun')) }}
        </div>
      </div>
      
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Penggunaan Tertinggi dan Terbaik (HBU) Keadaan Kosong</label>  
          {{ Form::select('hbu_kosong', $hbu_kosong, isset($aset->hbu_kosong) ? $aset->hbu_kosong : '', $attributes = array('class'=>'form-control', 'id'=>'hbu_kosong')) }}
        </div>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-12">
        <legend>INDIKASI NILAI PASAR</legend>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Total Absolut</label>  
          <input id="total_absolut" name="total_absolut" value="{{ isset($aset->total_absolut) ? $aset->total_absolut : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Inverse</label>  
          <input id="inverse" name="inverse" value="{{ isset($aset->inverse) ? $aset->inverse : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Weighted</label>  
          <input id="weighted" name="weighted" value="{{ isset($aset->weighted) ? $aset->weighted : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Range Price Maximum</label>  
          <input id="price_maximum" name="price_maximum" value="{{ isset($aset->price_maximum) ? $aset->price_maximum : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Range Price Minimum</label>  
          <input id="price_minimum" name="price_minimum" value="{{ isset($aset->price_minimum) ? $aset->price_minimum : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Validation</label>  
          <input id="validation" name="validation" value="{{ isset($aset->validation) ? $aset->validation : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Indikasi Nilai Pasar (m2)</label>  
          <input id="imv_psm" name="imv_psm" value="{{ isset($aset->imv_psm) ? $aset->imv_psm : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Indikasi Nilai Pasar</label>  
          <input id="imv" name="imv" value="{{ isset($aset->imv) ? $aset->imv : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Indikasi Nilai Pasar Dibulatkan</label>  
          <input id="imv_bulat" name="imv_bulat" value="{{ isset($aset->imv_bulat) ? $aset->imv_bulat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
    
        <div class="form-group">
          <label for="order">Indikasi Nilai Pasar (Pendekatan Selain Pasar)</label>  
          <input type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
    </div> -->
    <div class="row">
      <div class="col-lg-4">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Data</button>
      </div>
    </div>
</fieldset>
</form>
<script>
var map;
var marker;
$(document).ready(function(){
  @if ($form_title == "add")
    map = L.map('map').setView([-2.548926,118.0148634], 5);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZWxjaWNrbyIsImEiOiJQOFVPUFBrIn0.SYrEN9dSd23jmioBB6NAvQ'
    }).addTo(map);   
    marker = L.marker([-2.548926,118.0148634], { draggable :true }).addTo(map) 
  @elseif ($form_title == "edit")
    map = L.map('map').setView([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634' }}], 15);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZWxjaWNrbyIsImEiOiJQOFVPUFBrIn0.SYrEN9dSd23jmioBB6NAvQ'
    }).addTo(map);
    marker = L.marker([{{ isset($aset->koordinat_latitude) ? $aset->koordinat_latitude : '-2.548926' }},{{ isset($aset->koordinat_longitude) ? $aset->koordinat_longitude : '118.0148634' }}], { draggable :true }).addTo(map) 
  @endif

  marker.on('dragend', function(event){
      var marker = event.target;
      var lat = marker.getLatLng().lat;
      var lng = marker.getLatLng().lng;
      document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
  });
  map.addLayer(marker);
});
</script>