<ol class="breadcrumb">
    <li><a href="{{ url('kontrak/'.$lokasi.'') }}"><i class="fa fa-file-o"></i> Data Kontrak</a></li>
    <li><i class="fa fa-map-marker"></i> {{ ucwords($lokasi) }}</li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}" enctype="multipart/form-data">
  <fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Kode Asset</label>  
          <input id="kode_aset" name="kode_aset" value="{{ isset($aset->kode_aset) ? $aset->kode_aset : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Nama Asset</label>  
          <input id="nama_asset" name="nama_asset" value="{{ isset($aset->nama_asset_1) ? $aset->nama_asset_1 : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Jenis Aset</label>  
          {{ Form::select('jenis_asset', $jenis_asset, isset($aset->jenis_asset) ? $aset->jenis_asset : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_asset')) }}
        </div>

        <div class="form-group">
          <input type="hidden" name="nomor_kontrak" value="{{ isset($nomor_kontrak) ? $nomor_kontrak : '' }}">
          <button type="submit" class="btn btn-success" style="margin-top:22px;"><i class="fa fa-save"></i> Simpan Data</button>
        </div>

      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Alamat</label>  
          <input id="alamat" name="alamat" value="{{ isset($aset->alamat) ? $aset->alamat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Luas</label>  
          <input id="luas" name="luas" value="{{ isset($aset->luas) ? $aset->luas : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">PIC</label>  
          {{ Form::select('pic', $pic, isset($aset->pic) ? $aset->pic : '', $attributes = array('class'=>'form-control', 'id'=>'pic')) }}
        </div>

      </div>
    </div>
  </fieldset>
</form>