<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>
<script type="text/javascript" src="{{ url('assets/lib/js/markerclusterer_packed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/lib/js/googleearth-compiled.js') }}"></script>
<ol class="breadcrumb">
  <li><i class="fa fa-file-o"></i> Data Tanah</li>
</ol>
<br>
<ul class="nav nav-tabs">
    <li><a href="{{ url('aset') }}">Table Format</a></li>
    <li class="active"><a href="#">Map Format</a></li>
</ul>
<br>
<div class="row"> 
    <div class="col-lg-12">
    @if ($write == 1)
      <!-- <a class="btn btn-success" href="{{ url('aset/create') }}"><i class="fa fa-plus"></i> Tambah Data</a> -->
    @else
    @endif
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
        <div class="col-lg-12" style="background:#ccc; padding-top: 25px; margin-bottom:20px; margin-top:15px; margin-left: 20px; padding-right: 20px; width: 90%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-4">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="ID Aset / Nama Aset" name="q" id="q">
                    </div>
                    <div class="form-group">
                      {{ Form::select('company', $company, '', $attributes = array('style'=>'width:100%;', 'id'=>'company')) }}
                    </div>

                     <div class="form-group">
                      {{ Form::select('tipe_pembangkit', $tipe_pembangkit, '', $attributes = array('style'=>'width:100%;', 'id'=>'tipe_pembangkit')) }}
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="caripeta"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                   
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      {{ Form::select('atl', $atl, '', $attributes = array('style'=>'width:100%;', 'id'=>'atl')) }}
                    </div>

                    <div class="form-group">
                      {{ Form::select('provinsi', $provinsi, '', $attributes = array('style'=>'width:100%;', 'id'=>'provinsi', 'onchange' => 'ajaxkota(this.options[this.selectedIndex].value)')) }}
                    </div>

                    <div class="form-group">
                      {{ Form::select('kabkota', $kabkota, '', $attributes = array('style'=>'width:100%;', 'id'=>'kabkota', 'onchange' => 'ajaxkecamatan(this.options[this.selectedIndex].value)')) }}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      {{ Form::select('kecamatan', $kecamatan, '', $attributes = array('style'=>'width:100%;', 'id'=>'kecamatan', 'onchange' => 'ajaxkelurahan(this.options[this.selectedIndex].value)')) }}
                    </div>
                    
                    <div class="form-group">
                      {{ Form::select('kelurahan', $kelurahan, '', $attributes = array('style'=>'width:100%;', 'id'=>'kelurahan')) }}
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="IMV PSM" name="imv_psm" id="imv_psm">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div id="map" style="width:100%; height:700px"></div>
        <script>
            var ajaxku;
            var map;
            var markers;
            var marker;
            var contentString;
            var gambar_tanda = "{{ url('assets/img/marker_location.png') }}";
            function ajaxkota(id) {
                ajaxku = buatajax();
                var url = "{{ url('getkabkota') }}";
                url = url + "/" + id;
                ajaxku.onreadystatechange = cityChange;
                ajaxku.open("GET", url, true);
                ajaxku.send(null);
            }

            function ajaxkecamatan(id) {
                ajaxku = buatajax();
                var url = "{{ url('getkecamatanbykabkota') }}";
                url = url + "/" + id;
                ajaxku.onreadystatechange = districtChange;
                ajaxku.open("GET", url, true);
                ajaxku.send(null);
            }

            function ajaxkelurahan(id) {
                ajaxku = buatajax();
                var url = "{{ url('getkelurahanbykecamatan') }}";
                url = url + "/" + id;
                ajaxku.onreadystatechange = villageChange;
                ajaxku.open("GET", url, true);
                ajaxku.send(null);
            }

            function buatajax() {
              if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
              }

              if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
              }

              return null;
            }

            function cityChange() {
              var data;
              if (ajaxku.readyState == 4) {
                data = ajaxku.responseText;
                if (data.length >= 0) {
                    document.getElementById("kabkota").innerHTML = data
                } else {
                    document.getElementById("kabkota").value = "<option selected>-- Silahkan Pilih Kota / Kabupaten --</option>";
                }
              }
            }

            function districtChange() {
            var data;
            if (ajaxku.readyState == 4) {
                data = ajaxku.responseText;
                if (data.length >= 0) {
                    document.getElementById("kecamatan").innerHTML = data
                } else {
                    document.getElementById("kecamatan").value = "<option selected>-- Silahkan Pilih Kecamatan --</option>";
                }
            }
            }

            function villageChange() {
            var data;
            if (ajaxku.readyState == 4) {
                data = ajaxku.responseText;
                if (data.length >= 0) {
                    document.getElementById("kelurahan").innerHTML = data
                } else {
                    document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan --</option>";
                }
            }
            }


            $(document).ready(function(){
                var url = "{{ url('getaset') }}";
                var googleEarth;
                $.ajax({
                    url: url,
                    dataType: 'json',
                    cache: true,
                    success: function(msg){
                        var indonesia = new google.maps.LatLng(-2.548926,118.0148634);
                        var petaoption = {
                            zoom: 5,
                            center: indonesia,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                        peta = new google.maps.Map(document.getElementById("map"),petaoption);
                        googleEarth = new GoogleEarth(peta);
                       
                        google.maps.event.addListener(peta,'click',function(event){
                            
                        });
                       
                        var markers = [];
                        var info = [];
                        for (var i = 0; i < msg.length; i++) {
                            var point = new google.maps.LatLng(parseFloat(msg[i].latitude),parseFloat(msg[i].longitude));

                            tanda = new google.maps.Marker({
                                    position: point,
                                    map: peta,
                                    icon: gambar_tanda,
                                    animation: google.maps.Animation.DROP,
                                    clickable: true
                                    });

                            google.maps.event.addListener(tanda, 'click', (function(tanda, i) {
                                return function() {
                                    contentString = [
                                                  '<div style="padding:10px; width:450px; height:auto;">',
                                                    '<table width="100%" cellpadding="5" cellspacing="5">',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Kode Aset',
                                                            '</td>',
                                                            '<td>',
                                                            '&nbsp;<a href="{{ url('aset/detail/')}}/'+msg[i].id+'">'+msg[i].kodeaset+'',
                                                            '</td>',
                                                        '</tr>',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Nama Aset',
                                                            '</td>',
                                                            '<td width="50%">',
                                                            '&nbsp;'+msg[i].nama_aset+'',
                                                            '</td>',
                                                        '</tr>',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Provinsi',
                                                            '</td>',
                                                            '<td>',
                                                            '&nbsp;'+msg[i].provinsi+'',
                                                            '</td>',
                                                        '</tr>',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Kabupaten / Kota',
                                                            '</td>',
                                                            '<td>',
                                                            '&nbsp;'+msg[i].kabkota+'',
                                                            '</td>',
                                                        '</tr>',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Keterangan Lokasi',
                                                            '</td>',
                                                            '<td>',
                                                            '&nbsp;'+msg[i].kodelokasi+'',
                                                            '</td>',
                                                        '</tr>',
                                                        '<tr>',
                                                            '<td width="20%">',
                                                            'Business Area',
                                                            '</td>',
                                                            '<td>',
                                                            '&nbsp;'+msg[i].businessarea+'',
                                                            '</td>',
                                                        '</tr>',
                                                    '</table>',
                                                  '</div>'
                                                ].join('');
                                    infowindow.setContent(contentString);
                                    infowindow.open(peta, tanda);
                                }
                            })(tanda, i));
                
                            markers.push(tanda);         
                            var infowindow = new google.maps.InfoWindow();
                        }
                        var markerCluster = new MarkerClusterer(peta, markers);
                    }
                });
            });

            $(document).ready(function(){
                $("#caripeta").click(function(){
                var q            = $("#q").val();
                var atl          = $("#atl").val();
                var prov         = $("#provinsi").val();
                var kab          = $("#kabkota").val();
                var kec          = $("#kecamatan").val();
                var kel          = $("#kelurahan").val();
                var company      = $("#company").val();
                var tipe_pembangkit = $("#tipe_pembangkit").val();
                var imv_psm      = $("#imv_psm").val();
                
                var url = "{{ url('searchallaset') }}";
                $.ajax({
                url: url,
                data: "q="+q+"&atl="+atl+"&prov="+prov+"&kab="+kab+"&kec="+kec+"&kel="+kel+"&company="+company+"&tipe_pembangkit="+tipe_pembangkit+"&imv_psm="+imv_psm,
                type: 'POST',
                dataType: 'json',
                cache: false,
                    success: function(msg){
                        map.remove();
                        map = L.map('map').setView([-2.548926,118.0148634], 5);
                        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                        }).addTo(map);
                        markers = L.markerClusterGroup();
                        for (var i = 0; i < msg.length; i++) {
                              contentString = [
                                              '<div style="padding:10px; width:450px; height:auto;">',
                                                '<table width="100%" cellpadding="3" cellspacing="3">',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kode Aset',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;<a href="{{ url('aset/detail/')}}/'+msg[i].id+'">'+msg[i].kodeaset+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Nama Aset',
                                                        '</td>',
                                                        '<td width="50%">',
                                                        '&nbsp;'+msg[i].nama_aset+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Provinsi',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].provinsi+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Kabupaten / Kota',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].kabkota+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Keterangan Lokasi',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].kodelokasi+'',
                                                        '</td>',
                                                    '</tr>',
                                                    '<tr>',
                                                        '<td width="20%">',
                                                        'Business Area',
                                                        '</td>',
                                                        '<td>',
                                                        '&nbsp;'+msg[i].businessarea+'',
                                                        '</td>',
                                                    '</tr>',
                                                '</table>',
                                              '</div>'
                                            ].join('');
                            marker = L.marker(new L.LatLng(msg[i].latitude, msg[i].longitude));
                            marker.bindPopup(contentString, { maxWidth:800 });
                            markers.addLayer(marker);
                        }
                        map.addLayer(markers);
                        }
                    });
                });
            });
        </script>
    </div>
</div>