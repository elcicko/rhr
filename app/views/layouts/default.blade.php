<!DOCTYPE html>
<html>
<head>
<title>:: KPJJ RHR ::</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/animate.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/themes/flat-blue.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/lib/js/datatable/css/jquery.dataTables.css') }}">
<link href="{{ url('assets/lib/js/tablesorter/themes/blue/style.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ url('assets/lib/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/lib/js/jquery.browser.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/app/config.js') }}"></script>
<style type="text/css">
#datatable th { text-align: center }  
</style>
</head>
<body class="flat-blue">
	<div class="app-container">
		<div class="row content-container">
			<nav class="navbar navbar-inverse navbar-fixed-top navbar-top">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-expand-toggle"><i class="fa fa-bars icon"></i></button>
						<ol class="breadcrumb navbar-breadcrumb">
							<li class="active">{{ $title }}</li>
						</ol>
						<button type="button" class="navbar-right-expand-toggle pull-right visible-xs"><i class="fa fa-th icon"></i></button>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<button type="button" class="navbar-right-expand-toggle pull-right visible-xs"><i class="fa fa-times icon"></i></button>
					</ul>
				</div>
			</nav>
			<div class="side-menu sidebar-inverse">
				<nav class="navbar navbar-inverse" role="navigation">
					<div class="side-menu-container">
						<div class="navbar-header">
							<a class="navbar-brand" href="{{ url('dashboard')}}">
								<div class="icon fa fa-leaf"></div>
								<div class="title">KJPP RHR</div>
							</a>
							<button type="button" class="navbar-expand-toggle pull-right visible-xs"><i class="fa fa-times icon"></i></button>
						</div>
						<ul class="nav navbar-nav">
							<li>
								<a href="{{ url('dashboard')}}"><span class="icon fa fa-home"></span>
									<span class="title">Dashboard</span>
								</a>
							</li>
							<li class="panel panel-default dropdown">
	                            <a data-toggle="collapse" href="#dropdown-configuration">
	                                <span class="icon fa fa-gear"></span><span class="title">Konfigurasi</span>
	                            </a>
	                            <div id="dropdown-configuration" class="panel-collapse collapse">
	                                <div class="panel-body">
	                                    <ul class="nav navbar-nav">
	                                        <li><a href="{{ url('user') }}"><span class="icon fa fa-users"></span><span class="title"> Data User</span></a></li>
	                                        <li><a href="{{ url('menu') }}"><span class="icon fa fa-leaf"></span><span class="title"> Data Menu</span></a></li>
	                                        <li><a href="{{ url('hakakses') }}"><span class="icon fa fa-unlock"></span><span class="title"> Hak Akses Menu</span> </a></li>
	                                    </ul>
	                                </div>
	                            </div>
                            </li>
                            <li class="panel panel-default dropdown">
	                            <a data-toggle="collapse" href="#dropdown-project">
	                                <span class="icon fa fa-file-o"></span><span class="title">Kontrak</span>
	                            </a>
	                            <div id="dropdown-project" class="panel-collapse collapse">
	                                <div class="panel-body">
	                                    <ul class="nav navbar-nav">
	                                        <li><a href="{{ url('kontrak/jakarta') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Jakarta</span></a></li>
	                                        <li><a href="{{ url('kontrak/surabaya') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Surabaya</span></a></li>
	                                        <li><a href="{{ url('kontrak/medan') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Medan</span> </a></li>
	                                        <li><a href="{{ url('kontrak/bali') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Bali</span> </a></li>
	                                        <li><a href="{{ url('kontrak/makassar') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Makassar</span> </a></li>
	                                        <li><a href="{{ url('kontrak/solo') }}"><span class="icon fa fa-map-marker"></span><span class="title"> Solo</span> </a></li>
	                                    </ul>
	                                </div>
	                            </div>
                            </li>
							<li>
								<a href="{{ url('pembanding')}}"><span class="icon fa fa-building-o"></span>
									<span class="title">Data Pembanding</span>
								</a>
							</li>
							
                            <li>
								<a href="{{ url('penilaian')}}"><span class="icon fa fa-check-square-o"></span>
									<span class="title">Resume Penilaian</span>
								</a>
							</li>
							<li>
								<a href="{{ url('aset')}}"><span class="icon fa fa-file-excel-o"></span>
									<span class="title">Laporan</span>
								</a>
							</li>
							<li>
								<a href="{{ url('import')}}"><span class="icon fa fa-upload"></span>
									<span class="title">Import Data</span>
								</a>
							</li>
							<li>
								<a href="{{ url('logout')}}"><span class="icon fa fa-power-off"></span>
									<span class="title">Logout</span>
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="container-full">
				<div class="side-body padding-top">
					@if(Session::has('revoke'))
	                  <script>
	                    alert('{{ Session::get('revoke') }}');
	                  </script>
	                @endif
					{{ $content }}
				</div>
			</div>
		</div>
		<footer class="app-footer">
			<div class="wrapper">
				<span class="pull-right"><a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright
			</div>
		</footer>
		<div>
			<script type="text/javascript" src="{{ url('assets/lib/js/bootstrap.min.js') }}"></script>
			<script type="text/javascript" src="{{ url('assets/lib/js/select2.full.min.js') }}"></script>
			<script type="text/javascript" src="{{ url('assets/js/app.js') }}"></script>
			<script type="text/javascript" src="{{ url('assets/lib/js/jquery-validate/jquery.validate.min.js') }}"></script>
			<script type="text/javascript" src="{{ url('assets/lib/js/datatable/js/jquery.dataTables.min.js') }}"></script>
			<script src="{{ url('assets/lib/js/datatable/js/datatable.ext.js')}}"></script>
		</div>
	</div>
</body>
</html>