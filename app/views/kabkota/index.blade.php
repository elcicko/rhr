<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><i class="fa fa-map-marker"></i> Kabupaten / Kota</li>
</ol>
<div class="row"> 
  <div class="col-lg-12">
  		@if ($write == 1)
  			<a class="btn btn-primary" href="{{ url('kabkota/create') }}"><i class="fa fa-plus"></i> Tambah Kab / kota</a>
  		@endif
  		<?php 
  		if ($count > 0) {
  		?>
  			<br><br>
			<form action="{{ $form_action }}" class="form-search" method="POST">
				<div class="col-md-4">
			        <div class="form-group">
			          <input id="kabkota" name="kabkota" type="text" placeholder="Ketikkan Nama Kab / Kota" class="form-control input-md">
			        </div>
			    </div>
			    <div class="col-md-4">
			        <div class="form-group">
			          {{ Form::select('provinsi', $provinsi, '', $attributes = array('class'=>'form-control input-md', 'id'=>'provinsi')) }}
			        </div>
			    </div>

			    <div class="col-md-3">
			        <div class="form-group">
			            <button id="" name="" class="btn btn-primary"><i class="fa fa-search"></i> CARI DATA</button>
			        </div>
				</div>
			</form>
			@if(Session::has('message'))
				<br><br><br>
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
	        <table class="tablesorter">
	            <thead>
	            <tr>
	                <th><center>#</center></th>
	                <th><center>Nama Kab / kota</center></th>
	                <th><center>Provinsi</center></th>
	                <th><center>Garis Lintang</center></th>
	                <th><center>Garis Bujur</center></th>
	                <th colspan="2"><center>Action</center></th>
	            </tr>
	            </thead>
	            <tbody>
	            <?php 
	            $page = $kabkotas->getCurrentPage();
	            if ($page == 1) {
	            	$i = 1;
	            } else {
	            	$i = $kabkotas->getFrom();
	            } 

	            ?>
	            @foreach ($kabkotas as $kabkota)
	            <tr>
	            	<td><center>{{ $i }}</center></td>
	                <td>{{ $kabkota->nama_kabkota }}</td>
	                <td>{{ $kabkota->provinsi->nama_provinsi }}</td>
	                <td>{{ $kabkota->latitude }}</td>
	                <td>{{ $kabkota->longitude }}</td>
	                <td>
	                	@if($update == 1)
	                		<center><a href="{{ url('kabkota/edit/'.$kabkota->id.'') }}"><i class="fa fa-wrench"></i> Ubah</a></center>
	                	@else
	                		<center><i class="fa fa-wrench"></i> Ubah</center>
	                	@endif
	                </td>
	                <td>
	                	@if($delete == 1)
	                		<center><a data-toggle="modal" href="#confirm{{ $i }}"><i class="fa fa-trash-o"></i> Hapus</a></center>
	                	@else
	                		<center><i class="fa fa-trash-o"></i> Hapus</center>
	                	@endif
	                </td>
	            </tr>
	            <div class="modal fade" id="confirm{{ $i }}" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Konfirmasi</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-primary" href="<?php echo url('kabkota/destroy/'.$kabkota->id.''); ?>" >Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>
	            <?php $i++; ?>
	            @endforeach
	            </tbody>
	        </table>
	        <ul class="pagination">
			@if(isset($search) AND ($search) == TRUE)
				{{ $kabkotas->appends(Request::all())->links() }}
			@elseif (isset($search) AND ($search) == FALSE)
				{{ $kabkotas->links() }}
			@endif
			</ul>
	    <?php
		} else {
	    ?>
	    <br><br>
	    <div class="alert alert-danger"><center>DATA KABUPATEN / KOTA KOSONG</center></div>
	    <?php
		}
	    ?>
  </div>
</div>