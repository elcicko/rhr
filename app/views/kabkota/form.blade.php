<link rel="stylesheet" href="{{ asset('assets/js/leafletjs/leaflet.css') }}">
<script src="{{ asset('assets/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ asset('js_app/validation/kabkota_form.js') }}"></script>
<ol class="breadcrumb">
    <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('kabkota') }}"><i class="fa fa-map-marker"></i> Manajemen Kabupaten / Kota</a></li>
    <li><i class="fa fa-map-marker"></i> {{ $action_title }}</li>
</ol>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
<fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="col-lg-8">
      <div id="map" style="width:100%; height:500px"></div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Provinsi</label>  
          {{ Form::select('provinsi', $provinsi, isset($prov) ? $prov : '', $attributes = array('class'=>'form-control', 'id'=>'provinsi', 'onchange' => 'setpeta(this.options[this.selectedIndex].value)')) }}
        </div>

        <div class="form-group">
          <label for="order">Nama Kab / Kota</label>  
          <input id="nama_kabkota" name="nama_kabkota" value="{{ isset($kabkota->nama_kabkota) ? $kabkota->nama_kabkota : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Garis Lintang</label>  
          <input id="latitude" name="latitude" value="{{ isset($kabkota->latitude) ? $kabkota->latitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Garis Bujur</label>  
          <input id="longitude" name="longitude" value="{{ isset($kabkota->longitude) ? $kabkota->longitude : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
            <button id="" name="" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
</fieldset>
</form>
<script>
var map;
var marker;

$(document).ready(function(){
    @if ($form_title == "add")
      map = L.map('map').setView([-2.548926,118.0148634], 5);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);   
      marker = L.marker([-2.548926,118.0148634], { draggable :true }).addTo(map) 
  
    @elseif ($form_title == "edit")
  
      map = L.map('map').setView(
                                [{{ isset($kabkota->latitude) ? $kabkota->latitude : '' }},
                                 {{ isset($kabkota->longitude) ? $kabkota->longitude : '' }}], 8);

      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);  

      marker = L.marker(
                        [{{ isset($kabkota->latitude) ? $kabkota->latitude : '' }},
                         {{ isset($kabkota->longitude) ? $kabkota->longitude : '' }}], 
                        { draggable :true }).addTo(map) 

    @endif

  marker.on('dragend', function(event){
      var marker = event.target;
      var lat = marker.getLatLng().lat;
      var lng = marker.getLatLng().lng;
      document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
  });
  map.addLayer(marker);
});

function setpeta(lokasi){
  var koordinat = lokasi.split('|');
  var x = koordinat[0];
  var y = koordinat[1];
  var id = koordinat[2];

  map.remove();
  map = L.map('map').setView([x,y], 8);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'}).addTo(map);   
  marker = L.marker([x,y], { draggable :true }).addTo(map) 
  marker.on('dragend', function(event){
      var marker = event.target;
      var lat = marker.getLatLng().lat;
      var lng = marker.getLatLng().lng;
      document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
  });
  map.addLayer(marker);
}
</script>