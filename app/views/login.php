
<!DOCTYPE html>
<html>
<head>
    <title>:: KPJJ RHR :: </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/lib/css/select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/themes/flat-blue.css') }}">
</head>
<body class="flat-blue login-page">
    <div class="container">
        <div class="login-box">
            <div>
                <div class="login-form row">
                    <div class="col-sm-12 text-center">
                        <h2 class="login-title">
                            KPJJ RHR
                        </h2>
                    </div>
                    <div class="col-sm-12">
                        <div class="login-body">
                            <form class="form-signin" id="login_form" action="{{ url('login') }}" method="POST">
                                <div class="control">
                                    <input name="username" type="text" class="form-control" Placeholder="Username" required/>
                                </div>
                                <div class="control">
                                    <input name="password" type="password" class="form-control" Placeholder="Password" required/>
                                </div>
                                <div class="login-button text-center">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-lock"></i> Login</button>
                                </div>
                                <br>
                                @if(Session::has('message'))
                                    <center><div class="alert alert-danger">{{ Session::get('message') }}</div></center>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
             <footer class="app-footer">
            <div class="wrapper">
            <center> © <?php echo date('Y'); ?> Copyright</center>
            </div>
        </footer>
        </div>
    </div>
    <!-- Javascript Libs -->
    <script type="text/javascript" src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/app.js') }}"></script>
</body>
</html>
