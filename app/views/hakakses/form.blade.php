<ol class="breadcrumb">
  <li><a href="{{ url('hakakses') }}"><i class="fa fa-unlock"></i> Hak Akses Menu</a></li>
  <li><i class="fa fa-unlock"></i> {{ $action_title }}</li>
</ol>
<br>
<script src="{{ asset('js_app/validation/akses_form.js') }}"></script>
<form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
<fieldset>
    <legend><?php echo $action_title; ?></legend>
    <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Menu</label>  
          {{ Form::select('menu', $menu, isset($menu_id) ? $menu_id : '', $attributes = array('class'=>'form-control', 'id'=>'menu')) }}
        </div>

        <div class="form-group">
          <label for="order">Level</label>  
          {{ Form::select('level', $level, isset($level_id) ? $level_id : '', $attributes = array('class'=>'form-control', 'id'=>'level')) }}
        </div>

        <div class="checkbox">
          <label class="checkbox-inline">
          {{ Form::checkbox('write', isset($writes) ? $writes : '1', isset($writes) ? true : '', $attributes = array('id'=>'write')) }} Simpan Data
          </label>
       
          <label class="checkbox-inline">
          {{ Form::checkbox('update', isset($updates) ? $updates : '1', isset($updates) ? true : '', $attributes = array('id'=>'update')) }} Ubah Data
          </label>

          <label class="checkbox-inline">
          {{ Form::checkbox('delete', isset($deletes) ? $deletes : '1', isset($deletes) ? true : '', $attributes = array('id'=>'delete')) }} Hapus Data
          </label>
        </div>

        <div class="form-group">
            <button id="" name="" class="btn btn-success"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
</fieldset>
</form>