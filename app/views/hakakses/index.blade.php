<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<ol class="breadcrumb">
  <li><i class="fa fa-unlock"></i> Hak Akses Menu</li>
</ol>
<br>
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-12 well">
      <form class="form-search" action="" method="POST">
        <div class="col-lg-4">
           <div class="form-group">
                {{ Form::select('level', $level, '', $attributes = array('class'=>'form-control', 'id'=>'level')) }}
              </div>
        </div>
        <div class="col-lg-4">
           <div class="form-group">
                {{ Form::select('menu', $menu, '', $attributes = array('class'=>'form-control', 'id'=>'menu')) }}
              </div>
        </div>
        <div class="col-lg-4">
          <div class="form-group">
            <button type="button" class="btn btn-info" id="search">Cari Data</button>
            &nbsp; &nbsp;
            @if ($write == 1)
                <a class="btn btn-success" href="{{ url('hakakses/create') }}"><i class="fa fa-plus"></i> Tambah Akses Menu</a>
              @endif
          </div>
        </div>
      </form>
    </div>

  			@if(Session::has('message'))
				<br><br>
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@else
				<br><br>
			@endif
      <script>
        $(document).ready(function(){
          $('#search').click(function() { oTable.fnReloadAjax (); });
        });
      </script>
			{{
			 	Datatable::table()
			    ->addColumn('#','Nama Menu','URL','Level','Simpan Data','Ubah Data','Hapus Data','Action')
			    ->setUrl(route('datatable.hakakses', array('update'=>$update, 'delete'=>$delete)))
          ->setCallbacks(["fnServerParams" => 'function ( aoData ) {
        							    aoData.push({ "name": "level", "value": $(\'#level\').val() });
        							    aoData.push({ "name": "menu", "value": $(\'#menu\').val() });
        							    }'
        							   ])
			    ->render()
			}}
  </div>
</div>
