<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/leaflet.css') }}">
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/MarkerCluster.css') }}" />
<link rel="stylesheet" href="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/MarkerCluster.Default.css') }}" />
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/lib/js/leafletjs/plugins/markercluster/dist/leaflet.markercluster-src.js') }}"></script>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
</ol>
<br>
<div class="row">
    <div class="col-lg-12">
        <form class="form-search">
		    <div class="col-lg-4">
		        <div class="form-group">
		          {{ Form::select('atl', $atl, '', $attributes = array('style'=>'width:100%;', 'id'=>'atl')) }}
		        </div>
		    </div>
		    <div class="col-lg-2">
		        <div class="form-group">
		            <button id="caripeta" name="caripeta" class="btn btn-primary" type="button"><i class="fa fa-search"></i> CARI DATA</button>
		        </div>
			</div>
		</form>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div id="map" style="width:100%; height:700px"></div>
        <script src="{{ url('assets/app/dashboard.js') }}"></script>
    </div>
</div>