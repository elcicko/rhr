<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><i class="fa fa-map-marker"></i> Provinsi</li>
</ol>
<div class="row"> 
  <div class="col-lg-12">
  		@if($write == 1) 
  			<a class="btn btn-primary" href="{{ url('provinsi/create') }}"><i class="fa fa-plus"></i> Tambah Provinsi</a>
  		@endif
  		@if ($count > 0)
  			@if(Session::has('message'))
				<br><br>
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
	        <table class="tablesorter">
	            <thead>
	            <tr>
	                <th><center>#</center></th>
	                <th><center>Nama Provinsi</center></th>
	                <th><center>Garis Lintang</center></th>
	                <th><center>Garis Bujur</center></th>
	                <th colspan="2"><center>Action</center></th>
	            </tr>
	            </thead>
	            <tbody>
	            <?php 
	            $page = $provinsis->getCurrentPage();
	            if ($page == 1) {
	            	$i = 1;
	            } else {
	            	$i = $provinsis->getFrom();
	            } 

	            ?>
	            @foreach ($provinsis as $provinsi)
	            <tr>
	            	<td><center>{{ $i }}</center></td>
	                <td>{{ $provinsi->nama_provinsi }}</td>
	                <td>{{ $provinsi->latitude }}</td>
	                <td>{{ $provinsi->longitude }}</td>
	                <td>
	                	@if($update == 1)
	                		<center><a href="{{ url('provinsi/edit/'.$provinsi->id.'') }}"><i class="fa fa-wrench"></i> Ubah</a></center>
	                	@else
	                		<center><i class="fa fa-wrench"></i> Ubah</center>
	                	@endif
	                </td>
	                <td>
	                	@if($delete == 1)
	                		<center><a data-toggle="modal" href="#confirm{{ $i }}"><i class="fa fa-trash-o"></i> Hapus</a></center>
	                	@else 
	                		<center><i class="fa fa-trash-o"></i> Hapus</center>
	                	@endif
	                </td>
	            </tr>
	            <div class="modal fade" id="confirm{{ $i }}" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Konfirmasi</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-primary" href="{{ url('provinsi/destroy/'.$provinsi->id.''); }}" >Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>
	            <?php $i++; ?>
	            @endforeach
	            </tbody>
	        </table>
	        <ul class="pagination">
			{{ $provinsis->links() }}
			</ul>
		@else
	    <br><br>
	    <div class="alert alert-danger"><center>DATA PROVINSI KOSONG</center></div>
	    @endif
  </div>
</div>