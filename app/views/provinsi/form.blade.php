<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><a href="{{ url('provinsi') }}"><i class="fa fa-map-marker"></i> Provinsi</a></li>
  <li><i class="fa fa-map-marker"></i> {{ $action_title }}</li>
</ol>
<link rel="stylesheet" href="{{ asset('assets/js/leafletjs/leaflet.css') }}">
<script src="{{ asset('assets/js/leafletjs/leaflet.js') }}" type="text/javascript"></script>
<script src="{{ asset('js_app/validation/provinsi_form.js') }}"></script>
<form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
<fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="col-lg-8">
      <div id="map" style="width:100%; height:500px"></div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
          <label for="order">Nama Provinsi</label>  
          <input id="nama_provinsi" name="nama_provinsi" value="{{ isset($provinsi->nama_provinsi) ? $provinsi->nama_provinsi : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Garis Lintang</label>  
          <input id="latitude" name="latitude" value="{{ isset($provinsi->latitude) ? $provinsi->latitude : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Garis Bujur</label>  
          <input id="longitude" name="longitude" value="{{ isset($provinsi->longitude) ? $provinsi->longitude : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
            <button id="" name="" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
</fieldset>
</form>
<script>
$(document).ready(function(){
  	@if ($form_title == "add")
  	  map = L.map('map').setView([-2.548926,118.0148634], 5);
  		    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  		    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
  		}).addTo(map);   
  		marker = L.marker([-2.548926,118.0148634], { draggable :true }).addTo(map) 
	
    @elseif ($form_title == "edit")
  		map = L.map('map').setView(
                                [{{ isset($provinsi->latitude) ? $provinsi->latitude : '' }},
                                 {{ isset($provinsi->longitude) ? $provinsi->longitude : '' }}], 8);

  		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  		    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
  		}).addTo(map);   
  		marker = L.marker(
                        [{{ isset($provinsi->latitude) ? $provinsi->latitude : '' }},
                         {{ isset($provinsi->longitude) ? $provinsi->longitude : '' }}], 
                         { draggable :true }).addTo(map) 
    @endif

	marker.on('dragend', function(event){
	    var marker = event.target;
	    var lat = marker.getLatLng().lat;
	    var lng = marker.getLatLng().lng;
	   	document.getElementById('latitude').value = lat;
      document.getElementById('longitude').value = lng;
	});
	map.addLayer(marker);
});
</script>