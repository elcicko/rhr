<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<ol class="breadcrumb">
  <li><i class="fa fa-users"></i> Data User</li>
</ol>
<br><br>
<div class="row"> 
    <div class="col-lg-12">
    <a class="btn btn-success" href="{{ url('user/create') }}"><i class="fa fa-plus"></i> Tambah Data</a>
    &nbsp;&nbsp;
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
      	<div class="col-lg-12" style="background:#ccc;padding-top: 25px; margin-top: 15px; margin-left: 20px; padding-right: 20px; width: 95%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Username" name="username" id="username">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="search" style="margin-top: 0px;"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                      {{ Form::select('level', $level, '', $attributes = array('style'=>'width:100%;', 'id'=>'level')) }}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
    @if(Session::has('message'))
      <br><br>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <center>{{ Session::get('message') }}</center>
        </div>
    @else
        <br><br>
    @endif
    <script>
    $(document).ready(function(){
        $('#search').click(function() { oTable.fnReloadAjax (); });
    });
    </script>
    {{
        Datatable::table()
        ->addColumn('#','Username','Level','Action')
        ->setUrl(route('datatable.user', array('update'=>$update,'delete'=>$delete)))
        ->setCallbacks(["fnServerParams" => 'function ( aoData ) {           
                          aoData.push({ "name": "username", "value": $(\'#username\').val() });
                          aoData.push({ "name": "level", "value": $(\'#level\').val() });
                        }'
                      ])
        ->render()
    }}
  </div>
</div>