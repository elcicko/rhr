<ol class="breadcrumb">
  <li><a href="{{ url('user') }}"><i class="fa fa-users"></i> User</a></li>
  <li><i class="fa fa-users"></i> {{ $action_title }}</li>
</ol>
<br>
<form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
  <fieldset>
    <legend>{{ $action_title }}</legend>
    @if(Session::has('message'))
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <center>{{ Session::get('message') }}</center>
      </div>
    @endif
    <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Username</label>  
          <input id="username" name="username" value="{{ isset($user->username) ? $user->username : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>
        
        <div class="form-group">
          <label for="order">Level</label>  
          {{ Form::select('level', $level, isset($user->level_id) ? $user->level_id : '', $attributes = array('class'=>'form-control', 'id'=>'level')) }}
        </div>
       
        <div class="form-group">
            <button id="" name="" class="btn btn-success"><i class="fa fa-save"></i> SIMPAN DATA</button>
        </div>
    </div>
  </fieldset>
</form>