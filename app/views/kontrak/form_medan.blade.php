<link rel="stylesheet" href="<?php echo asset('assets/lib/js/datepicker/bootstrap-datepicker.css'); ?>">
<script src="<?php echo asset('assets/lib/js/datepicker/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
<script>
$(document).ready(function(){
  $('#tanggal_kontrak').datepicker({ format: "yyyy-mm-dd", autoclose: true });
});
</script>
<ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('kontrak/medan') }}"><i class="fa fa-file-o"></i> Data Kontrak</a></li>
    <li><i class="fa fa-map-marker"></i> Jakarta</li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br>
<form id="form" class="form-vertical" method="POST" action="{{ $form_action }}">
  <fieldset>
    <legend>{{ $action_title }}</legend>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Nomor Kontrak</label>  
          <input id="nomor_kontrak" name="nomor_kontrak" value="{{ isset($kontrak->nomor_kontrak) ? $kontrak->nomor_kontrak : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Tanggal Kontrak</label>  
          <input id="tanggal_kontrak" name="tanggal_kontrak" value="{{ isset($kontrak->tanggal_kontrak) ? $kontrak->tanggal_kontrak : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Jenis Aset</label>  
          {{ Form::select('jenis_asset', $jenis_asset, isset($kontrak->jenis_asset) ? $kontrak->jenis_asset : '', $attributes = array('class'=>'form-control', 'id'=>'jenis_asset')) }}
        </div>

        <div class="form-group">
          <label for="order">Alamat</label>  
          <input id="alamat" name="alamat" value="{{ isset($kontrak->alamat) ? $kontrak->alamat : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label for="order">Tujuan Penilaian</label>  
          <input id="tujuan_penilaian" name="tujuan_penilaian" value="{{ isset($kontrak->tujuan_penilaian) ? $kontrak->tujuan_penilaian : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Fee</label>  
          <input id="fee" name="fee" value="{{ isset($kontrak->fee) ? $kontrak->fee : ''; }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Pimpinan Proyek</label>  
          {{ Form::select('pimpinan_proyek', $user, isset($kontrak->pimpinan_proyek) ? $kontrak->pimpinan_proyek : '', $attributes = array('class'=>'form-control', 'id'=>'kontrak')) }}
        </div>

        <div class="form-group">
          <input type="hidden" name="lokasi" value="Medan">
          <button type="submit" class="btn btn-success" style="margin-top:21px;"><i class="fa fa-save"></i> Simpan Data</button>
        </div>
      </div>
    </div>
  </fieldset>
</form>