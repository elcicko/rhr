<ol class="breadcrumb">
    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a href="{{ url('kontrak/makassar') }}"><i class="fa fa-file-o"></i> Data Kontrak</a></li>
    <li><i class="fa fa-map-marker"></i> Makassar</li>
    <li><i class="fa fa-leaf"></i> {{ $action_title }}</li>
</ol>
<br><br>
<fieldset>
	<div class="row">
	    <div class="col-lg-4">
	      <br><br>
	      <center>
		  @if (isset($kontrak->file_kontrak) && ($kontrak->file_kontrak != ''))
	      	<a href="{{ url('file_kontrak/'.$kontrak->file_kontrak.'') }}" target="_blank"><img src="{{ url('assets/img/file.png') }}"></a>
	      @else
	      	<img src="{{ url('assets/img/file.png') }}">
	      @endif
	      </center>
	    </div>
	    <div class="col-lg-8">
	      <table class="table table-hovered" width="100%" cellpadding="5" cellspacing="5" style="font-size:13px;">
	        <tr>
	          <td width="20%"><b>Nomor Kontrak</b></td><td>{{ isset($kontrak->nomor_kontrak) ? $kontrak->nomor_kontrak : '' }}</td>
	        </tr>
	        <tr>
	          <td><b>Tanggal Kontrak</b></td><td>{{ isset($kontrak->tanggal_kontrak) ? $kontrak->tanggal_kontrak : '' }}</td>
	        </tr>
	        <tr>
	          <td><b>Jenis Asset</b></td><td>{{ isset($kontrak->jenis_asset) ? $kontrak->jenis_asset : '' }}</td>
	        </tr>
	        <tr>
	          <td><b>Alamat</b></td><td>{{ isset($kontrak->alamat) ? $kontrak->alamat : '' }}</td>
	        </tr>
	        <tr>
	          <td><b>Tujuan Penilaian</b></td><td>{{ isset($kontrak->tujuan_penilaian) ? $kontrak->tujuan_penilaian : ''  }}</td>
	        </tr>
	        <tr>
	          <td><b>Fee</b></td><td>Rp {{ isset($kontrak->fee) ? number_format($kontrak->fee ,2, ',' , '.' ) : '-' }}</td>
	        </tr>
	        <tr>
	          <td><b>Pimpinan Proyek</b></td><td>{{ isset($kontrak->pimpinan_proyek) ? $kontrak->pimpinan_proyek : '' }}</td>
	        </tr>
	      </table>
	    </div>
  	</div>
  	<div class="row">
  		<div class="col-lg-12">
  			<legend>LIST DATA ASSET PADA KONTRAK</legend>
  			<div class="row"> 
		    	<div class="col-lg-12">
				    @if ($write == 1)
				      <a class="btn btn-success" href="{{ url('aset/create_asetkontrak/makassar/'.$kontrak->nomor_kontrak.'') }}"><i class="fa fa-plus"></i> Tambah Data Aset</a>
				    @else
		    		@endif
		    		<a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
		    			<i class="fa fa-search"></i> Form Pencarian
		    		</a>
		    	</div>
		    	<div class="collapse" id="search_form">
		      	<div class="col-lg-12" style="background:#ccc; padding-top: 25px; margin-bottom:20px; margin-top:15px; margin-left: 20px; padding-right: 20px; width: 95%;">
		            <form class="form-vertical" action="" method="POST">
		                <div class="col-lg-6">
		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="ID Aset / Nama Aset" name="q" id="q">
		                    </div>

		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Alamat" name="alamat" id="alamat">
		                    </div>

		                     <div class="form-group">
		                      {{ Form::select('jenis_asset', $jenis_asset, '', $attributes = array('style'=>'width:100%;', 'id'=>'jenis_asset')) }}
		                    </div>

		                    <div class="form-group">
		                        <button type="button" class="btn btn-info" id="search"><i class="fa fa-search"></i> Cari Data</button>
		                    </div>
		                   
		                </div>
		                <div class="col-lg-6">
							<div class="form-group">
							  {{ Form::select('satuan', $satuan, '', $attributes = array('style'=>'width:100%;', 'id'=>'satuan')) }}
							</div>

		                    <div class="form-group">
		                        <input type="text" class="form-control" placeholder="Luas / Kap" name="luas" id="luas">
		                    </div>

		                    <div class="form-group">
		                      {{ Form::select('pic', $pic, '', $attributes = array('style'=>'width:100%;', 'id'=>'pic')) }}
		                    </div>
		                </div>
		            </form>
		        </div>
		    	</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
				    @if(Session::has('message'))
				      <br><br>
				      <div class="alert alert-success">
				          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				          <center>{{ Session::get('message') }}</center>
				      </div>
				    @else
				    @endif
				    <script>
				    $(document).ready(function(){
				        $('#search').click(function() { oTable.fnReloadAjax (); });
				    });
				    </script>
				    {{
				        Datatable::table()
				        ->addColumn('#','ID Asset','Nama Asset','Alamat', 'Jenis Asset', 'Luas / Kap','PIC','Action')
				        ->setUrl(route('datatable.asetkontrak',array('session_user'=>$session_user,'update'=>$update,'delete'=>$delete,'nomor_kontrak'=> $kontrak->nomor_kontrak)))
				        ->setCallbacks(["fnServerParams" => 'function ( aoData ) {           
				                          aoData.push({ "name": "alamat", "value": $(\'#alamat\').val() });
				                          aoData.push({ "name": "jenis_asset", "value": $(\'#jenis_asset\').val() });
				                          aoData.push({ "name": "luas", "value": $(\'#luas\').val() });
				                          aoData.push({ "name": "pic", "value": $(\'#pic\').val() });
				                          aoData.push({ "name": "q", "value": $(\'#q\').val() });
				                        }'
				                      ])
				        ->render()
				    }}
			  	</div>
			</div>
  		</div>
  	</div>
</fieldset>