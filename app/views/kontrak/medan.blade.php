<link rel="stylesheet" href="{{ url('assets/lib/js/pace/themes/blue/pace-theme-loading-bar.css') }}">
<script src="{{ url('assets/lib/js/pace/pace.min.js') }}" type="text/javascript"></script>
<ol class="breadcrumb">
  <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><i class="fa fa-file-o"></i> Data Kontrak</li>
  <li><i class="fa fa-map-marker"></i> Medan</li>
</ol>
<br><br>
<div class="row"> 
    <div class="col-lg-12">
    <a class="btn btn-success" href="{{ url('kontrak/medan/create') }}"><i class="fa fa-plus"></i> Tambah Data</a>
    &nbsp;&nbsp;
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#search_form" aria-expanded="false" aria-controls="collapseExample">
    <i class="fa fa-search"></i> Form Pencarian
    </a>
    </div>
    <div class="collapse" id="search_form">
        <div class="col-lg-12" style="background:#ccc;padding-top: 25px; margin-top: 15px; margin-left: 20px; padding-right: 20px; width: 95%;">
            <form class="form-vertical" action="" method="POST">
                <div class="col-lg-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nomor Kontrak" name="nomor_kontrak" id="nomor_kontrak">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Tanggal Kontrak" name="tanggal_kontrak" id="tanggal_kontrak">
                    </div>

                    <div class="form-group">
                      {{ Form::select('jenis_asset', $jenis_asset, '', $attributes = array('style'=>'width:100%;', 'id'=>'jenis_asset')) }}
                    </div>

                     <div class="form-group">
                      {{ Form::select('user', $user, '', $attributes = array('style'=>'width:100%;', 'id'=>'user')) }}
                    </div>
                   
                </div>
                <div class="col-lg-6">
                   <div class="form-group">
                        <input type="text" class="form-control" placeholder="Lokasi" name="lokasi" id="lokasi">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Tujuan Penilaian" name="tujuan_penilaian" id="tujuan_penilaian">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Fee" name="fee" id="fee">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-info" id="search" style="margin-top: 0px;"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-lg-12">
    @if(Session::has('message'))
      <br><br>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <center>{{ Session::get('message') }}</center>
        </div>
    @else
        <br><br>
    @endif
    <script>
    $(document).ready(function(){
        $('#search').click(function() { oTable.fnReloadAjax (); });
    });
    </script>
    {{
        Datatable::table()
        ->addColumn('#','Nomor Kontrak','Tanggal','Jenis Aset', 'Lokasi', 'Tujuan Penilaian', 'Fee','Pimpinan Proyek','Action')
        ->setUrl(route('datatable.kontrak_medan', array('session_user'=>$session_user)))
        ->setCallbacks(["fnServerParams" => 'function ( aoData ) {           
                          aoData.push({ "name": "nomor_kontrak", "value": $(\'#nomor_kontrak\').val() });
                          aoData.push({ "name": "tanggal_kontrak", "value": $(\'#tanggal_kontrak\').val() });
                          aoData.push({ "name": "jenis_asset", "value": $(\'#jenis_asset\').val() });
                          aoData.push({ "name": "lokasi", "value": $(\'#lokasi\').val() });
                          aoData.push({ "name": "tujuan_penilaian", "value": $(\'#tujuan_penilaian\').val() });
                          aoData.push({ "name": "fee", "value": $(\'#fee\').val() });
                          aoData.push({ "name": "jc", "value": $(\'#tipe_pembangkit\').val() });
                        }'
                      ])
        ->render()
    }}
  </div>
</div>