<?php

class HakAksesController extends BaseController
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Hak Akses Menu';
	public $route	= 'hakakses';
	public $write;
	public $update;
	public $delete;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;
					}
				}
			}

			if ($i == 0)
				return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$view = View::make('hakakses.index');
		$view->form_action = url('hakakses/search');

		$menus = Menu::all();
		$menu = array();

		foreach ($menus as $val) {
			$menu[''] = 'Menu';
			$menu[$val->id] = $val->menu_name;
		}

		$levels = Level::all();
		$level = array();
		
		foreach ($levels as $val) {
			$level[''] = '-- Level --';
			$level[$val->id] = $val->level_name;
		}

		$view->menu = $menu;
		$view->level = $level;

		$view->write  = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;

		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url = 'general';
		$view = View::make('hakakses.form');
		$view->form_action = url('hakakses/store');
		$view->form_title = 'add';
		$view->action_title = 'Tambah Hak Akses Menu';

		$menus = Menu::all();
		$menu = array();

		foreach ($menus as $val) {
			$menu[''] = '-- Menu --';
			$menu[$val->id] = $val->menu_name;
		}

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = '-- Level --';
			$level[$val->id] = $val->level_name;
		}

		$view->menu = $menu;
		$view->level = $level;
		$this->layout->content = $view;
	}

	public function store() {
		$hakakses = new HakAkses;
		$hakakses->menu_id		= Input::get('menu');
		$hakakses->level_id		= Input::get('level');
		$hakakses->write 		= Input::get('write');
		$hakakses->update 		= Input::get('update');
		$hakakses->delete 		= Input::get('delete');
		$hakakses->save();
		return Redirect::to('hakakses')->with('message', 'HAK AKSES MENU BERHASIL DISIMPAN');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'general';
		$view = View::make('hakakses.form');
		$view->form_action = url('hakakses/update/'.$id.'');
		$view->action_title = 'Ubah Otoritas Menu';
		$view->form_title = 'edit';

		$menus = Menu::all();
		$menu = array();

		foreach ($menus as $val) {
			$menu[''] = '-- Silahkan Pilih Menu --';
			$menu[$val->id] = $val->menu_name;
		}

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = '-- Silahkan Pilih Level --';
			$level[$val->id] = $val->level_name;
		}

		$view->menu = $menu;
		$view->level = $level;

		$hakakses	= HakAkses::find($id);
		$menu_id	= Menu::find($hakakses->menu_id);
		$level_id 	= Level::find($hakakses->level_id);

		$view->menu_id  = $menu_id->id;
		$view->level_id = $level_id->id;

		if ($hakakses->write) {
			$view->writes  = $hakakses->write;
		}

		if($hakakses->update) {
			$view->updates = $hakakses->update;
		}

		if ($hakakses->delete == 1) {
			$view->deletes = $hakakses->delete;
		}

		$this->layout->content = $view;
		Session::flash('akses_id', $id);
	}

	public function update($id) {
		if (Session::has('login')) {
			$hakakses = HakAkses::find($id);
			$hakakses->menu_id		= Input::get('menu');
			$hakakses->level_id	= Input::get('level');
			$hakakses->write 		= Input::get('write');
			$hakakses->update 		= Input::get('update');
			$hakakses->delete 		= Input::get('delete');
			$hakakses->save();
			return Redirect::to('hakakses')->with('message', 'OTORITAS MENU BERHASIL DIUBAH');
		} else {
			return Redirect::to('/');
		}
	}

	public function destroy($id) {
		if (Session::has('login')) {
			$hakakses = HakAkses::find($id);
			$hakakses->delete();
			return Redirect::to('hakakses')->with('message', 'OTORITAS MENU BERHASIL DIHAPUS');
		} else {
			return Redirect::to('/');
		}
	}

}
