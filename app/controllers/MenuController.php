<?php

class MenuController extends BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Menu';
	public $route	= 'menu';
	public $write;
	public $update;
	public $delete;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;	
					}
				}
			}

			if ($i == 0) 
				return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$view = View::make('menu.index');
		$view->count = Menu::all()->count();
		$view->write  = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$this->layout->content = $view;		
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url = 'general';	
		$view = View::make('menu.form');
		$view->form_action = url('menu/store');
		$view->form_title = 'add';
		$view->action_title = 'Tambah Menu';
		$this->layout->content = $view;
	}

	public function store() {
		$menu = new Menu;
		$menu->menu_name 	= Input::get('name');
		$menu->route 		= Input::get('route');
		$menu->save();
		return Redirect::to('menu')->with('message', 'MENU BERHASIL DISIMPAN');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$view = View::make('menu.form');
		$view->form_action = url('menu/update/'.$id.'');
		$view->action_title = 'Ubah Menu';
		$view->form_title = 'edit';
		$view->menu = Menu::find($id);
		$this->layout->content = $view;
		Session::flash('menu_id', $id);
	}
	
	public function update($id) {
		$menu = Menu::find($id);
		$menu->menu_name 	= Input::get('name');
		$menu->route 		= Input::get('route');
		$menu->save();
		return Redirect::to('menu')->with('message', 'MENU BERHASIL DIUBAH');
	}
	
	public function destroy($id) {
		$menu = Menu::find($id);
		$menu->delete();
		return Redirect::to('menu')->with('message', 'MENU BERHASIL DIHAPUS');
	}

}