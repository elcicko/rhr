<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

class DatatableController extends \BaseController {

	public $i;
	public $column;
	public $query;
	public $provinsi;
	public $kota;
	public $kecamatan;
	public $desa;
	public $atl;
	public $type_pembangkit;
	public $imv_psm;
	public $company;
	public $name;
	public $phone;
	public $address;
	public $type;
	public $type_data;
	public $offering_jual;
	public $offering_transacted_price;
	public $nomor_kontrak;
	public $tanggal_kontrak;
	public $alamat;
	public $jenis_asset;
	public $tujuan_penilaian;
	public $fee;
	public $pimpinan_proyek;
	public $username;
	public $level;
	public $write;
	public $update;
	public $delete;
	public $detail;
	public $session_user;
	public $menu;
	public $luas;
	public $satuan;
	public $pic;

	public function getHakAkses($update, $delete) {
		$this->update = $update;
		$this->delete = $delete;
		$this->i = 0;

    	$this->column = array('id','menu_id','level_id','write','update','delete');

	    if (Input::has('menu')) {
	        $this->menu = Input::get('menu');
	    }

		if (Input::has('level')) {
	        $this->level = Input::get('level');
	    }

	    return Datatable::collection(HakAkses::searchMenu($this->menu)->searchLevel($this->level)->get($this->column))
	    	->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })
	    ->addColumn('menu_name',function($model) { return $model->menu->menu_name; })
	    ->addColumn('route',function($model) { return $model->menu->route; })
	    ->addColumn('level_name',function($model) { return $model->level->level_name; })
	    ->addColumn('Write', function($model) {
			$o = '<center>';
			if ($model->write == 1) {
        		$o  .= '<img src="assets/img/1.png">';
    		} else {
    			$o  .= '<img src="assets/img/0.png">';
    		}
    		$o .= '</center>';
      		return $o;
    	})

	    ->addColumn('Update', function($model) {
				$o = '<center>';
				if ($model->update == 1) {
	        	$o  .= '<img src="assets/img/1.png">';
	    	} else {
	    		$o  .= '<img src="assets/img/0.png">';
	    	}
	    	$o .= '</center>';
	      return $o;
	    })

	    ->addColumn('Delete', function($model) {
				$o = '<center>';
				if ($model->delete == 1) {
	        	$o  .= '<img src="assets/img/1.png">';
	    	} else {
	    		$o  .= '<img src="assets/img/0.png">';
	    	}
	    	$o .= '</center>';
	      return $o;
    	})

    	->addColumn('Action', function($model) {
			$o = '<center>';
			if ($this->update == 1) {
	        	$o  .= '<a href=hakakses/edit/'.$model->id.'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
	    	} else {
	    		$o  .= '<i class="fa fa-wrench"></i> &nbsp;&nbsp;';
	    	}

	  		if ($this->delete == 1) {
		        $o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
		        $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button class="close" data-dismiss="modal">×</button>
		                          <h4>Konfirmasi</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Anda Yakin Menghapus Data Ini ?</p>
		                      </div>
		                      <div class="modal-footer">
		                          <a class="btn btn-success" href="hakakses/destroy/'.$model->id.'">Ya</a>
		                          <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
		                      </div>
		                  </div>
		              </div>
		        	</div>';
		    } else {
		    	$o .= '<i class="fa fa-trash-o"></i>';
		    }
	   		$o .= '</center>';
	    	return $o;
    	})
	    ->searchColumns('menu_name','menu_route','level_name')
	    ->orderColumns('id','menu_name','menu_route', 'level_name')
	    ->make();
    }

	public function getMenu($update, $delete) {
		$this->update = $update;
		$this->delete = $delete;
		$this->i = 0;

		if (Input::has('q')) {
			$this->query = Input::get('q');
  		}

	    return Datatable::collection(Menu::all(array('id','menu_name','route')))
	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })
    	
    	->showColumns('menu_name', 'route')
    	->addColumn('Action', function($model) {
    		$o = '<center>';
    		if ($this->update == 1) {
            	$o  .= '<a href=menu/edit/'.$model->id.'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
        	} else {
        		$o  .= '<i class="fa fa-wrench"></i> &nbsp;&nbsp;';
        	}

        	if ($this->delete == 1) {
	            $o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
	            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
		                <div class="modal-dialog">
		                    <div class="modal-content">
		                        <div class="modal-header">
		                            <button class="close" data-dismiss="modal">×</button>
		                            <h4>Konfirmasi</h4>
		                        </div>
		                        <div class="modal-body">
		                            <p>Anda Yakin Menghapus Data Ini ?</p>
		                        </div>
		                        <div class="modal-footer">
		                            <a class="btn btn-success" href="menu/destroy/'.$model->id.'">Ya</a>
		                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
		                        </div>
		                    </div>
		                </div>
	            	</div>';
	      } else {
	      	$o .= '<i class="fa fa-trash-o"></i> Hapus';
	      }

	      $o .= '</center>';
	      return $o;
    	})

	    ->searchColumns('menu_name')
	    ->orderColumns('id','menu_name')
	    ->make();
  	}

	// GET User
	public function getUser($update, $delete) {
		$this->update = $update;
		$this->delete = $delete;
		$this->i = 0;

		$this->column = array('id','username','level_id');

    	if (Input::has('level')) {
        	$this->level = Input::get('level');
    	}

    	if (Input::has('username')) {
        	$this->username = Input::get('username');
    	}

        return Datatable::collection(User::searchLevel($this->level)->searchNotAdmin()->get($this->column))
         ->addColumn('no',function($model) {
        	$o = '<center>'.++$this->i.'</center>';
        	return $o;
        })

        ->addColumn('username',function($model) { return $model->username; })
        ->addColumn('level_id',function($model) { return $model->level->level_name; })
        ->addColumn('Action', function($model) {
    		$o = '<center>';
    		if ($this->update == 1) {
            	$o  .= '<a href=user/edit/'.$model->id.'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
        	} else {
        		$o  .= '<i class="fa fa-wrench"></i> &nbsp;&nbsp;';
        	}

        	if ($this->delete == 1) {
	            $o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
	            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
		                <div class="modal-dialog">
		                    <div class="modal-content">
		                        <div class="modal-header">
		                            <button class="close" data-dismiss="modal">×</button>
		                            <h4>Konfirmasi</h4>
		                        </div>
		                        <div class="modal-body">
		                            <p>Anda Yakin Menghapus Data Ini ?</p>
		                        </div>
		                        <div class="modal-footer">
		                            <a class="btn btn-success" href="user/destroy/'.$model->id.'">Ya</a>
		                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
		                        </div>
		                    </div>
		                </div>
	            	</div>';
	        } else {
	        	$o .= '<i class="fa fa-trash-o"></i>';
	        }
	        $o .= '</center>';
	        return $o;
        })
        ->searchColumns('level_id','username')
        ->orderColumns('id')
        ->make();
    }

    // GET Aset
	public function getAset($session_user, $update, $delete) {
		$this->i = 0;
		$this->update = $update;
		$this->delete = $delete;
		$this->session_user = $session_user;

		$this->column = array('id','nama_asset_1','provinsi','kota','kecamatan','desa','atl','kode_company','type_pembangkit','imv_psm');
		
		if (Input::has('q')) {
			$this->query = Input::get('q');
		}

		if (Input::has('provinsi')) {
			$this->provinsi = Input::get('provinsi');
		}

		if (Input::has('kabkota')) {
			$this->kota = Input::get('kabkota');
		}

		if (Input::has('kecamatan')) {
			$this->kecamatan = Input::get('kecamatan');
		}

		if (Input::has('kelurahan')) {
			$this->desa = Input::get('kelurahan');
		}

		if (Input::has('atl')) {
			$this->atl = Input::get('atl');
		}

		if (Input::has('company')) {
			$this->company = Input::get('company');
		}

		if (Input::has('imv_psm')) {
			$this->imv_psm = Input::get('imv_psm');
		}

		if (Input::has('tipe_pembangkit')) {
			$this->type_pembangkit = Input::get('tipe_pembangkit');
		}

    	return Datatable::collection(Asset::SearchAset($this->query)
											->SearchProvinsi($this->provinsi)
											->SearchKota($this->kota)
											->SearchKecamatan($this->kecamatan)
											->SearchDesa($this->desa)
											->SearchATL($this->atl)
											->SearchIMV($this->imv_psm)
											->SearchPembangkit($this->type_pembangkit)
											->SearchCompany($this->company)
											->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('id_asset',function($model) { return $model->id; })
	    ->addColumn('nama_asset_1',function($model) { return $model->nama_asset_1; })
	    ->addColumn('provinsi',function($model) { return $model->provinsi; })
	    ->addColumn('kota',function($model) { return $model->kota; })
	    ->addColumn('kecamatan',function($model) { return $model->kecamatan; })
	    ->addColumn('desa',function($model) { return $model->desa; })
	    ->addColumn('atl',function($model) { return $model->atl; })
	    ->addColumn('kode_company',function($model) { return $model->kode_company; })
	    ->addColumn('type_pembangkit',function($model) { return $model->type_pembangkit; })
	    ->addColumn('imv_psm',function($model) { return $model->imv_psm; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';
    		
    		if (($model->pic == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href=aset/detail/'.$model->id.'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1) {
            	$o .= '<a href=aset/edit/'.$model->id.'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1) {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="aset/destroy/'.$model->id.'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('id_asset','nama_asset_1','provinsi','kota','kecamatan','desa','atl','type_pembangkit','imv_psm','kode_company')
	    ->orderColumns('id_asset')
	    ->make();
  	}

  	// GET Pembanding
	public function getPembanding($update, $delete) {
		$this->i = 0;
		$this->update = $update;
		$this->delete = $delete;

		$this->column = array('id','name','phone','address','type','offering_jual','offering_transacted_price');
		
		if (Input::has('q')) {
			$this->query = Input::get('q');
		}

		if (Input::has('name')) {
			$this->name = Input::get('name');
		}

		if (Input::has('phone')) {
			$this->phone = Input::get('phone');
		}

		if (Input::has('address')) {
			$this->address = Input::get('address');
		}
/*
		if (Input::has('type')) {
			$this->type = Input::get('type');
		}*/

		if (Input::has('offering_jual')) {
			$this->offering_jual = Input::get('offering_jual');
		}

		/*if (Input::has('offering_transacted_price')) {
			$this->offering_transacted_price = Input::get('offering_transacted_price');
		}*/

    	return Datatable::collection(Pembanding::SearchName($this->name)
												->SearchPhone($this->phone)
												->SearchAddress($this->address)
												/*->SearchType($this->type)*/
												/*->SearchTypeData($this->type_data)*/
												->SearchOfferingJual($this->offering_jual)
												/*->SearchOfferingTransaction($this->offering_transacted_price)*/
												->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('name',function($model) { return $model->name; })
	    ->addColumn('phone',function($model) { return $model->phone; })
	    ->addColumn('address',function($model) { return $model->address; })
	    ->addColumn('type',function($model) { return $model->type; })
	    /*->addColumn('type_data',function($model) { return $model->type_data; })*/
	    ->addColumn('offering_jual',function($model) { return $model->offering_jual; })
	    ->addColumn('offering_transacted_price',function($model) { return $model->offering_transacted_price; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pic == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href=pembanding/detail/'.$model->id.'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1) {
            	$o .= '<a href=pembanding/edit/'.$model->id.'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1) {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="pembanding/destroy/'.$model->id.'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('id_asset','name','phone','address','type','type_data','offering_jual','offering_transacted_price')
	    ->orderColumns('id_asset')
	    ->make();
  	}

  	// GET Kontrak Jakarta
	public function getKontrakJakarta($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchJakarta()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/jakarta/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/jakarta/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/jakarta/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Kontrak Surabaya
	public function getKontrakSurabaya($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchSurabaya()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/surabaya/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/surabaya/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/surabaya/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Kontrak Medan
	public function getKontrakMedan($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchMedan()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/medan/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/medan/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/medan/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Kontrak Bali
	public function getKontrakBali($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchBali()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/bali/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/bali/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/bali/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Kontrak Makassar
	public function getKontrakMakassar($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchMakassar()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/makassar/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/makassar/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/makassar/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Kontrak Solo
	public function getKontrakSolo($session_user) {
		$this->i = 0;
		$this->session_user = $session_user;

		$this->column = array('id','nomor_kontrak','tanggal_kontrak','jenis_asset','alamat','tujuan_penilaian','fee','pimpinan_proyek');
		
		if (Input::has('nomor_kontrak')) {
			$this->nomor_kontrak = Input::get('nomor_kontrak');
		}

		if (Input::has('tanggal_kontrak')) {
			$this->tanggal_kontrak = Input::get('tanggal_kontrak');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('tujuan_penilaian')) {
			$this->tujuan_penilaian = Input::get('tujuan_penilaian');
		}

		if (Input::has('fee')) {
			$this->fee = Input::get('fee');
		}

		if (Input::has('pimpinan_proyek')) {
			$this->pimpinan_proyek = Input::get('pimpinan_proyek');
		}

    	return Datatable::collection(Kontrak::SearchNomorKontrak($this->nomor_kontrak)
											  ->SearchTanggalKontrak($this->tanggal_kontrak)
											  ->SearchJenisAset($this->jenis_asset)
											  ->SearchAlamat($this->alamat)
											  ->SearchTujuanPenilaian($this->tujuan_penilaian)
											  ->SearchFee($this->fee)
											  ->SearchPimpinanProyek($this->pimpinan_proyek)
											  ->SearchSolo()
											  ->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('nomor_kontrak',function($model) { return $model->nomor_kontrak; })
	    ->addColumn('tanggal_kontrak',function($model) { return $model->tanggal_kontrak; })
	    ->addColumn('jenis_asset',function($model) { return $model->jenis_asset; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('tujuan_penilaian',function($model) { return $model->tujuan_penilaian; })
	    ->addColumn('fee',function($model) { return "Rp ". number_format($model->fee ,2, ',' , '.' ); })
	    ->addColumn('pimpinan_proyek',function($model) { return $model->pimpinan_proyek; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';

    		if (($model->pimpinan_proyek == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('kontrak/solo/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		if ($this->update == 1 || $this->session_user == 'admin') {
            	$o .= '<a href='.url('kontrak/solo/edit/'.$model->id.'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1 || $this->session_user == 'admin') {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('kontrak/solo/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('nomor_kontrak','tanggal_kontrak','alamat','jenis_asset','tujuan_penilaian','fee','pimpinan_proyek')
	    ->orderColumns('id')
	    ->make();
  	}

  	// GET Aset
	public function getAsetKontrak($session_user, $update, $delete, $nomor_kontrak) {
		$this->i = 0;
		$this->update = $update;
		$this->delete = $delete;
		$this->nomor_kontrak = $nomor_kontrak;
		$this->session_user = $session_user;

		$this->column = array('id','nama_asset_1','alamat','jenis_aset','luas','pic');
		
		if (Input::has('q')) {
			$this->query = Input::get('q');
		}

		if (Input::has('alamat')) {
			$this->alamat = Input::get('alamat');
		}

		if (Input::has('jenis_asset')) {
			$this->jenis_asset = Input::get('jenis_asset');
		}

		if (Input::has('luas')) {
			$this->luas = Input::get('luas');
		}

		if (Input::has('pic')) {
			$this->pic = Input::get('pic');
		}

    	return Datatable::collection(Asset::SearchAset($this->query)
											->SearchAlamat($this->alamat)
											->SearchJenisAset($this->jenis_asset)
											->SearchLuas($this->luas)
											->SearchPIC($this->pic)
											->SearchNomorKontrak($this->nomor_kontrak)
											->get($this->column))

	    ->addColumn('no',function($model) {
	    	$o = '<center>'.++$this->i.'</center>';
	    	return $o;
	    })

	    ->addColumn('id_asset',function($model) { return $model->id; })
	    ->addColumn('nama_asset_1',function($model) { return $model->nama_asset_1; })
	    ->addColumn('alamat',function($model) { return $model->alamat; })
	    ->addColumn('jenis_aset',function($model) { return $model->jenis_aset; })
	    ->addColumn('luas',function($model) { return $model->luas; })
	    ->addColumn('pic',function($model) { return $model->pic; })
	    ->addColumn('Action', function($model) {
    		$o  = '<center>';
    		
    		if (($model->pic == $this->session_user) || ($this->session_user == 'admin')) {
    			$o .= '<a href='.url('aset/detail/'.$model->id.'').'><i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		} else {
    			$o .= '<i class="fa fa-search"></i></a> &nbsp;&nbsp;';
    		}

    		$kontrak = Kontrak::where('nomor_kontrak', $this->nomor_kontrak)->first();

    		if ($this->update == 1) {
            	$o .= '<a href='.url('aset/edit_asetkontrak/'.$model->id.'/'.strtolower($kontrak['lokasi']).'').'><i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            	if (($model->jenis_aset == "Tanah") || ($model->jenis_aset == "Ruko")) {
            		$o .= '<a href='.url('aset/tanah/'.$model->id.'').'><i class="fa fa-refresh"></i></a> &nbsp;&nbsp;';
            	} elseif ($model->jenis_aset == "Bangunan") {
            		$o .= '<a href='.url('aset/bangunan/'.$model->id.'').'><i class="fa fa-refresh"></i></a> &nbsp;&nbsp;';
            	}
            } else {
            	$o .= '<i class="fa fa-wrench"></i></a> &nbsp;&nbsp;';
            	$o .= '<i class="fa fa-refresh"></i></a> &nbsp;&nbsp;';
            }

            if ($this->delete == 1) {
            	$o .= '<a data-toggle="modal" href="#confirm'.$model->id.'"><i class="fa fa-trash-o"></i></a>';
            } else {
            	$o .= '<i class="fa fa-trash-o"></i></a>';
            }

            $o .= '<div class="modal fade" id="confirm'.$model->id.'" style="display:none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button class="close" data-dismiss="modal">×</button>
	                            <h4>Confirm</h4>
	                        </div>
	                        <div class="modal-body">
	                            <p>Anda Yakin Menghapus Data Ini ?</p>
	                        </div>
	                        <div class="modal-footer">
	                            <a class="btn btn-success" href="'.url('aset/destroy/'.$model->id.'').'">Ya</a>
	                            <a href="#" data-dismiss="modal" class="btn btn-danger">Tidak</a>
	                        </div>
	                    </div>
	                </div>
            	</div>';
	     
	        $o .= '</center>';
	        return $o;
	    })
	    ->searchColumns('id_asset','nama_asset_1','alamat','jenis_aset','luas','pic')
	    ->orderColumns('id_asset')
	    ->make();
  	}
}