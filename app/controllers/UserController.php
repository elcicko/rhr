<?php

class UserController extends \BaseController
{

	public $layout 	= 'layouts.default';
	public $title  	= 'DATA USER';
	public $route	= 'user';
	public $write;
	public $update;
	public $delete;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;
					}
				}
			}

			if ($i == 0)
				return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'general';
			$view = View::make('user.index');
			$view->count = User::all()->count();
			$levels = Level::all();
			$level = array();

			foreach ($levels as $val) {
				$level[''] = 'Silahkan Pilih Level';
				$level[$val->id] = $val->level_name;
			}

			$view->level  = $level;
			$view->write  = $this->write;
			$view->update = $this->update;
			$view->delete = $this->delete;
			$this->layout->content = $view;

		} else {
			return Redirect::to('/');
		}
	}

	public function create() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'general';
			$view = View::make('user.form');
			$view->form_action = url('user/store');
			$view->action_title = 'Tambah User';

			$levels = Level::where('id','!=', 4)->get();
			$level = array();

			foreach ($levels as $val) {
				$level[''] = '-- Level --';
				$level[$val->id] = $val->level_name;
			}

			$view->level = $level;
			$this->layout->content = $view;
		} else {
			return Redirect::to('/');
		}
	}


	public function store() {
		$user = new User;
		$user->username = Input::get('username');
		$user->password = md5('user');
		$user->level_id = Input::get('level');
		$user->save();
		return Redirect::to('user')->with('message', 'USER BERHASIL DISIMPAN');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'general';
		$view = View::make('user.form');
		$view->form_action = url('user/update/'.$id.'');
		$view->action_title = 'Ubah User';
		$view->user = User::find($id);

		$levels = Level::where('id','!=', 4)->get();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = '-- Level --';
			$level[$val->id] = $val->level_name;
		}

		$view->level = $level;
		$this->layout->content = $view;
		Session::flash('user_id', $id);
		
	}


	public function update($id) {
		$user = User::find($id);
		$user->username = Input::get('username');
		$user->password = md5('user');
		$user->level_id = Input::get('level');
		$user->save();
		return Redirect::to('user')->with('message', 'USER BERHASIL DISIMPAN');
	}


	public function destroy($id) {
		$user = User::find($id);
		$user->delete();
		return Redirect::to('user')->with('message', 'USER BERHASIL DIHAPUS');
	}

}
