<?php

class ImportControllerController extends \BaseController
{

	public $layout 	= 'layouts.default';
	public $title  	= 'IMPORT DATA';
	public $route	= 'import';

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {

					}
				}
			}

			if ($i == 0)
				return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url = 'import';
		$view = View::make('import.form');
		$view->form_action = url('import/import_process');
		$view->action_title = 'Import Data';

		$destinations = array('kontrak'=>'Data Kontrak','aset'=>'Data Aset','pembanding'=>'Data Pembanding');
		$dest = array();

		foreach ($destinations as $key=>$val) {
			$dest[''] = '-- Destinasi Import --';
			$dest[$key] = $val;
		}

		$this->layout->content = $view;
	}

	public function import_process() {
		$dest 	 = Input::get('dest');
		$target  = '';
		$data = array();

		if ($dest == 'kontrak') {
			$target = 'import_kontrak';
		} elseif ($dest == 'aset') {
			$target = 'import_aset';
		} elseif ($dest == 'pembanding') {
			$target = 'import_pembanding';
		}

		return Redirect::route($target, array('import_data' => $data));
	}

	public function import_kontrak() {
		$import_data = Input::get('import_data');
	}

	public function import_aset() {
		$import_data = Input::get('import_data');
	}

	public function import_aset() {
		$import_data = Input::get('import_data');
	}

}
