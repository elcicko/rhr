<?php

class LoginController extends BaseController 
{
	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	}

	public function index() {
		return View::make('login', array('form_action' => url('validate')));
	}

	public function validate() {
        $data = array();
        $user_data['username'] = Input::get('username');
        $user_data['password'] = md5(Input::get('password'));
        $user = User::where($user_data)->first();
        if ($user) {
	        Session::put('user', $user);
	        Session::put('login', TRUE);
            
            $akses = DB::table('akses_menu')
                        ->join('menu', 'menu.id', '=', 'akses_menu.menu_id')
                        ->where('akses_menu.level_id', $user->level_id)
                        ->select('menu.route', 'akses_menu.write', 'akses_menu.update', 'akses_menu.delete')
                        ->get();

            Session::push('akses', $akses);
                
            return Redirect::to('dashboard');
        } else {
        	return Redirect::to('login')->with('error', 'USERNAME ATAU PASSWORD SALAH')->withInput();
        }	
    }

    public function logout() {
    	Auth::logout();
        Session::flush();
        return Redirect::to('login');
    }
}