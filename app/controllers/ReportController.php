<?php

class ReportController extends \BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Laporan Aset';
	public $route	= 'report';
	public $write;
	public $update;
	public $delete;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses_menu');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;	
					}
				}
			}

			if ($i == 0) 
				return Redirect::to('dashboard')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}
	
	public function index() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'report';	

			$view = View::make('report.index');
			$view->form_action = url('printreport');
			$view->action_title = 'Cetak Laporan Aset';

			$provinsis = Provinsi::all();
			$kabkotas  = Kabkota::all();
			$types	   = array('pdf'=>'Laporan PDF', 'excel'=>'Laporan Excel');

			$provinsi = array();
			$kabkota  = array();
			$type	  = array();

			foreach ($provinsis as $val) {
				$provinsi[''] = '-- Silahkan Pilih Provinsi --';
				$provinsi[$val->id] = $val->nama_provinsi;
			}

			foreach ($kabkotas as $val) {
				$kabkota[''] = '-- Silahkan Pilih Kabupaten / Kota --';
				$kabkota[$val->id] = $val->nama_kabkota;
			}

			foreach ($types as $key=>$val) {
				$type[''] = '-- Silahkan Pilih Jenis Cetakan --';
				$type[$key] = $val;
			}
			
			$view->provinsi = $provinsi;
			$view->kabkota  = $kabkota;
			$view->type 	= $type;
			
			$this->layout->content = $view;
		} else {
			return Redirect::to('/');
		}
	}

	public function printreport() {
		$prov = Input::get('provinsi');
		$kabkota = Input::get('kabkota');
		$type = Input::get('type');

		$target = '';

		if ($type == 'pdf') {
			$target = 'pdf';
		} else {
			$target = 'excel';
		}

		return Redirect::route($target, array('prov' => $prov, 'kabkota' => $kabkota));

	}

	public function pdf() {
		$kab  = Input::get('kabkota');
		$prov = Input::get('prov');
		
		$provinsi = Provinsi::find($prov);
		$kabkota  = Kabkota::find($kab);

		$asets = Aset::SearchProvinsi($prov)->SearchKabkota($kab)->get();
		$view = View::make('report.pdf');

		$view->asets = $asets;
		$view->kabkota  = isset($kabkota) ? $kabkota->nama_kabkota : '-';
		$view->provinsi = isset($provinsi) ? $provinsi->nama_provinsi : '-';
		
		$pdf = App::make('dompdf');
		$pdf->loadHTML($view)->setPaper('a4')->setOrientation('potrait');
		return $pdf->stream();
	}

	public function excel() {
		Excel::create('Laporan Aset', function($excel) {
    		$excel->sheet('Laporan Aset ', function($sheet) {
    			$kab  = Input::get('kabkota');
				$prov = Input::get('prov');
				
				$provinsi = Provinsi::find($prov);
				$kabkota  = Kabkota::find($kab);

				$asets 	= Aset::SearchProvinsi($prov)->SearchKabkota($kab)->get();
				$header = array('Nomor Aset', 'Nama Aset', 'Provinsi', 'Kabupaten / Kota', 'Alamat');
				$aset 	= array();

				$i = 0;
				foreach ($asets as $val) {
					$aset[$i]['Nomor Aset'] = $val->nomor_aset;
					$aset[$i]['Nama Aset'] = $val->nama_aset;
					$aset[$i]['Provinsi'] = $val->provinsi->nama_provinsi;
					$aset[$i]['Kabupaten / Kota'] = $val->kabkota->nama_kabkota;
					$aset[$i]['Alamat'] = $val->alamat;
					$i++;
				}

		        $sheet->fromArray($aset);
    		});
		})->export('xlsx');
	}

}