<?php

class ProvinsiController extends BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Manajemen Provinsi';
	public $route	= 'provinsi';
	public $write;
	public $update;
	public $delete;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses_menu');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;	
					}
				}
			}

			if ($i == 0) 
				return Redirect::to('dashboard')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'master';	
			$view = View::make('provinsi.index');
			$view->provinsis = Provinsi::paginate(20);
			$view->count = Provinsi::all()->count();

			$view->write  = $this->write;
			$view->update = $this->update;
			$view->delete = $this->delete;
			
			$this->layout->content = $view;

		} else {
			return Redirect::to('/');
		}
	}


	public function create() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'master';	
			$view = View::make('provinsi.form');
			$view->form_action = url('provinsi/store');
			$view->form_title = 'add';
			$view->action_title = 'Tambah Provinsi';
			$this->layout->content = $view;
		} else {
			return Redirect::to('/');
		}
	}

	
	public function store() {
		if (Session::has('login')) {
			$provinsi = new Provinsi;
			$provinsi->nama_provinsi 	= Input::get('nama_provinsi');
			$provinsi->latitude 		= Input::get('latitude');
			$provinsi->longitude 		= Input::get('longitude');
			$provinsi->save();
			return Redirect::to('provinsi')->with('message', 'PROVINSI BERHASIL DISIMPAN');
		} else {
			return Redirect::to('/');
		}
	}

	public function edit($id) {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'master';	
			$view = View::make('provinsi.form');
			$view->form_action = url('provinsi/update/'.$id.'');
			$view->action_title = 'Ubah Provinsi';
			$view->form_title = 'edit';
			$view->provinsi = Provinsi::find($id);
			$this->layout->content = $view;
			Session::flash('provinsi_id', $id);
		} else {
			return Redirect::to('/');
		}
	}

	
	public function update($id) {
		if (Session::has('login')) {
			$provinsi 					= Provinsi::find($id);
			$provinsi->nama_provinsi 	= Input::get('nama_provinsi');
			$provinsi->latitude 		= Input::get('latitude');
			$provinsi->longitude 		= Input::get('longitude');
			$provinsi->save();
			return Redirect::to('provinsi')->with('message', 'PROVINSI BERHASIL DIUBAH');
		} else {
			return Redirect::to('/');
		}
	}

	
	public function destroy($id) {
		if (Session::has('login')) {
			$provinsi = Provinsi::find($id);
			$provinsi->delete();
			return Redirect::to('provinsi')->with('message', 'PROVINSI BERHASIL DIHAPUS');
		} else {
			return Redirect::to('/');
		}
	}

}