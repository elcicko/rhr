<?php

class BackendController extends \BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Dashboard';
	public $route	= 'dashboard';

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url = 'dashboard';	

		$view = View::make('dashboard.index');

		$atls = array('1' => 'ATL 1',
            '2' => 'ATL 2',
            '3' => 'ATL 3',
            '4' => 'ATL 4',
            '5' => 'ATL 5',
            '6' => 'ATL 6',
            '7' => 'ATL 7',
            '8' => 'ATL 8',
            '9' => 'ATL 9',
            '10' => 'ATL 10'
        );


		foreach ($atls as $key=>$val) {
			$atl[''] = '-- ATL --';
			$atl[$key] = $val;
		}

		$view->atl = $atl;
		$this->layout->content = $view;
	}
}