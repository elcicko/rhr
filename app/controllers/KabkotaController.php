<?php

class KabkotaController extends BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Manajemen Kabupaten / Kota';
	public $route	= 'kabkota';

	
	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url = 'master';	
		$view = View::make('kabkota.index');
		$view->form_action = url('kabkota/search');
		$view->kabkotas = Kabkota::paginate(50);
		$view->count = Kabkota::all()->count();

		$provinsis = Provinsi::all();
		$provinsi = array();

		foreach ($provinsis as $val) {
			$provinsi[''] = '-- Silahkan Pilih Provinsi --';
			$provinsi[$val->id] = $val->nama_provinsi;
		}

		$view->provinsi = $provinsi;
		$view->write  = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->search = FALSE;
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url = 'master';	
		$view = View::make('kabkota.index');
		$view->form_action = url('kabkota/search');

		$prov = Input::get('provinsi');
		$kabkota = Input::get('kabkota');
		$view->kabkotas = Kabkota::searchKabkota($kabkota)->searchProvinsi($prov)->paginate(50);
		$view->count = Kabkota::searchKabkota($kabkota)->searchProvinsi($prov)->count();

		$provinsis = Provinsi::all();
		$provinsi = array();

		foreach ($provinsis as $val) {
			$provinsi[''] = '-- Silahkan Pilih Provinsi --';
			$provinsi[$val->id] = $val->nama_provinsi;
		}

		$view->provinsi = $provinsi;
		$view->write  = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->search = TRUE;
		$this->layout->content = $view;
	
	}

	public function create() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'master';	
			$view = View::make('kabkota.form');
			
			$provinsis = Provinsi::all();
			$provinsi = array();

			foreach ($provinsis as $val) {
				$provinsi[''] = '-- Silahkan Pilih Provinsi --';
				$provinsi[$val->latitude."|".$val->longitude."|".$val->id] = $val->nama_provinsi;
			}
			
			$view->provinsi = $provinsi;

			$view->form_action = url('kabkota/store');
			$view->form_title = 'add';
			$view->action_title = 'Tambah Kabupaten / Kota';
			$this->layout->content = $view;
		} else {
			return Redirect::to('/');
		}
	}
	
	public function store() {
		if (Session::has('login')) {
			$provinsi = explode('|', Input::get('provinsi'));
			$kabkota = new Kabkota;
			$kabkota->nama_kabkota	= Input::get('nama_kabkota');
			$kabkota->provinsi_id	= $provinsi[2];
			$kabkota->latitude 		= Input::get('latitude');
			$kabkota->longitude 	= Input::get('longitude');
			$kabkota->save();
			return Redirect::to('kabkota')->with('message', 'KABUPATEN / KOTA BERHASIL DISIMPAN');
		} else {
			return Redirect::to('/');
		}
	}

	public function edit($id) {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'master';	
			$view = View::make('kabkota.form');
			$view->form_action = url('kabkota/update/'.$id.'');
			$view->action_title = 'Ubah Kabupaten / Kota';
			$view->form_title = 'edit';

			$provinsis = Provinsi::all();
			$provinsi = array();

			foreach ($provinsis as $val) {
				$provinsi[''] = '-- Silahkan Pilih Provinsi --';
				$provinsi[$val->latitude."|".$val->longitude."|".$val->id] = $val->nama_provinsi;
			}
			
			$view->provinsi = $provinsi;

			$kabkota = Kabkota::find($id);
			$prov = Provinsi::find($kabkota->provinsi_id);
			
			$view->kabkota = $kabkota;
			$view->prov = $prov->latitude."|".$prov->longitude."|".$prov->id;
			$this->layout->content = $view;
			Session::flash('kabkota_id', $id);
		} else {
			return Redirect::to('/');
		}
	}

	public function update($id) {
		if (Session::has('login')) {
			$provinsi = explode('|', Input::get('provinsi'));
			$kabkota = Kabkota::find($id);
			$kabkota->nama_kabkota 	= Input::get('nama_kabkota');
			$kabkota->provinsi_id 	= $provinsi[2];
			$kabkota->latitude 		= Input::get('latitude');
			$kabkota->longitude 	= Input::get('longitude');
			$kabkota->save();
			return Redirect::to('kabkota')->with('message', 'KABUPATEN / KOTA BERHASIL DIUBAH');
		} else {
			return Redirect::to('/');
		}
	}

	public function destroy($id) {
		if (Session::has('login')) {
			$kabkota = Kabkota::find($id);
			$kabkota->delete();
			return Redirect::to('kabkota')->with('message', 'KABUPATEN / KOTA BERHASIL DIHAPUS');
		} else {
			return Redirect::to('/');
		}
	}

	

    public function getkabkotabyprov($provinsi) {
		$this->layout->title = $this->title;
		$this->layout->url = 'master';	
		$view = '';
		$this->layout->content = '';
        $provinsi = urldecode(html_entity_decode($provinsi));
        $kabkotas = DB::table('lokasi')->groupBy('kabupaten')->groupBy('jenis')->where('provinsi', $provinsi)->get();
        $data = "<option selected value=''>-- Silahkan Pilih Kota / Kabupaten --</option>";
        foreach ($kabkotas as $obj) {
        	if ($obj->jenis == 'Kota') {
          		$data .= "<option value='".$obj->jenis."|" . $obj->kabupaten . "'>".$obj->jenis." " . $obj->kabupaten . "</option>";
          	} elseif ($obj->jenis == 'Kab.') {
          		$data .= "<option value='".$obj->jenis."|" . $obj->kabupaten . "'>".$obj->jenis." " . $obj->kabupaten . "</option>";
          	}
        }
        return $data;
    }

    public function getkecamatanbykabkota($kabkota) {
		$this->layout->title = $this->title;
		$this->layout->url = 'master';	
		$view = '';
		$this->layout->content = '';
        $kabkota = urldecode(html_entity_decode($kabkota));
        $kabkota_temp = explode("|", $kabkota);
        $kecamatans = DB::table('lokasi')->groupBy('kecamatan')->where(array('jenis' => $kabkota_temp[0],'kabupaten'=>$kabkota_temp[1] ))->get();
        $data = "<option selected value=''>-- Silahkan Pilih Kecamatan --</option>";
        foreach ($kecamatans as $obj) {
          	$data .= "<option value='". $obj->kecamatan . "'>". $obj->kecamatan . "</option>";
        }
        return $data;
    }

    public function getkelurahanbykec($kecamatan) {
		$this->layout->title = $this->title;
		$this->layout->url = 'master';	
		$view = '';
		$this->layout->content = '';
        $kecamatan = urldecode(html_entity_decode($kecamatan));
        $kelurahans = DB::table('lokasi')->groupBy('kelurahan')->where('kecamatan', $provinsi)->get();
        $data = "<option selected value=''>-- Silahkan Pilih Kelurahan --</option>";
        foreach ($kelurahans as $obj) {
        	$data .= "<option value='". $obj->kelurahan . "'>". $obj->kelurahan . "</option>";
        }
        return $data;
    }

}