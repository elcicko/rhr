<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

use Illuminate\Database\Eloquent\ModelNotFoundException;

class AsetController extends BaseController {

    public $layout = 'layouts.default';
    public $title = 'DATA ASSET';
    public $route = 'aset';
    public $write;
    public $update;
    public $delete;
    public $session_user;

    public function __construct() {
        $this->beforeFilter('@filterRequest');
        DB::connection()->disableQueryLog();
    }

    public function filterRequest() {
        if (Session::has('login')) {
            $akses = Session::get('akses');
            $user = Session::get('user');
            $i = 0;
            foreach ($akses as $key => $values) {
                foreach ($values as $val) {
                    if ($val->route == $this->route) {
                        $this->write = $val->write;
                        $this->update = $val->update;
                        $this->delete = $val->delete;
                        $i++;
                    }
                }
            }
            $this->session_user = $user->username;
            if ($i == 0)
                return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
        } else {
            return Redirect::to('login');
        }
    }

    public function index() {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.index');
        $view->form_action = url('aset/search');

        $provinsis = Lokasi::groupBy('provinsi')->get();
        $companies = Company::all();
        $provinsi = array();

        $atls = array('1' => 'ATL 1',
            '2' => 'ATL 2',
            '3' => 'ATL 3',
            '4' => 'ATL 4',
            '5' => 'ATL 5',
            '6' => 'ATL 6',
            '7' => 'ATL 7',
            '8' => 'ATL 8',
            '9' => 'ATL 9',
            '10' => 'ATL 10'
        );

        foreach ($provinsis as $val) {
            $provinsi[''] = '-- Provinsi --';
            $provinsi[$val->provinsi] = strtoupper($val->provinsi);
        }

        foreach ($companies as $val) {
            $company[''] = '-- Company --';
            $company[$val->kode] = $val->nama;
        }

        foreach ($atls as $key => $val) {
            $atl[''] = '-- ATL --';
            $atl[$key] = $val;
        }

        $kabkota[''] = '-- Kabupaten / Kota --';
        $kecamatan[''] = '-- Kecamatan --';
        $kelurahan[''] = '-- Kelurahan / Desa --';
        $tipe_pembangkit[''] = '-- Tipe Pembangkit --';

        $view->provinsi = $provinsi;
        $view->kabkota = $kabkota;
        $view->kecamatan = $kecamatan;
        $view->kelurahan = $kelurahan;
        $view->tipe_pembangkit = $tipe_pembangkit;
        $view->atl = $atl;
        $view->company = $company;
        $view->update = $this->update;
        $view->delete = $this->delete;
        $view->write = $this->write;
        $view->session_user = $this->session_user;

        $this->layout->content = $view;
    }

    public function index_peta() {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.index_peta');
        $view->form_action = url('aset/search');

        $provinsis = Lokasi::groupBy('provinsi')->get();
        $companies = Company::all();
        $provinsi = array();

        $atls = array('1' => 'ATL 1',
            '2' => 'ATL 2',
            '3' => 'ATL 3',
            '4' => 'ATL 4',
            '5' => 'ATL 5',
            '6' => 'ATL 6',
            '7' => 'ATL 7',
            '8' => 'ATL 8',
            '9' => 'ATL 9',
            '10' => 'ATL 10'
        );

        foreach ($provinsis as $val) {
            $provinsi[''] = '-- Provinsi --';
            $provinsi[$val->provinsi] = strtoupper($val->provinsi);
        }

        foreach ($companies as $val) {
            $company[''] = '-- Company --';
            $company[$val->kode] = $val->nama;
        }

        foreach ($atls as $key => $val) {
            $atl[''] = '-- ATL --';
            $atl[$key] = $val;
        }

        $kabkota[''] = '-- Kabupaten / Kota --';
        $kecamatan[''] = '-- Kecamatan --';
        $kelurahan[''] = '-- Kelurahan / Desa --';
        $tipe_pembangkit[''] = '-- Tipe Pembangkit --';

        $view->provinsi = $provinsi;
        $view->kabkota = $kabkota;
        $view->kecamatan = $kecamatan;
        $view->kelurahan = $kelurahan;
        $view->tipe_pembangkit = $tipe_pembangkit;
        $view->atl = $atl;
        $view->company = $company;
        $view->update = $this->update;
        $view->delete = $this->delete;
        $view->write = $this->write;
        $view->session_user = $this->session_user;

        $this->layout->content = $view;
    }
    
    public function create_asetkontrak($lokasi, $nomor_kontrak) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.form_asetkontrak');
        $view->form_action = url('aset/store_asetkontrak/' . $lokasi . '');
        $view->action_title = 'Tambah Aset';
        $view->form_title = 'add';

        $users = User::where('level_id', '3')->get();
        $asset_types = array('Tanah' => 'Tanah',
        					 'Bangunan' => 'Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

        $pic = array();
        $jenis_asset = array();

        if (count($users) > 0) {
            foreach ($users as $val) {
                $pic[''] = '-- PIC --';
                $pic[$val->username] = $val->username;
            }
        } else {
            $pic[''] = '-- PIC --';
        }

        foreach ($asset_types as $key => $val) {
            $jenis_asset[''] = '-- Jenis Aset --';
            $jenis_asset[$key] = $val;
        }

        $view->jenis_asset = $jenis_asset;
        $view->pic = $pic;
        $view->nomor_kontrak = $nomor_kontrak;
        $view->lokasi = $lokasi;
        $this->layout->content = $view;
    }

    public function store_asetkontrak($lokasi) {
        $aset = new Asset;
        $aset->nomor_kontrak = Input::get('nomor_kontrak');
        $aset->kode_aset = Input::get('kode_aset');
        $aset->nama_asset_1 = Input::get('nama_asset');
        $aset->alamat = Input::get('alamat');
        $aset->jenis_asset = Input::get('jenis_asset');
        $aset->luas = Input::get('luas');
        $aset->pic = Input::get('pic');
        $aset->save();

        $kontrak = Kontrak::where('nomor_kontrak', $aset->nomor_kontrak)->first();

        return Redirect::to('kontrak/' . $lokasi . '/detail/' . $kontrak->id . '')->with('message', 'ASET BERHASIL DISIMPAN');
    }

    public function edit_asetkontrak($id, $lokasi) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.form_asetkontrak');
        $view->form_action = url('aset/update_asetkontrak/' . $id . '/' . $lokasi . '');
        $view->action_title = 'Ubah Aset';
        $view->form_title = 'add';

        $aset = Asset::find($id);
        $users = User::where('level_id', '3')->get();
        $asset_types = array('Tanah' => 'Tanah', 'Bangunan' => 'Bangunan', 'Ruko' => 'Ruko');

        $pic = array();
        $jenis_asset = array();

        if (count($users) > 0) {
            foreach ($users as $val) {
                $pic[''] = '-- PIC --';
                $pic[$val->username] = $val->username;
            }
        } else {
            $pic[''] = '-- PIC --';
        }

        foreach ($asset_types as $key => $val) {
            $jenis_asset[''] = '-- Jenis Aset --';
            $jenis_asset[$key] = $val;
        }

        $view->jenis_asset = $jenis_asset;
        $view->pic = $pic;
        $view->lokasi = $lokasi;
        $view->aset = $aset;
        $this->layout->content = $view;
    }

    public function update_asetkontrak($id, $lokasi) {
        $aset = Asset::find($id);
        $aset->nomor_kontrak = Input::get('nomor_kontrak');
        $aset->kode_aset = Input::get('kode_aset');
        $aset->nama_asset_1 = Input::get('nama_asset');
        $aset->alamat = Input::get('alamat');
        $aset->jenis_asset = Input::get('jenis_asset');
        $aset->luas = Input::get('luas');
        $aset->pic = Input::get('pic');
        $aset->save();

        $kontrak = Kontrak::where('nomor_kontrak', $aset->nomor_kontrak)->first();

        return Redirect::to('kontrak/' . $lokasi . '/detail/' . $kontrak->id . '')->with('message', 'ASET BERHASIL DIUBAH');
    }
    
    public function tanah($id) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.form_tanah');
        $view->form_action = url('aset/update_survey/' . $id . '');
        $view->action_title = 'Perbaharui Aset';
        $view->form_title = 'edit';

        $aset = Asset::find($id);
        DB::disableQueryLog();
        $provinsis = DB::table('lokasi')->groupby('provinsi')->get();
        $companies = Company::all();
        $businessareas = BusinessArea::all();
        //$lokasi  		= DB::table('lokasi')->get();
        $atls = array('1' => 'ATL 1',
            '2' => 'ATL 2',
            '3' => 'ATL 3',
            '4' => 'ATL 4',
            '5' => 'ATL 5',
            '6' => 'ATL 6',
            '7' => 'ATL 7',
            '8' => 'ATL 8',
            '9' => 'ATL 9',
            '10' => 'ATL 10'
        );

        $aset_type = array('Operational' => 'Operational', 'Non-Operational' => 'Non-Operational');

        $availables = array('Ada' => 'Ada', 'Tidak');

        $property_types = array('Tanah Milik' => 'Tanah Milik');

        $property_usages = array('Perumahan' => 'Perumahan',
            'Ritel' => 'Ritel',
            'Perkantoran' => 'Perkantoran',
            'Industri' => 'Industri',
            'Lainnya' => 'Lainnya'
        );

        $locations = array('Pinggir Jalan Provinsi' => 'Pinggir Jalan Provinsi',
            'Pinggir Jalan Dalam Kota' => 'Pinggir Jalan Dalam Kota',
            'Pinggir Jalan Penghubung Kota' => 'Pinggir Jalan Penghubung Kota',
            'Pinggir Jalan Desa' => 'Pinggir Jalan Desa',
            'Lainnya' => 'Lainnya'
        );

        $hard_types = array('Tanah' => 'Tanah',
            'Batu / Kerikil' => 'Batu / Kerikil',
            'Aspal' => 'Aspal',
            'Beton' => 'Beton'
        );

        $jalurs = array('1' => '1', '2' => '2');

        $lajurs = array('1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
        );

        $options = array('Ya' => 'Ya', 'Tidak' => 'Tidak');

        $traffics = array('Padat' => 'Padat', 'Ramai' => 'Ramai', 'Lancar' => 'Lancar');


        $document_statuses = array('SHM' => 'SHM',
            'HGB' => 'HGB',
            'HPL' => 'HPL',
            'HJB' => 'HJB',
            'Girik' => 'Girik',
            'HP' => 'HP',
            'Lainnya' => 'Lainnya'
        );

        $orientations = array('Barat' => 'Barat',
            'Timur' => 'Timur',
            'Utara' => 'Utara',
            'Selatan' => 'Selatan'
        );


        $positions = array('Tengah' => 'Tengah',
            'Pojok' => 'Pojok',
            'Tusuk Sate' => 'Tusuk Sate',
            'Buntu' => 'Buntu'
        );

        $topography = array('Datar' => 'Datar',
            'Bergelombang' => 'Bergelombang',
            'Bukit' => 'Bukit',
            'Curam' => 'Curam'
        );

        $treads = array('Persegi' => 'Persegi',
            'Bujur Sangkar' => 'Bujur Sangkar',
            'Trapesium' => 'Trapesium',
            'Tidak Beraturan' => 'Tidak Beraturan'
        );

        $tread_position = array('Lebih Tinggi' => 'Lebih Tinggi',
            'Sama Rata' => 'Sama Rata',
            'Lebih Rendah' => 'Lebih Rendah'
        );

        $hbu_build = array('Tidak Tersedia' => 'Tidak Tersedia',
            'Perusahaan' => 'Perusahaan',
            'Komersial' => 'Komersial',
            'Perkantoran' => 'Perkantoran',
            'Industri' => 'Industri',
            'Pertanian' => 'Pertanian',
            'Fasilitas Umum' => 'Fasilitas Umum',
            'Lainnya' => 'Lainnya'
        );

        $hbu_empty = array('Tidak Tersedia' => 'Tidak Tersedia',
            'Perusahaan' => 'Perusahaan',
            'Komersial' => 'Komersial',
            'Perkantoran' => 'Perkantoran',
            'Industri' => 'Industri',
            'Pertanian' => 'Pertanian',
            'Fasilitas Umum' => 'Fasilitas Umum',
            'Lainnya' => 'Lainnya'
        );



        foreach ($provinsis as $val) {
            $provinsi[''] = '-- Provinsi --';
            $provinsi[strtoupper($val->provinsi)] = strtoupper($val->provinsi);
        }

        foreach ($companies as $val) {
            $company[''] = '-- Company --';
            $company[$val->kode] = $val->nama;
        }

        /* 		
          foreach ($lokasi as $val) {
          $kabkota[''] = '-- Kota / Kabupaten --';
          if ($val->jenis == 'Kota') {
          $kabkota[$val->jenis."|".$val->kabupaten] = $val->kabupaten;
          } elseif ($val->jenis == 'Kab.') {
          $kabkota[$val->jenis."|".$val->kabupaten] = $val->jenis." ".$val->kabupaten;
          }
          } */

        $kabkota[''] = '-- Kota / Kabupaten --';
        $kecamatan[''] = '-- Kecamatan --';
        $desa[''] = '-- Kelurahan --';

        /* foreach ($lokasi as $val) {
          $kecamatan[''] = '-- Kecamatan --';
          $kecamatan[$val->kecamatan] = $val->kecamatan;
          }

          foreach ($lokasi as $val) {
          $desa[''] = '-- Kelurahan --';
          $desa[$val->kelurahan] = $val->kelurahan;
          } */

        foreach ($businessareas as $val) {
            $kode_bisnis[''] = '-- Business Area --';
            $kode_bisnis[$val->kode] = $val->nama;
        }

        foreach ($aset_type as $key => $val) {
            $jenis_asset[''] = '-- Jenis Aset --';
            $jenis_asset[$key] = $val;
        }

        foreach ($atls as $key => $val) {
            $atl[''] = '-- ATL --';
            $atl[$key] = $val;
        }

        foreach ($availables as $key => $val) {
            $listrik[''] = '-- Ketersediaan Listrik --';
            $listrik[$key] = $val;
        }

        foreach ($availables as $key => $val) {
            $telepon[''] = '-- Ketersediaan Telepon --';
            $telepon[$key] = $val;
        }

        foreach ($availables as $key => $val) {
            $air[''] = '-- Ketersediaan Air --';
            $air[$key] = $val;
        }

        foreach ($property_types as $val) {
            $jenis_properti[$key] = $val;
        }

        foreach ($property_usages as $key => $val) {
            $penggunaan_properti[''] = '-- Penggunaan Properti --';
            $penggunaan_properti[$key] = $val;
        }

        foreach ($locations as $key => $val) {
            $lokasi_aset[''] = '-- Lokasi --';
            $lokasi_aset[$key] = $val;
        }

        foreach ($hard_types as $key => $val) {
            $jenis_perkerasan[''] = '-- Jenis Perkerasan --';
            $jenis_perkerasan[$key] = $val;
        }

        foreach ($jalurs as $key => $val) {
            $jalur[''] = '-- Jalur --';
            $jalur[$key] = $val;
        }

        foreach ($lajurs as $key => $val) {
            $lajur[''] = '-- Lajur --';
            $lajur[$key] = $val;
        }

        foreach ($options as $key => $val) {
            $keberadaan_median[''] = '-- Keberadaan Median --';
            $keberadaan_median[$key] = $val;
        }

        foreach ($options as $key => $val) {
            $dilalui_angkutan_umum[''] = '-- Dilalui Angkutan Umum --';
            $dilalui_angkutan_umum[$key] = $val;
        }

        foreach ($traffics as $key => $val) {
            $kepadatan_lalu_lintas[''] = '-- Kepadatan Lalu Lintas --';
            $kepadatan_lalu_lintas[$key] = $val;
        }

        foreach ($document_statuses as $key => $val) {
            $status_dokumen_tanah[''] = '-- Status Dokumen Tanah --';
            $status_dokumen_tanah[$key] = $val;
        }

        foreach ($orientations as $key => $val) {
            $orientasi[''] = '-- Orientasi --';
            $orientasi[$key] = $val;
        }

        foreach ($positions as $key => $val) {
            $posisi[''] = '-- Posisi Tanah --';
            $posisi[$key] = $val;
        }

        foreach ($topography as $key => $val) {
            $topografi[''] = '-- Topografi --';
            $topografi[$key] = $val;
        }

        foreach ($treads as $key => $val) {
            $bentuk_tapak[''] = '-- Bentuk Tapak --';
            $bentuk_tapak[$key] = $val;
        }

        foreach ($tread_position as $key => $val) {
            $kedudukan_tapak[''] = '-- Kedudukan Tapak --';
            $kedudukan_tapak[$key] = $val;
        }

        foreach ($hbu_build as $key => $val) {
            $hbu_terbangun[''] = '-- HBU Terbangun --';
            $hbu_terbangun[$key] = $val;
        }

        foreach ($hbu_empty as $key => $val) {
            $hbu_kosong[''] = '-- HBU Kosong --';
            $hbu_kosong[$key] = $val;
        }

        $view->provinsi = $provinsi;
        $view->kecamatan = $kecamatan;
        $view->desa = $desa;
        $view->kabkota = $kabkota;
        $view->kodebisnis = $kode_bisnis;
        $view->jenis_asset = $jenis_asset;
        $view->company = $company;
        $view->atl = $atl;
        $view->air = $air;
        $view->listrik = $listrik;
        $view->telepon = $telepon;
        $view->telepon = $telepon;
        $view->jenis_properti = $jenis_properti;
        $view->penggunaan_properti = $penggunaan_properti;
        $view->lokasi_aset = $lokasi_aset;
        $view->jenis_perkerasan = $jenis_perkerasan;
        $view->jumlah_jalur = $jalur;
        $view->jumlah_lajur = $lajur;
        $view->keberadaan_median = $keberadaan_median;
        $view->dilalui_angkutan_umum = $dilalui_angkutan_umum;
        $view->kepadatan_lalu_lintas = $kepadatan_lalu_lintas;
        $view->status_dokumen_tanah = $status_dokumen_tanah;
        $view->orientasi = $orientasi;
        $view->posisi_tanah = $posisi;
        $view->topografi = $topografi;
        $view->bentuk_tapak = $bentuk_tapak;
        $view->kedudukan_tapak = $kedudukan_tapak;
        $view->hbu_terbangun = $hbu_terbangun;
        $view->hbu_kosong = $hbu_kosong;
        $view->aset = $aset;
        $this->layout->content = $view;
    }

    public function bangunan($id) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.form_bangunan');
        $view->form_action = url('aset/update_bangunan/' . $id . '');
        $view->action_title = 'Perbaharui Aset';
        $view->form_title = 'edit';

        $aset = Asset::find($id);
        DB::disableQueryLog();
        $provinsis = DB::table('lokasi')->groupby('provinsi')->get();
        $businessareas = BusinessArea::all();

        $atls = array('1' => 'ATL 1',
            '2' => 'ATL 2',
            '3' => 'ATL 3',
            '4' => 'ATL 4',
            '5' => 'ATL 5',
            '6' => 'ATL 6',
            '7' => 'ATL 7',
            '8' => 'ATL 8',
            '9' => 'ATL 9',
            '10' => 'ATL 10'
        );

        $floor_numbers = array('1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10');

        $type_bangunan = array('Rumah Mewah' => 'Rumah Mewah',
            'Rumah Menengah' => 'Rumah Menengah',
            'Rumah Bawah' => 'Rumah Bawah',
            'Bangunan Bertingkat (kantor)' => 'Bangunan Bertingkat (kantor)',
            'Gudang' => 'Gudang',
            'Pabrik' => 'Pabrik'
        );

        $aset_type = array('Operational' => 'Operational', 'Non-Operational' => 'Non-Operational');


        $foundation = array('Lajur Batu Kali' => 'Lajur Batu Kali',
            'Beton Plat Setempat (telapak)' => 'Beton Plat Setempat (telapak)',
            'Tiang Pancang Kayu' => 'Tiang Pancang Kayu',
            'Tiang Pancang Beton' => 'Tiang Pancang Beton',
            'Lainnya' => 'Lainnya'
        );

        $structure = array('Struktur Beton Cor 1 lantai' => 'Struktur Beton Cor 1 lantai',
            'Struktur Beton Cor 2 lantai' => 'Struktur Beton Cor 2 lantai',
            'Struktur Baja' => 'Struktur Baja',
            'Struktur Beton' => 'Struktur Beton',
            'Komposit' => 'Komposit',
            'Lainnya' => 'Lainnya'
        );

        $walls = array(
            'Batu bata di plester dan di cat' => 'Batu bata di plester dan di cat',
            'Bata hebel di plester dan di cat' => 'Bata hebel di plester dan di cat',
            'Batako di plester dan di cat' => 'Batako di plester dan di cat',
            'Gipsum rangka kayu dicat' => 'Gipsum rangka kayu dicat',
            'Batu bata di plester' => 'Batu bata di plester',
            'Bata hebel di plester' => 'Bata hebel di plester',
            'Batako di plester' => 'Batako di plester',
            'Kawat besi' => 'Kawat besi',
            'Metal Zinkalum' => 'Metal Zinkalum',
            'Double metal zinkalum' => 'Double metal zinkalum',
            'Kaca rangka Alumunium' => 'Kaca rangka Alumunium',
            'Kaca Rangka Kayu' => 'Kaca Rangka Kayu',
            'Dinding Geser' => 'Dinding Geser',
            'Lainnya' => 'Lainnya'
        );

        $doors = array("Panel Kaca Rayban rangka kayu" => 'Panel Kaca Rayban rangka kayu',
            "Panel Kaca rangka kayu" => 'Panel Kaca rangka kayu',
            "Panel kaca ice rangka kayu" => 'Panel kaca ice rangka kayu',
            "Pintu dan jendela dari kayu kelas 1" => 'Pintu dan jendela dari kayu kelas 1',
            "Pintu dan jendela dari kayu kelas 2" => 'Pintu dan jendela dari kayu kelas 2',
            "Pintu dan jendela dari kayu kelas 3" => 'Pintu dan jendela dari kayu kelas 3',
            "Pintu dan jendela dari triplek" => 'Pintu dan jendela dari triplek',
            "Kusen pintu dan jendela Aluminium " => 'Kusen pintu dan jendela Aluminium ',
            "Pintu dan jendela Besi " => 'Pintu dan jendela Besi',
            "Pintu dan Jendela Kaca Gravir" => 'Pintu dan Jendela Kaca Gravir',
            'Lainnya' => 'Lainnya'
        );

        $floors = array(
            "Beton Bertulang" => "Beton Bertulang",
            "Beton Bertulang+epoxi" => "Beton Bertulang+epoxi",
            "Keramik 20x20" => "Keramik 20x20",
            "Keramik 30x30" => "Keramik 30x30",
            "Keramik 40x40" => "Keramik 40x40",
            "Granit" => "Granit",
            "Marmer" => "Marmer",
            "Parquete" => "Parquete",
            "Teraso" => "Teraso",
            "Semen/Plur" => "Semen/Plur",
            "Semen + pvc vinyl" => "Semen + pvc vinyl",
            "Semen/Plur+epoxi" => "Semen/Plur+epoxi",
            "Homogenus Tile" => "Homogenus Tile",
            "Papan Kayu" => "Papan Kayu",
            "Lainnya" => "Lainnya"
        );

        $skies = array(
            "Gipsum rangka hollow di cat" => "Gipsum rangka hollow di cat",
            "Triplek Rangka Kayu di cat" => "Triplek Rangka Kayu di cat",
            "Gipsum rangka kayu di cat" => "Gipsum rangka kayu di cat",
            "Eternit" => "Eternit",
            "Metal alumunium" => "Metal alumunium",
            "Double metal zinkalum" => "Double metal zinkalum",
            "Glasswool dilapis alumniumfoil" => "Glasswool dilapis alumniumfoil",
            "Lainnya" => "Lainnya"
        );

        $roof_truss = array(
            "Kayu Kelas I" => 'Kayu Kelas I',
            "Kayu Kelas II" => 'Kayu Kelas II',
            "Baja Ringan" => 'Baja Ringan',
            "Baja" => 'Baja',
            "Lainnya" => 'Lainnya'
        );

        $roof_cover = array(
            "Dak Beton" => "Dak Beton",
            "Fibreglass" => "Fibreglass",
            "Genteng Keramik" => "Genteng Keramik",
            "Genteng Kodok" => "Genteng Kodok",
            "GentengMetal" => "GentengMetal",
            "Genteng Beton" => "Genteng Beton",
            "Genteng Tegola" => "Genteng Tegola",
            "Fiber Cement Slab Harflex" => "Fiber Cement Slab Harflex",
            "Spandek" => "Spandek",
            "Sirap" => "Sirap",
            "Asbes" => "Asbes",
            "Lainnya" => 'Lainnya'
        );

        $electricity = array('PLN' => 'PLN',
            'Genset' => 'Genset',
            'Lainnya' => 'Lainnya'
        );

        $water = array("PDAM" => "PDAM",
            "Sumur Dalam" => "Sumur Dalam",
            "Sumur Pompa" => "Sumur Pompa",
            "Tangki Air" => "Tangki Air",
            "Tangki bawah tanah",
            "Lainnya");

        $waste = array('Septic Tank' => 'Septic Tank', 'Saluran Kota' => 'Saluran Kota', 'STP' => 'STP', 'Lainnya' => 'Lainnya');

        $air_conditioner = array('Sentral (PK/HP)' => 'Sentral (PK/HP)',
            'Paket per lantai (PK/HP)' => 'Paket per lantai (PK/HP)',
            'Split (Unit)' => 'Split (Unit)',
            'Chiller, AHU (Unit)' => 'Chiller, AHU (Unit)',
            'Packet (PK/HP)' => 'Packet (PK/HP)',
            'Lainnya (PK)' => 'Lainnya (PK)'
        );

        $firefighters = array(
            'Sprinkler' => 'Sprinkler',
            'Hydrant' => 'Hydrant',
            'Portable Extinguisher' => 'Portable Extinguisher',
            'Pendeteksi Asap' => 'Pendeteksi Asap',
            'Pendeteksi Panas' => 'Pendeteksi Panas',
            'Sirens' => 'Sirens',
            'Lainnya' => 'Lainnya'
        );

        $hard_types = array('Tanah' => 'Tanah',
            'Batu / Kerikil' => 'Batu / Kerikil',
            'Aspal' => 'Aspal',
            'Beton' => 'Beton'
        );

        $cctvs = array('1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
        );

        foreach ($provinsis as $val) {
            $provinsi[''] = '-- Provinsi --';
            $provinsi[strtoupper($val->provinsi)] = strtoupper($val->provinsi);
        }

        foreach ($type_bangunan as $key => $val) {
            $jenis_bangunan[''] = '-- Jenis Bangunan --';
            $jenis_bangunan[$key] = $val;
        }

        /* 		
          foreach ($lokasi as $val) {
          $kabkota[''] = '-- Kota / Kabupaten --';
          if ($val->jenis == 'Kota') {
          $kabkota[$val->jenis."|".$val->kabupaten] = $val->kabupaten;
          } elseif ($val->jenis == 'Kab.') {
          $kabkota[$val->jenis."|".$val->kabupaten] = $val->jenis." ".$val->kabupaten;
          }
          }          */

        $kabkota[''] = '-- Kota / Kabupaten --';
        $kecamatan[''] = '-- Kecamatan --';
        $desa[''] = '-- Kelurahan --';

        /* foreach ($lokasi as $val) {
          $kecamatan[''] = '-- Kecamatan --';
          $kecamatan[$val->kecamatan] = $val->kecamatan;
          }

          foreach ($lokasi as $val) {
          $desa[''] = '-- Kelurahan --';
          $desa[$val->kelurahan] = $val->kelurahan;
          } */

        foreach ($businessareas as $val) {
            $kode_bisnis[''] = '-- Kode Bisnis --';
            $kode_bisnis[$val->kode] = $val->nama;
        }

        foreach ($aset_type as $key => $val) {
            $jenis_asset[''] = '-- Tipe Aset --';
            $jenis_asset[$key] = $val;
        }

        foreach ($floor_numbers as $key => $val) {
            $jumlah_lantai[''] = '-- Jumlah Lantai --';
            $jumlah_lantai[$key] = $val;
        }

        foreach ($foundation as $key => $val) {
            $pondasi[''] = '-- Pondasi --';
            $pondasi[$key] = $val;
        }

        foreach ($structure as $key => $val) {
            $struktur[''] = '-- Struktur --';
            $struktur[$key] = $val;
        }

        foreach ($walls as $key => $val) {
            $dinding[''] = '-- Dinding --';
            $dinding[$key] = $val;
        }

        foreach ($floors as $key => $val) {
            $lantai[''] = '-- Lantai --';
            $lantai[$key] = $val;
        }

        foreach ($doors as $key => $val) {
            $pintu[''] = '-- Pintu & Jendela --';
            $pintu[$key] = $val;
        }

        foreach ($skies as $key => $val) {
            $langit[''] = '-- Langit-langit --';
            $langit[$key] = $val;
        }

        foreach ($roof_truss as $key => $val) {
            $rangka_atap[''] = '-- Rangkat Atap --';
            $rangka_atap[$key] = $val;
        }

        foreach ($skies as $key => $val) {
            $tutup_atap[''] = '-- Penutup Atap --';
            $tutup_atap[$key] = $val;
        }

        foreach ($air_conditioner as $key => $val) {
            $ac[''] = '-- Air Conditioner --';
            $ac[$key] = $val;
        }

        foreach ($waste as $key => $val) {
            $limbah[''] = '-- Limbah --';
            $limbah[$key] = $val;
        }

        foreach ($firefighters as $key => $val) {
            $pemadam_kebakaran[''] = '-- Pemadam Kebakaran --';
            $pemadam_kebakaran[$key] = $val;
        }

        foreach ($atls as $key => $val) {
            $atl[''] = '-- ATL --';
            $atl[$key] = $val;
        }

        foreach ($electricity as $key => $val) {
            $listrik[''] = '-- Listrik --';
            $listrik[$key] = $val;
        }

        foreach ($water as $key => $val) {
            $air[''] = '-- Air --';
            $air[$key] = $val;
        }

        foreach ($cctvs as $key => $val) {
            $cctv[''] = '-- CCTV --';
            $cctv[$key] = $val;
        }

        $view->provinsi = $provinsi;
        $view->kecamatan = $kecamatan;
        $view->desa = $desa;
        $view->kabkota = $kabkota;
        $view->kodebisnis = $kode_bisnis;
        $view->jenis_asset = $jenis_asset;
        $view->atl = $atl;
        $view->air = $air;
        $view->listrik = $listrik;
        $view->ac = $ac;
        $view->limbah = $limbah;
        $view->pemadam_kebakaran = $pemadam_kebakaran;
        $view->cctv = $cctv;
        $view->jenis_bangunan = $jenis_bangunan;
        $view->jumlah_lantai = $jumlah_lantai;
        $view->pondasi = $pondasi;
        $view->struktur = $struktur;
        $view->lantai = $lantai;
        $view->dinding = $dinding;
        $view->pintu = $pintu;
        $view->langit = $langit;
        $view->rangka_atap = $rangka_atap;
        $view->tutup_atap = $tutup_atap;
        $view->aset = $aset;
        $this->layout->content = $view;
    }
    
    public function update_bangunan($id) {
        $aset = Asset::find($id);
        $aset->tanggal_penilaian = Input::get('tanggal_penilaian');
        $aset->kode_bisnis = Input::get('kode_bisnis');
        $aset->kode_aset = Input::get('kode_aset');
        $aset->keterangan_lokasi = Input::get('keterangan_lokasi');
        $aset->atl = Input::get('atl');
        $aset->tipe_aset = Input::get('tipe_aset');
        $aset->nama_asset_1 = Input::get('nama_asset_1');
        $aset->luas = Input::get('luas');
        $aset->tahun_selesai_dibangun = Input::get('tahun_selesai_dibangun');
        $aset->tahun_renovasi = Input::get('tahun_renovasi');
        $aset->jumlah_lantai = Input::get('jumlah_lantai');
        $aset->jenis_bangunan = Input::get('jenis_bangunan');
        $aset->alamat = Input::get('alamat');
        $aset->provinsi = strtoupper(Input::get('provinsi'));
        $aset->kota = Input::get('kabkota');
        $aset->kecamatan = Input::get('kecamatan');
        $aset->desa = Input::get('desa');
        $aset->pondasi = Input::get('pondasi');
        $aset->struktur = Input::get('struktur');
        $aset->lantai = Input::get('lantai');
        $aset->dinding = Input::get('dinding');
        $aset->pintu = Input::get('pintu');
        $aset->langit = Input::get('langit');
        $aset->rangka_atap = Input::get('rangka_atap');
        $aset->tutup_atap = Input::get('tutup_atap');
        $aset->pondasi_kondisi = Input::get('pondasi_kondisi');
        $aset->struktur_kondisi = Input::get('struktur_kondisi');
        $aset->lantai_kondisi = Input::get('lantai_kondisi');
        $aset->dinding_kondisi = Input::get('dinding_kondisi');
        $aset->pintu_kondisi = Input::get('pintu_kondisi');
        $aset->langit_kondisi = Input::get('langit_kondisi');
        $aset->rangka_atap_kondisi = Input::get('rangka_atap_kondisi');
        $aset->tutup_atap_kondisi = Input::get('tutup_atap_kondisi');
        $aset->pondasi_nilai = Input::get('pondasi_nilai');
        $aset->struktur_nilai = Input::get('struktur_nilai');
        $aset->lantai_nilai = Input::get('lantai_nilai');
        $aset->dinding_nilai = Input::get('dinding_nilai');
        $aset->pintu_nilai = Input::get('pintu_nilai');
        $aset->langit_nilai = Input::get('langit_nilai');
        $aset->rangka_atap_nilai = Input::get('rangka_atap_nilai');
        $aset->tutup_atap_nilai = Input::get('tutup_atap_kondisi');
        $aset->kondisi = Input::get('kondisi');
        $aset->utilitas = Input::get('utilitas');
        $aset->def_fisik = Input::get('def_fisik');
        $aset->def_fungsi = Input::get('def_fungsi');
        $aset->def_external = Input::get('def_external');
        $aset->indikasi_nilai_pasar = Input::get('indikasi_nilai_pasar');
        $aset->sisa_umur_ekonomi = Input::get('sisa_umur_ekonomi');
        $aset->listrik = Input::get('listrik');
        $aset->listrik_lainnya = Input::get('listrik_lainnya');
        $aset->kapasitas_listrik = Input::get('kapasitas_listrik');
        $aset->total_saluran_telepon = Input::get('total_saluran_telepon');
        $aset->saluran_fax = Input::get('saluran_fax');
        $aset->pbax = Input::get('pbax');
        $aset->air = Input::get('air');
        $aset->air_lainnya = Input::get('air_lainnya');
        $aset->ac = Input::get('ac');
        $aset->ac_lainnya = Input::get('ac_lainnya');
        $aset->limbah = Input::get('limbah');
        $aset->limbah_lainnya = Input::get('limbah_lainnya');
        $aset->pemadam_kebakaran = Input::get('pemadam_kebakaran');
        $aset->pemadam_kebakaran_lainnya = Input::get('pemadam_kebakaran_lainnya');
        $aset->lift_penumpang_merk = Input::get('lift_penumpang_merk');
        $aset->lift_penumpang_kap_org = Input::get('lift_penumpang_kap_org');
        $aset->lift_penumpang_kap_kg = Input::get('lift_penumpang_kap_kg');
        $aset->lift_penumpang_unit = Input::get('lift_penumpang_unit');
        $aset->lift_barang_merk = Input::get('lift_barang_merk');
        $aset->lift_barang_kap_org = Input::get('lift_barang_kap_org');
        $aset->lift_barang_kap_kg = Input::get('lift_barang_kap_kg');
        $aset->lift_barang_unit = Input::get('lift_barang_unit');
        $aset->bas_type = Input::get('bas_type');
        $aset->bas_merk = Input::get('bas_merk');
        $aset->cctv = Input::get('cctv');
        $aset->lainnya = Input::get('lainnya');
        $aset->koordinat_latitude = Input::get('latitude');
        $aset->koordinat_longitude = Input::get('longitude');
        $aset->save();
        return Redirect::to('aset/detail/' . $aset->id . '')->with('message', 'ASET BERHASIL DISIMPAN');
    }
    
    public function update_tanah($id) {
        $aset = Asset::find($id);
        $aset->kode_aset = Input::get('kode_aset');
        $aset->nama_asset_1 = Input::get('nama_asset_1');
        $aset->nama_asset_2 = Input::get('nama_asset_2');
        $aset->atl = Input::get('atl');
        $aset->kode_company = Input::get('company');
        $aset->keterangan_lokasi = Input::get('keterangan_lokasi');
        $aset->kode_bisnis = Input::get('kode_bisnis');
        $aset->kode_aset = Input::get('kode_aset');
        $aset->kontak_nama = Input::get('kontak_nama');
        $aset->kontak_telepon = Input::get('kontak_telepon');
        $aset->tanggal_inspeksi = Input::get('tanggal_inspeksi');
        $aset->tanggal_penilaian = Input::get('tanggal_penilaian');
        $aset->jenis_asset = Input::get('jenis_asset');
        $aset->jenis_properti = Input::get('jenis_properti');
        $aset->alamat = Input::get('alamat');
        $aset->provinsi = Input::get('provinsi');
        $aset->kota = Input::get('kota');
        $aset->kecamatan = Input::get('kecamatan');
        $aset->desa = Input::get('desa');
        $aset->penggunaan_properti = Input::get('penggunaan_properti');
        $aset->lokasi_aset = Input::get('lokasi_aset');
        /*$aset->nop = Input::get('nop');
        $aset->njop = Input::get('njop');*/
        $aset->lebar_jalan = Input::get('lebar_jalan');
        $aset->jenis_perkerasan = Input::get('jenis_perkerasan');
        $aset->jumlah_jalur = Input::get('jumlah_jalur');
        $aset->jumlah_lajur = Input::get('jumlah_lajur');
        $aset->keberadaan_median = Input::get('keberadaan_median');
        $aset->kepadatan_lalu_lintas = Input::get('jumlah_lajur');
        $aset->dilalui_angkutan_umum = Input::get('dilalui_angkutan_umum');
        $aset->status_dokumen_tanah = Input::get('status_dokumen_tanah');
        $aset->no_sertifikat = Input::get('no_sertifikat');
        $aset->luas = Input::get('luas');
        $aset->nama_pemegang = Input::get('nama_pemegang');
        $aset->tanggal_dikeluarkan = Input::get('tanggal_dikeluarkan');
        $aset->tanggal_berakhir = Input::get('tanggal_berakhir');
        $aset->nomor_gambar_situasi = Input::get('nomor_gambar_situasi');
        $aset->tanggal_gambar_situasi = Input::get('tanggal_gambar_situasi');
        $aset->periode_sertifikat = Input::get('periode_sertifikat');
        $aset->orientasi = Input::get('orientasi');
        $aset->lebar = Input::get('lebar');
        $aset->panjang_belakang = Input::get('panjang_belakang');
        $aset->posisi_tanah = Input::get('posisi_tanah');
        $aset->listrik = Input::get('listrik');
        $aset->air = Input::get('air');
        $aset->telepon = Input::get('telepon');
        $aset->lainnya = Input::get('lainnya');
        $aset->topografi = Input::get('topografi');
        $aset->bentuk_tapak = Input::get('bentuk_tapak');
        $aset->kedudukan_tapak = Input::get('kedudukan_tapak');
        $aset->batas_utara = Input::get('batas_utara');
        $aset->batas_barat = Input::get('batas_barat');
        $aset->batas_selatan = Input::get('batas_selatan');
        $aset->batas_timur = Input::get('batas_timur');
        $aset->koordinat_latitude = Input::get('latitude');
        $aset->koordinat_longitude = Input::get('longitude');
        /*$aset->hbu_terbangun = Input::get('hbu_terbangun');
        $aset->hbu_kosong = Input::get('hbu_kosong');
        $aset->total_absolut = Input::get('total_absolut');
        $aset->inverse = Input::get('inverse');
        $aset->weighted = Input::get('weighted');
        $aset->price_maximum = Input::get('price_maximum');
        $aset->price_minimum = Input::get('price_minimum');
        $aset->validation = Input::get('hbu_kosong');
        $aset->imv_psm = Input::get('imv_psm');
        $aset->imv = Input::get('imv');
        $aset->imv_bulat = Input::get('imv_bulat');*/
        $aset->save();
        return Redirect::to('aset/detail/' . $id . '')->with('message', 'DATA ASET BERHASIL DIPERBAHARUI');
    }

    public function detail($id) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.detail');
        $view->action_title = 'Detail Aset';

        $aset = Asset::find($id);

        $pembanding  = Pembanding::whereIn('id', array($aset->pembanding_1, $aset->pembanding_2, $aset->pembanding_3))->get();
        $pembanding1 = Pembanding::where(array('id' => $aset->pembanding_1))->first();
        $pembanding2 = Pembanding::where(array('id' => $aset->pembanding_2))->first();
        $pembanding3 = Pembanding::where(array('id' => $aset->pembanding_3))->first();

        $view->aset = $aset;
        $view->pembanding = $pembanding;
        $view->pembanding1 = $pembanding1;
        $view->pembanding2 = $pembanding2;
        $view->pembanding3 = $pembanding3;
        $this->layout->content = $view;
    }

    public function destroy($id) {
        $aset = Asset::find($id);
        $aset->delete();
        return Redirect::to('aset')->with('message', 'ASET BERHASIL DIHAPUS');
    }

    public function cari_pembanding($id, $tipe_data) {
        $this->layout->title = $this->title;
        $this->layout->url = 'aset';
        $view = View::make('aset.form_pembanding');
        $view->form_action = url('aset/update_pembanding/' . $id . '');
        $view->action_title = 'Cari Data Pembanding';
        $view->form_title = 'edit';

        $pembandings = Pembanding::all();
        $aset = Asset::find($id);

        foreach ($pembandings as $val) {
            $pembanding[''] = '-- Data Pembanding --';
            $pembanding[$val->id] = $id . " - " . $val->address;
        }

        $view->pembanding = $pembanding;
        $view->tipe_data = $tipe_data;
        $view->aset = $aset;

        $this->layout->content = $view;
        Session::flash('aset_id', $id);
    }

    public function update_pembanding($id) {
        $aset = Asset::find($id);
        if (Input::get('tipe_data') == 'pembanding_1') {
            $aset->pembanding_1 = Input::get('id_pembanding');
        } elseif (Input::get('tipe_data') == 'pembanding_2') {
            $aset->pembanding_2 = Input::get('id_pembanding');
        } elseif (Input::get('tipe_data') == 'pembanding_3') {
            $aset->pembanding_3 = Input::get('id_pembanding');
        }
        $aset->save();
        return Redirect::to('aset/detail/' . $id . '')->with('message', 'DATA PEMBANDING BERHASIL DIPERBAHARUI');
    }
}
