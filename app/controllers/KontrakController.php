<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

use Illuminate\Database\Eloquent\ModelNotFoundException;

class KontrakController extends BaseController {

	public $layout 	= 'layouts.default';
	public $title  	= 'DATA KONTRAK';
	public $route	= 'kontrak';
	public $write;
	public $update;
	public $delete;
	public $session_user;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$user  = Session::get('user');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;
					}
				}
			}
			$this->session_user = $user->username;
			if ($i == 0)
				return Redirect::to('dashboard')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('login');
		}
	}
	
	public function jakarta() {
		$this->layout->title = $this->title. ' - JAKARTA';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.jakarta');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function surabaya() {
		$this->layout->title = $this->title. ' - SURABAYA';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.surabaya');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function medan() {
		$this->layout->title = $this->title. ' - MEDAN';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.medan');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function bali() {
		$this->layout->title = $this->title. ' - BALI';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.bali');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function makassar() {
		$this->layout->title = $this->title. ' - MAKASSAR';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.makassar');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function solo() {
		$this->layout->title = $this->title. ' - SOLO';
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.solo');

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->session_user = $this->session_user;
		$this->layout->content = $view;
	}

	public function create_jakarta() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_jakarta');
		$view->form_action = url('kontrak/jakarta/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function create_surabaya() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_surabaya');
		$view->form_action = url('kontrak/surabaya/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function create_medan() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_medan');
		$view->form_action = url('kontrak/medan/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function create_bali() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_bali');
		$view->form_action = url('kontrak/bali/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function create_makassar() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_makassar');
		$view->form_action = url('kontrak/makassar/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function create_solo() {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_solo');
		$view->form_action = url('kontrak/solo/store');
		$view->action_title = 'Tambah Data kontrak';
		$view->form_title = 'add';

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;

		$this->layout->content = $view;
	}

	public function store_jakarta() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/jakarta')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function store_surabaya() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/surabaya')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function store_medan() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/medan')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function store_bali() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/bali')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function store_makassar() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/makassar')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function store_solo() {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = new Kontrak;
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/solo')->with('message', 'DATA KONTRAK BERHASIL DISIMPAN');
	}

	public function detail_jakarta($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_jakarta');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function detail_surabaya($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_surabaya');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function detail_medan($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_medan');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function detail_bali($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_bali');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function detail_makassar($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_makassar');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function detail_solo($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.detail_solo');
		$view->action_title = 'Detail Kontrak';
		
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$view->session_user = $this->session_user;

		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','3')->get();
		$asset_types = array('Tanah'=>'Tanah','Tanah dan Bangunan', 'Ruko');
		$units = array('m2'=>'m2','Kva', 'Kva');

		$pic = array();
		$satuan = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$pic[''] = '-- PIC --';
				$pic[$val->username] = $val->username;
			}
		} else {
			$pic[''] = '-- PIC --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		foreach ($units as $key=>$val) {
			$satuan[''] = '-- Satuan --';
			$satuan[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->pic  = $pic;
		$view->satuan  = $satuan;

		$view->kontrak = $kontrak;
		$this->layout->content = $view;
	}

	public function edit_jakarta($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_jakarta');
		$view->form_action = url('kontrak/jakarta/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');


		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function edit_surabaya($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_surabaya');
		$view->form_action = url('kontrak/surabaya/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function edit_medan($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_medan');
		$view->form_action = url('kontrak/medan/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function edit_bali($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_bali');
		$view->form_action = url('kontrak/bali/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function edit_makassar($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_makassar');
		$view->form_action = url('kontrak/makassar/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function edit_solo($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'kontrak';	
		$view = View::make('kontrak.form_solo');
		$view->form_action = url('kontrak/solo/update/'.$id.'');
		$view->action_title = 'Ubah Kontrak';
		$view->form_title = 'edit';
		
		$kontrak = Kontrak::find($id);

		$users = User::where('level_id','1')->get();
		$asset_types = array('Tanah' => 'Tanah',
        					 'Tanah dan Bangunan' => 'Tanah dan Bangunan',
        					 'Kios' => 'Kios',
        					 'Ruang Kantor' => 'Ruang Kantor',
        					 'Apartemen' => 'Apartemen',
        					 'Ruko' => 'Ruko');

		$user = array();
		$jenis_asset = array();

		if (count($users) > 0) {
			foreach ($users as $val) {
				$user[''] = '-- Pimpinan Proyek --';
				$user[$val->username] = $val->username;
			}
		} else {
			$user[''] = '-- Pimpinan Proyek --';
		}

		foreach ($asset_types as $key=>$val) {
			$jenis_asset[''] = '-- Jenis Aset --';
			$jenis_asset[$key] = $val;
		}

		$view->jenis_asset  = $jenis_asset;
		$view->user  = $user;
		$view->kontrak = $kontrak;
		$this->layout->content = $view;
		Session::flash('kontrak_id', $id);
	}

	public function update_jakarta($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/jakarta')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function update_surabaya($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/surabaya')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function update_medan($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/medan')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function update_bali($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/bali')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function update_makassar($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/makassar')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function update_solo($id) {
		$destinationPath = url('file_kontrak');
		if (Input::hasFile('file_kontrak')) {
			Input::file('file_kontrak')->move('file_kontrak', Input::file('file_kontrak')->getClientOriginalName());   
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->file_kontrak		= Input::file('file_kontrak')->getClientOriginalName();
			$kontrak->save();
		} else {
			$kontrak = Kontrak::find($id);
			$kontrak->nomor_kontrak		= Input::get('nomor_kontrak');
			$kontrak->tanggal_kontrak	= Input::get('tanggal_kontrak');
			$kontrak->jenis_asset		= Input::get('jenis_asset');
			$kontrak->alamat			= Input::get('alamat');
			$kontrak->tujuan_penilaian	= Input::get('tujuan_penilaian');
			$kontrak->fee				= Input::get('fee');
			$kontrak->pimpinan_proyek	= Input::get('pimpinan_proyek');
			$kontrak->lokasi 			= Input::get('lokasi');
			$kontrak->save();
		}
		return Redirect::to('kontrak/solo')->with('message', 'DATA KONTRAK BERHASIL DIUBAH');
	}

	public function destroy_jakarta($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/jakarta')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}

	public function destroy_surabaya($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/surabaya')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}

	public function destroy_medan($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/medan')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}

	public function destroy_bali($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/bali')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}

	public function destroy_makassar($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/makassar')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}

	public function destroy_solo($id) {
		$kontrak = Kontrak::find($id);
		$file_path = url('file_kontrak/'.$kontrak->file_kontrak.'');
		File::delete($file_path);
		$kontrak->delete();
		return Redirect::to('kontrak/solo')->with('message', 'DATA KONTRAK BERHASIL DIHAPUS');
	}
}