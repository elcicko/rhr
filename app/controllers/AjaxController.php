<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

class AjaxController extends \BaseController 
{

    public function getkabkota($id) {
        $id = urldecode(html_entity_decode($id));
        $kabkotas = DB::table('lokasi')->select('jenis', 'kabupaten')->groupBy('kabupaten')->groupBy('jenis')->where('provinsi', $id)->get();
        $data = "<option selected value=''>-- Pilih Kota / Kabupaten --</option>";
        $kota = '';
        $kab = '';
        $kota_value = '';
        $kab_value = '';
        
        foreach ($kabkotas as $obj) {
            if ($obj->jenis == 'Kota') {
                $kota = 'KOTA '. strtoupper($obj->kabupaten);
                $kota_value = 'KOTA|'. strtoupper($obj->kabupaten);
                $data .= "<option value='".$kota_value."'>".$kota."</option>";
            } elseif ($obj->jenis == 'Kab.') {
                $kab = 'KABUPATEN '. strtoupper($obj->kabupaten);
                $kab_value = 'KABUPATEN|'. strtoupper($obj->kabupaten);
                $data .= "<option value='".$kab_value."'>".$kab."</option>";
            }
        }
        return $data;
    }

    public function getkecamatanbykabkota($kabkota) {
        $kabkota = urldecode(html_entity_decode($kabkota));
        $kabkota_temp = explode("|", $kabkota);
        $jenis = '';

        if ($kabkota_temp[0] == 'KOTA') {
            $jenis = 'Kota';
        } elseif($kabkota_temp[0] == 'KABUPATEN') {
            $jenis = 'Kab. ';
        }
        
        $kecamatans = DB::table('lokasi')->select('kecamatan')->groupBy('kecamatan')->where(array('jenis' => $jenis,'kabupaten'=>ucwords(strtolower($kabkota_temp[1]))))->get();
        $data = "<option selected value=''>-- Pilih Kecamatan --</option>";
        foreach ($kecamatans as $obj) {
            $data .= "<option value='". $obj->kecamatan . "'>". $obj->kecamatan . "</option>";
        }
        return $data;
    }

    public function getkecamatanbykabkota2($kabkota) {
        $kabkota = urldecode(html_entity_decode($kabkota));
        $kabkota_temp = explode(" ", $kabkota);
        $total = count($kabkota_temp);
        $jenis = '';

        if ($kabkota_temp[0] == 'KOTA') {
            $jenis = 'Kota';
        } elseif($kabkota_temp[0] == 'KABUPATEN') {
            $jenis = 'Kab. ';
        }

        $nama_kota = '';

        for ($i=1; $i < $total; $i++) { 
            $nama_kota .= $kabkota_temp[$i]." ";    
        }

        $nama_kota = substr($nama_kota,0, strlen($nama_kota) - 1);
        $kecamatans = DB::table('lokasi')
                        ->select('kecamatan')
                        ->groupBy('kecamatan')
                        ->where(array('jenis' => $jenis))
                        ->where('kabupaten','LIKE',ucwords('%'.strtolower($nama_kota).'%'))
                        ->get();

        $data = "<option selected value=''>-- Pilih Kecamatan --</option>";
        foreach ($kecamatans as $obj) {
            $data .= "<option value='". $obj->kecamatan . "'>". $obj->kecamatan . "</option>";
        }
        return $data;
    }

    public function getkelurahanbykecamatan($kecamatan) {
        $kecamatan = urldecode(html_entity_decode($kecamatan));
        $kelurahans = DB::table('lokasi')->select('kelurahan')->groupBy('kelurahan')->where('kecamatan', $kecamatan)->get();
        $data = "<option selected value=''>-- Pilih Kelurahan --</option>";
        foreach ($kelurahans as $obj) {
            $data .= "<option value='". $obj->kelurahan . "'>". $obj->kelurahan . "</option>";
        }
        return $data;
    }

    public function getallaset() {
        $assets = DB::table('aset')->get();
        $json = array();
        $i = 0;
        foreach ($assets as $aset){
            $json[$i]['id'] = $aset->id;
            $json[$i]['kodeaset'] = $aset->kode_aset;
            $json[$i]['nama_aset'] = htmlspecialchars(trim($aset->nama_asset_1));
            $json[$i]['businessarea'] = trim($aset->kode_bisnis);
            $json[$i]['kodelokasi'] = htmlspecialchars(trim($aset->lokasi_aset));
            $json[$i]['kabkota'] = str_replace("|", " ", $aset->kota);
            $json[$i]['provinsi'] = $aset->provinsi;
            $json[$i]['latitude'] = floatval($aset->koordinat_latitude);
            $json[$i]['longitude'] = floatval($aset->koordinat_longitude);
            $i++;
        }

        return json_encode($json);
    }

    public function searchallaset() {
        if(Request::ajax()) {
            $prov         = strtoupper(Input::get('prov'));
            $company      = Input::get('company');
            $businessarea = Input::get('businessarea');
            $kab = '';

            if (Input::get('kab') != '') {
              $kab  = explode('|',Input::get('kab'));
            } else {
                $kab = '';
            }

            if ($kab != "" ) {
                $kab = $kab[0]. " ".$kab[1];
            } else {
                $kab = '';
            }
            
            $assets = Asset::SearchAset(Input::get('q'))
                            ->SearchProvinsi(Input::get('prov'))
                            ->SearchKota($kab)
                            ->SearchKecamatan(Input::get('kec'))
                            ->SearchDesa(Input::get('kel'))
                            ->SearchATL(Input::get('atl'))
                            ->SearchIMV(Input::get('imv_psm'))
                            ->SearchPembangkit(Input::get('tipe_pembangkit'))
                            ->SearchCompany(Input::get('company'))->get();
            
            $json = array();
            $i = 0;
            foreach ($assets as $aset){
                $json[$i]['id'] = $aset->id;
                $json[$i]['kodeaset'] = $aset->kode_aset;
                $json[$i]['nama_aset'] = htmlspecialchars(trim($aset->nama_asset_1));
                $json[$i]['businessarea'] = trim($aset->kode_bisnis);
                $json[$i]['kodelokasi'] = htmlspecialchars(trim($aset->lokasi_aset));
                $json[$i]['kabkota'] = str_replace("|", " ", $aset->kota);
                $json[$i]['provinsi'] = $aset->provinsi;
                $json[$i]['latitude'] = floatval($aset->koordinat_latitude);
                $json[$i]['longitude'] = floatval($aset->koordinat_longitude);
                $i++;
            }

            return json_encode($json);
        }
    }

    public function getallpembanding() {
        $pembandings = DB::table('pembanding_tanah')->get();
        $json = array();
        $i = 0;
        foreach ($pembandings as $pembanding){
            $json[$i]['id'] = $pembanding->id;
            $json[$i]['name'] = $pembanding->name;
            $json[$i]['phone'] = $pembanding->phone;
            $json[$i]['address'] = $pembanding->address;
            $json[$i]['type'] = $pembanding->type;
            $json[$i]['latitude'] = floatval($pembanding->latitude);
            $json[$i]['longitude'] = floatval($pembanding->longitude);
            $i++;
        }

        return json_encode($json);
    }

    public function searchallpembanding() {
        if(Request::ajax()) {
            $pembandings = Pembanding::SearchName(Input::get('name'))
                                       ->SearchPhone(Input::get('phone'))
                                       /*->SearchAddress(Input::get('address'))*/
                                       ->SearchType(Input::get('type'))
                                       ->SearchOfferingJual(Input::get('offering_jual'))
                                       ->get();
            
            $json = array();
            $i = 0;
            foreach ($pembandings as $pembanding){
                $json[$i]['id'] = $pembanding->id;
                $json[$i]['name'] = $pembanding->name;
                $json[$i]['phone'] = $pembanding->phone;
                $json[$i]['address'] = $pembanding->address;
                $json[$i]['type'] = $pembanding->type;
                $json[$i]['latitude'] = floatval($pembanding->latitude);
                $json[$i]['longitude'] = floatval($pembanding->longitude);
                $i++;
            }
            return json_encode($json);
        }
    }
}