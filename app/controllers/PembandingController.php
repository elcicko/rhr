<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

use Illuminate\Database\Eloquent\ModelNotFoundException;

class PembandingController extends BaseController 
{
	public $layout 	= 'layouts.default';
	public $title  	= 'Data Pembanding';
	public $route	= 'pembanding';
	public $write;
	public $update;
	public $delete;
	public $session_user;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$akses = Session::get('akses');
			$user  = Session::get('user');
			$i = 0;
			foreach ($akses as $key=>$values) {
				foreach ($values as $val) {
					if ($val->route == $this->route) {
						$this->write  = $val->write;
						$this->update = $val->update;
						$this->delete = $val->delete;
						$i++;
					}
				}
			}
			$this->session_user = $user->username;
			if ($i == 0)
				return Redirect::to('aset')->with('revoke', 'ANDA TIDAK PUNYA AKSES MENUJU HALAMAN INI');
		} else {
			return Redirect::to('login');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url = 'pembanding';	
		$view = View::make('pembanding.index');
		$view->form_action = url('pembanding/search');

		$type_datas = array('Pembanding 1' => 'Pembanding 1','Pembanding 2' => 'Pembanding 2', 'Pembanding 3' => 'Pembanding 3');
		$types = array('Tanah' => 'Tanah','Bangunan' => 'Bangunan', 'Mesin' => 'Mesin');

		foreach ($type_datas as $key=>$val) {
			$type_data[''] = '-- Pilih Tipe Data --';
			$type_data[$key] = $val;
		}

		foreach ($types as $key=>$val) {
			$type[''] = '-- Pilih Jenis Properti --';
			$type[$key] = $val;
		}

		$view->type_data = $type_data;
		$view->type = $type;
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$this->layout->content = $view;
	}

	public function index_peta() {
		$this->layout->title = $this->title;
		$this->layout->url = 'pembanding';	
		$view = View::make('pembanding.index_peta');
		$view->form_action = url('pembanding/search');

		$type_datas = array('Pembanding 1' => 'Pembanding 1','Pembanding 2' => 'Pembanding 2', 'Pembanding 3' => 'Pembanding 3');
		$types = array('Tanah' => 'Tanah','Bangunan' => 'Bangunan', 'Mesin' => 'Mesin');

		foreach ($type_datas as $key=>$val) {
			$type_data[''] = '-- Pilih Tipe Data --';
			$type_data[$key] = $val;
		}

		foreach ($types as $key=>$val) {
			$type[''] = '-- Pilih Jenis Properti --';
			$type[$key] = $val;
		}

		$view->type_data = $type_data;
		$view->type = $type;
		$view->write = $this->write;
		$view->update = $this->update;
		$view->delete = $this->delete;
		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url = 'aset';	
		$view = View::make('pembanding.form');
		$view->form_action = url('pembanding/store');
		$view->action_title = 'Tambah Data Pembanding';
		$view->form_title = 'add';
		
		$type_datas = array('Pembanding 1' => 'Pembanding 1','Pembanding 2' => 'Pembanding 2', 'Pembanding 3' => 'Pembanding 3');
		$types 		= array('Tanah' => 'Tanah','Bangunan' => 'Bangunan', 'Mesin' => 'Mesin');

		$document_statuses = array('SHM'=>'SHM', 'HGB'=>'HGB',
								   'HPL'=>'HPL', 'HJB'=>'HJB',
								   'Girik'=>'Girik', 'HP'=>'HP',
								   'Lainnya'=>'Lainnya'
								  );

		$terms = array('Tunai' => 'Tunai', 'Tunai Bertahap' => 'Tunai Bertahap', 'KPR' => 'KPR');
		$sale_conditions = array('Normal' => 'Normal', 'Terpaksa' => 'Terpaksa', 'Butuh Cepat' => 'Butuh Cepat');

		$availabilities = array('Ada'=>'Ada','Tidak Ada','Tidak Ada');
		$market_conditions = array('Normal'=>'Normal','Tidak Normal','Tidak Normal');

		$positions = array('Tengah'=>'Tengah', 'Pojok'=>'Pojok',
						   'Tusuk Sate'=>'Tusuk Sate', 'Buntu'=>'Buntu'
							);

		$treads  = array('Persegi'=>'Persegi', 'Bujur Sangkar'=>'Bujur Sangkar',
						 'Trapesium'=>'Trapesium', 'Tidak Beraturan'=>'Tidak Beraturan'
						);

		$topography  = array('Datar'=>'Datar', 'Bergelombang'=>'Bergelombang',
							'Bukit'=>'Bukit', 'Curam'=>'Curam'
							 );

		$orientations = array('Barat'=>'Barat', 'Timur'=>'Timur',
							  'Utara'=>'Utara', 'Selatan'=>'Selatan'
							 );

		$zonings = array('Perumahan' => 'Perumahan', 'Ritel' => 'Ritel',
						 'Perkantoran' => 'Perkantoran', 'Industri' => 'Industri',
						 'Lainnya' => 'Lainnya'
						);

		$foundation = array('Tidak Ada' => 'Tidak Ada',
							'Lajur Batu Kali' => 'Lajur Batu Kali',
							'Beton Plat Setempat (telapak)' => 'Beton Plat Setempat (telapak)', 
							'Tiang Pancang Kayu' => 'Tiang Pancang Kayu',
							'Tiang Pancang Beton' => 'Tiang Pancang Beton'
							);

		$structure = array('Tidak Ada' => 'Tidak Ada',
							'Struktur Beton Cor 1 lantai' => 'Struktur Beton Cor 1 lantai',
							'Struktur Beton Cor 2 lantai' => 'Struktur Beton Cor 2 lantai', 
							'Struktur Baja' => 'Struktur Baja',
							'Struktur Beton' => 'Struktur Beton',
							'Komposit' => 'Komposit'
						  );

		$walls = array( 'Tidak ada'=>'Tidak ada',
						'Batu bata di plester dan di cat'=>'Batu bata di plester dan di cat',
						'Bata hebel di plester dan di cat'=>'Bata hebel di plester dan di cat',
						'Batako di plester dan di cat'=>'Batako di plester dan di cat',
						'Gipsum rangka kayu dicat'=>'Gipsum rangka kayu dicat',
						'Batu bata di plester'=>'Batu bata di plester',
						'Bata hebel di plester'=>'Bata hebel di plester',
						'Batako di plester'=>'Batako di plester',
						'Kawat besi'=>'Kawat besi',
						'Metal Zinkalum'=>'Metal Zinkalum',
						'Double metal zinkalum'=> 'Double metal zinkalum',
						'Kaca rangka Alumunium'=> 'Kaca rangka Alumunium',
						'Kaca Rangka Kayu'=> 'Kaca Rangka Kayu',
						'Dinding Geser'=> 'Dinding Geser'
					);

		$doors = array( "Tidak ada"=>'Tidak ada',
						"Panel Kaca Rayban rangka kayu"=>'Panel Kaca Rayban rangka kayu',
						"Panel Kaca rangka kayu"=>'Panel Kaca rangka kayu',
						"Panel kaca ice rangka kayu"=>'Panel kaca ice rangka kayu',
						"Pintu dan jendela dari kayu kelas 1"=>'Pintu dan jendela dari kayu kelas 1',
						"Pintu dan jendela dari kayu kelas 2"=>'Pintu dan jendela dari kayu kelas 2',
						"Pintu dan jendela dari kayu kelas 3"=>'Pintu dan jendela dari kayu kelas 3',
						"Pintu dan jendela dari triplek"=>'Pintu dan jendela dari triplek',
						"Kusen pintu dan jendela Aluminium "=>'Kusen pintu dan jendela Aluminium ',
						"Pintu dan jendela Besi "=>'Pintu dan jendela Besi',
						"Pintu dan Jendela Kaca Gravir"=>'Pintu dan Jendela Kaca Gravir'
					);

		$floors = array(
						"Tidak ada"=>"Tidak ada",
						"Beton Bertulang"=>"Beton Bertulang",
						"Beton Bertulang+epoxi"=>"Beton Bertulang+epoxi",
						"Keramik 20x20"=>"Keramik 20x20",
						"Keramik 30x30"=>"Keramik 30x30",
						"Keramik 40x40"=>"Keramik 40x40",
						"Granit"=>"Granit",
						"Marmer"=>"Marmer",
						"Parquete"=>"Parquete",
						"Teraso"=>"Teraso",
						"Semen/Plur"=>"Semen/Plur",
						"Semen + pvc vinyl"=>"Semen + pvc vinyl",
						"Semen/Plur+epoxi"=>"Semen/Plur+epoxi",
						"Homogenus Tile"=>"Homogenus Tile",
						"Papan Kayu"=>"Papan Kayu"
					);

		$skies = array(
						"Tidak ada"=>"Tidak ada",
						"Gipsum rangka hollow di cat"=>"Gipsum rangka hollow di cat",
						"Triplek Rangka Kayu di cat"=>"Triplek Rangka Kayu di cat",
						"Gipsum rangka kayu di cat"=>"Gipsum rangka kayu di cat",
						"Eternit"=>"Eternit",
						"Metal alumunium"=>"Metal alumunium",
						"Double metal zinkalum"=>"Double metal zinkalum",
						"Glasswool dilapis alumniumfoil"=>"Glasswool dilapis alumniumfoil"
					);

		$roof_truss  = array(
							"Tidak ada"=>'Tidak ada',
							"Kayu Kelas I"=>'Kayu Kelas I',
							"Kayu Kelas II"=>'Kayu Kelas II',
							"Baja Ringan"=>'Baja Ringan',
							"Baja"=>'Baja');
		$roof_cover = array(
							"Tidak ada"=>"Tidak ada",
							"Dak Beton"=>"Dak Beton",
							"Fibreglass"=>"Fibreglass",
							"Genteng Keramik"=>"Genteng Keramik",
							"Genteng Kodok"=>"Genteng Kodok",
							"GentengMetal"=>"GentengMetal",
							"Genteng Beton"=>"Genteng Beton",
							"Genteng Tegola"=>"Genteng Tegola",
							"Fiber Cement Slab Harflex"=>"Fiber Cement Slab Harflex",
							"Spandek"=>"Spandek",
							"Sirap"=>"Sirap",
							"Asbes"=>"Asbes"
						   );
		
		foreach ($type_datas as $key=>$val) {
			$type_data[''] = '-- Pilih Tipe Data --';
			$type_data[$key] = $val;
		}

		foreach ($types as $key=>$val) {
			$type[''] = '-- Pilih Jenis Properti --';
			$type[$key] = $val;
		}

		foreach ($document_statuses as $key=>$val) {
			$status_dokumen_tanah[''] = '-- Pilih Status Dokumen Tanah --';
			$status_dokumen_tanah[$key] = $val;
		}

		foreach ($terms as $key=>$val) {
			$term[''] = '-- Pilih Syarat Pembiayaan --';
			$term[$key] = $val;
		}

		foreach ($sale_conditions as $key=>$val) {
			$sales_condition[''] = '-- Pilih Kondisi Penjualan --';
			$sales_condition[$key] = $val;
		}

		foreach ($availabilities as $key=>$val) {
			$other_expenses[''] = '-- Pilih Opsi --';
			$other_expenses[$key] = $val;
		}

		foreach ($market_conditions as $key=>$val) {
			$market_condition[''] = '-- Pilih Kondisi Pasar --';
			$market_condition[$key] = $val;
		}

		foreach ($positions as $key=>$val) {
			$land_position[''] = '-- Pilih Posisi Tanah --';
			$land_position[$key] = $val;
		}

		foreach ($treads as $key=>$val) {
			$land_shape[''] = '-- Pilih Bentuk Tanah --';
			$land_shape[$key] = $val;
		}

		foreach ($topography as $key=>$val) {
			$land_topography[''] = '-- Pilih Kondisi Topografi --';
			$land_topography[$key] = $val;
		}

		foreach ($orientations as $key=>$val) {
			$land_orientation[''] = '-- Pilih Orientasi --';
			$land_orientation[$key] = $val;
		}

		foreach ($zonings as $key=>$val) {
			$land_zoning[''] = '-- Pilih Zoning --';
			$land_zoning[$key] = $val;
		}

		foreach ($foundation as $key=>$val) {
			$pondasi[''] = '-- Pilih Pondasi --';
			$pondasi[$key] = $val;
		}

		foreach ($structure as $key=>$val) {
			$struktur[''] = '-- Pilih Struktur --';
			$struktur[$key] = $val;
		}

		foreach ($walls as $key=>$val) {
			$dinding[''] = '-- Pilih Dinding --';
			$dinding[$key] = $val;
		}

		foreach ($floors as $key=>$val) {
			$lantai[''] = '-- Pilih Lantai --';
			$lantai[$key] = $val;
		}

		foreach ($doors as $key=>$val) {
			$pintu[''] = '-- Pilih Pintu --';
			$pintu[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$langit[''] = '-- Pilih Langit-langit --';
			$langit[$key] = $val;
		}

		foreach ($roof_truss as $key=>$val) {
			$rangka_atap[''] = '-- Pilih Rangkat Atap --';
			$rangka_atap[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$penutup_atap[''] = '-- Pilih Penutup Atap --';
			$penutup_atap[$key] = $val;
		}

		$view->type_data = $type_data;
		$view->type = $type;
		$view->title_particular  = $status_dokumen_tanah;
		$view->term  = $term;
		$view->sales_condition  = $sales_condition;
		$view->other_expenses  = $other_expenses;
		$view->market_condition  = $market_condition;
		$view->land_position  = $land_position;
		$view->land_shape  = $land_shape;
		$view->land_topography  = $land_topography;
		$view->land_orientation  = $land_orientation;
		$view->land_zoning  = $land_zoning;
		$view->pondasi  = $pondasi;
		$view->struktur  = $struktur;
		$view->lantai  = $lantai;
		$view->dinding  = $dinding;
		$view->pintu  = $pintu;
		$view->langit  = $langit;
		$view->rangka_atap  = $rangka_atap;
		$view->penutup_atap  = $penutup_atap;
		
		$this->layout->content = $view;
	}

	public function create2($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'pembanding';	
		$view = View::make('pembanding.form');
		$view->form_action = url('pembanding/store/'.$id.'');
		$view->action_title = 'Tambah Data Pembanding';
		$view->form_title = 'add';
		
		$type_datas = array('Pembanding 1' => 'Pembanding 1','Pembanding 2' => 'Pembanding 2', 'Pembanding 3' => 'Pembanding 3');
		$types 		= array('Tanah' => 'Tanah','Bangunan' => 'Bangunan', 'Mesin' => 'Mesin');

		$document_statuses = array('SHM'=>'SHM', 'HGB'=>'HGB',
								   'HPL'=>'HPL', 'HJB'=>'HJB',
								   'Girik'=>'Girik', 'HP'=>'HP',
								   'Lainnya'=>'Lainnya'
								  );

		$terms = array('Tunai' => 'Tunai', 'Tunai Bertahap' => 'Tunai Bertahap', 'KPR' => 'KPR');
		$sale_conditions = array('Normal' => 'Normal', 'Terpaksa' => 'Terpaksa', 'Butuh Cepat' => 'Butuh Cepat');

		$availabilities = array('Ada'=>'Ada','Tidak Ada','Tidak Ada');
		$market_conditions = array('Normal'=>'Normal','Tidak Normal','Tidak Normal');

		$positions = array('Tengah'=>'Tengah', 'Pojok'=>'Pojok',
						   'Tusuk Sate'=>'Tusuk Sate', 'Buntu'=>'Buntu'
							);

		$treads  = array('Persegi'=>'Persegi', 'Bujur Sangkar'=>'Bujur Sangkar',
						 'Trapesium'=>'Trapesium', 'Tidak Beraturan'=>'Tidak Beraturan'
						);

		$topography  = array('Datar'=>'Datar', 'Bergelombang'=>'Bergelombang',
							'Bukit'=>'Bukit', 'Curam'=>'Curam'
							 );

		$orientations = array('Barat'=>'Barat', 'Timur'=>'Timur',
							  'Utara'=>'Utara', 'Selatan'=>'Selatan'
							 );

		$zonings = array('Perumahan' => 'Perumahan', 'Ritel' => 'Ritel',
						 'Perkantoran' => 'Perkantoran', 'Industri' => 'Industri',
						 'Lainnya' => 'Lainnya'
						);

		$foundation = array('Tidak Ada' => 'Tidak Ada',
							'Lajur Batu Kali' => 'Lajur Batu Kali',
							'Beton Plat Setempat (telapak)' => 'Beton Plat Setempat (telapak)', 
							'Tiang Pancang Kayu' => 'Tiang Pancang Kayu',
							'Tiang Pancang Beton' => 'Tiang Pancang Beton'
							);

		$structure = array('Tidak Ada' => 'Tidak Ada',
							'Struktur Beton Cor 1 lantai' => 'Struktur Beton Cor 1 lantai',
							'Struktur Beton Cor 2 lantai' => 'Struktur Beton Cor 2 lantai', 
							'Struktur Baja' => 'Struktur Baja',
							'Struktur Beton' => 'Struktur Beton',
							'Komposit' => 'Komposit'
						  );

		$walls = array( 'Tidak ada'=>'Tidak ada',
						'Batu bata di plester dan di cat'=>'Batu bata di plester dan di cat',
						'Bata hebel di plester dan di cat'=>'Bata hebel di plester dan di cat',
						'Batako di plester dan di cat'=>'Batako di plester dan di cat',
						'Gipsum rangka kayu dicat'=>'Gipsum rangka kayu dicat',
						'Batu bata di plester'=>'Batu bata di plester',
						'Bata hebel di plester'=>'Bata hebel di plester',
						'Batako di plester'=>'Batako di plester',
						'Kawat besi'=>'Kawat besi',
						'Metal Zinkalum'=>'Metal Zinkalum',
						'Double metal zinkalum'=> 'Double metal zinkalum',
						'Kaca rangka Alumunium'=> 'Kaca rangka Alumunium',
						'Kaca Rangka Kayu'=> 'Kaca Rangka Kayu',
						'Dinding Geser'=> 'Dinding Geser'
					);

		$doors = array( "Tidak ada"=>'Tidak ada',
						"Panel Kaca Rayban rangka kayu"=>'Panel Kaca Rayban rangka kayu',
						"Panel Kaca rangka kayu"=>'Panel Kaca rangka kayu',
						"Panel kaca ice rangka kayu"=>'Panel kaca ice rangka kayu',
						"Pintu dan jendela dari kayu kelas 1"=>'Pintu dan jendela dari kayu kelas 1',
						"Pintu dan jendela dari kayu kelas 2"=>'Pintu dan jendela dari kayu kelas 2',
						"Pintu dan jendela dari kayu kelas 3"=>'Pintu dan jendela dari kayu kelas 3',
						"Pintu dan jendela dari triplek"=>'Pintu dan jendela dari triplek',
						"Kusen pintu dan jendela Aluminium "=>'Kusen pintu dan jendela Aluminium ',
						"Pintu dan jendela Besi "=>'Pintu dan jendela Besi',
						"Pintu dan Jendela Kaca Gravir"=>'Pintu dan Jendela Kaca Gravir'
					);

		$floors = array(
						"Tidak ada"=>"Tidak ada",
						"Beton Bertulang"=>"Beton Bertulang",
						"Beton Bertulang+epoxi"=>"Beton Bertulang+epoxi",
						"Keramik 20x20"=>"Keramik 20x20",
						"Keramik 30x30"=>"Keramik 30x30",
						"Keramik 40x40"=>"Keramik 40x40",
						"Granit"=>"Granit",
						"Marmer"=>"Marmer",
						"Parquete"=>"Parquete",
						"Teraso"=>"Teraso",
						"Semen/Plur"=>"Semen/Plur",
						"Semen + pvc vinyl"=>"Semen + pvc vinyl",
						"Semen/Plur+epoxi"=>"Semen/Plur+epoxi",
						"Homogenus Tile"=>"Homogenus Tile",
						"Papan Kayu"=>"Papan Kayu"
					);

		$skies = array(
						"Tidak ada"=>"Tidak ada",
						"Gipsum rangka hollow di cat"=>"Gipsum rangka hollow di cat",
						"Triplek Rangka Kayu di cat"=>"Triplek Rangka Kayu di cat",
						"Gipsum rangka kayu di cat"=>"Gipsum rangka kayu di cat",
						"Eternit"=>"Eternit",
						"Metal alumunium"=>"Metal alumunium",
						"Double metal zinkalum"=>"Double metal zinkalum",
						"Glasswool dilapis alumniumfoil"=>"Glasswool dilapis alumniumfoil"
					);

		$roof_truss  = array(
							"Tidak ada"=>'Tidak ada',
							"Kayu Kelas I"=>'Kayu Kelas I',
							"Kayu Kelas II"=>'Kayu Kelas II',
							"Baja Ringan"=>'Baja Ringan',
							"Baja"=>'Baja');
		$roof_cover = array(
							"Tidak ada"=>"Tidak ada",
							"Dak Beton"=>"Dak Beton",
							"Fibreglass"=>"Fibreglass",
							"Genteng Keramik"=>"Genteng Keramik",
							"Genteng Kodok"=>"Genteng Kodok",
							"GentengMetal"=>"GentengMetal",
							"Genteng Beton"=>"Genteng Beton",
							"Genteng Tegola"=>"Genteng Tegola",
							"Fiber Cement Slab Harflex"=>"Fiber Cement Slab Harflex",
							"Spandek"=>"Spandek",
							"Sirap"=>"Sirap",
							"Asbes"=>"Asbes"
						   );
		
		foreach ($type_datas as $key=>$val) {
			$type_data[''] = '-- Pilih Tipe Data --';
			$type_data[$key] = $val;
		}

		foreach ($types as $key=>$val) {
			$type[''] = '-- Pilih Jenis Properti --';
			$type[$key] = $val;
		}

		foreach ($document_statuses as $key=>$val) {
			$status_dokumen_tanah[''] = '-- Pilih Status Dokumen Tanah --';
			$status_dokumen_tanah[$key] = $val;
		}

		foreach ($terms as $key=>$val) {
			$term[''] = '-- Pilih Syarat Pembiayaan --';
			$term[$key] = $val;
		}

		foreach ($sale_conditions as $key=>$val) {
			$sales_condition[''] = '-- Pilih Kondisi Penjualan --';
			$sales_condition[$key] = $val;
		}

		foreach ($availabilities as $key=>$val) {
			$other_expenses[''] = '-- Pilih Opsi --';
			$other_expenses[$key] = $val;
		}

		foreach ($market_conditions as $key=>$val) {
			$market_condition[''] = '-- Pilih Kondisi Pasar --';
			$market_condition[$key] = $val;
		}

		foreach ($positions as $key=>$val) {
			$land_position[''] = '-- Pilih Posisi Tanah --';
			$land_position[$key] = $val;
		}

		foreach ($treads as $key=>$val) {
			$land_shape[''] = '-- Pilih Bentuk Tanah --';
			$land_shape[$key] = $val;
		}

		foreach ($topography as $key=>$val) {
			$land_topography[''] = '-- Pilih Kondisi Topografi --';
			$land_topography[$key] = $val;
		}

		foreach ($orientations as $key=>$val) {
			$land_orientation[''] = '-- Pilih Orientasi --';
			$land_orientation[$key] = $val;
		}

		foreach ($zonings as $key=>$val) {
			$land_zoning[''] = '-- Pilih Zoning --';
			$land_zoning[$key] = $val;
		}

		foreach ($foundation as $key=>$val) {
			$pondasi[''] = '-- Pilih Pondasi --';
			$pondasi[$key] = $val;
		}

		foreach ($structure as $key=>$val) {
			$struktur[''] = '-- Pilih Struktur --';
			$struktur[$key] = $val;
		}

		foreach ($walls as $key=>$val) {
			$dinding[''] = '-- Pilih Dinding --';
			$dinding[$key] = $val;
		}

		foreach ($floors as $key=>$val) {
			$lantai[''] = '-- Pilih Lantai --';
			$lantai[$key] = $val;
		}

		foreach ($doors as $key=>$val) {
			$pintu[''] = '-- Pilih Pintu --';
			$pintu[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$langit[''] = '-- Pilih Langit-langit --';
			$langit[$key] = $val;
		}

		foreach ($roof_truss as $key=>$val) {
			$rangka_atap[''] = '-- Pilih Rangkat Atap --';
			$rangka_atap[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$penutup_atap[''] = '-- Pilih Penutup Atap --';
			$penutup_atap[$key] = $val;
		}

		$view->type_data = $type_data;
		$view->type = $type;
		$view->title_particular  = $status_dokumen_tanah;
		$view->term  = $term;
		$view->sales_condition  = $sales_condition;
		$view->other_expenses  = $other_expenses;
		$view->market_condition  = $market_condition;
		$view->land_position  = $land_position;
		$view->land_shape  = $land_shape;
		$view->land_topography  = $land_topography;
		$view->land_orientation  = $land_orientation;
		$view->land_zoning  = $land_zoning;
		$view->pondasi  = $pondasi;
		$view->struktur  = $struktur;
		$view->lantai  = $lantai;
		$view->dinding  = $dinding;
		$view->pintu  = $pintu;
		$view->langit  = $langit;
		$view->rangka_atap  = $rangka_atap;
		$view->penutup_atap  = $penutup_atap;
		$view->id_asset = $id;

		$this->layout->content = $view;
		Session::flash('aset_id', $id);
	}

	public function store() {
		$pembanding = new Pembanding;
		$pembanding->id_asset			= Input::get('id_asset');
		$pembanding->name				= Input::get('name');
		$pembanding->phone				= Input::get('phone');
		$pembanding->address			= Input::get('address');
		$pembanding->type 				= Input::get('type');
		$pembanding->type_data			= Input::get('type_data');
		$pembanding->latitude			= Input::get('latitude');
		$pembanding->longitude			= Input::get('longitude');
		$pembanding->title_particular	= Input::get('title_particular');
		$pembanding->term				= Input::get('term');
		$pembanding->sales_condition	= Input::get('sales_condition');
		$pembanding->other_expenses		= Input::get('other_expenses');
		$pembanding->market_condition	= Input::get('market_condition');
		$pembanding->land_area			= Input::get('land_area');
		$pembanding->land_position		= Input::get('land_position');
		$pembanding->land_shape			= Input::get('land_shape');
		$pembanding->land_topography	= Input::get('land_topography');
		$pembanding->land_orientation	= Input::get('land_orientation');
		$pembanding->land_frontage		= Input::get('land_frontage');
		$pembanding->land_zoning		= Input::get('land_zoning');
		$pembanding->land_perkiraan		= Input::get('land_perkiraan');
		$pembanding->land_indikasi		= Input::get('land_indikasi');
		$pembanding->building_area		= Input::get('building_area');
		$pembanding->building_storey			= Input::get('building_storey');
		$pembanding->building_year_constructed	= Input::get('building_year_constructed');
		$pembanding->building_year_renovated	= Input::get('building_year_renovated');
		$pembanding->pondasi					= Input::get('pondasi');
		$pembanding->struktur					= Input::get('struktur');
		$pembanding->lantai						= Input::get('lantai');
		$pembanding->dinding					= Input::get('dinding');
		$pembanding->pintu						= Input::get('pintu');
		$pembanding->langit						= Input::get('langit');
		$pembanding->rangka_atap				= Input::get('rangka_atap');
		$pembanding->penutup_atap				= Input::get('penutup_atap');
		$pembanding->equipment_electricity		= Input::get('equipment_electricity');
		$pembanding->equipment_ac				= Input::get('equipment_ac');
		$pembanding->equipment_water			= Input::get('equipment_water');
		$pembanding->equipment_telephone		= Input::get('equipment_telephone');
		$pembanding->equipment_others			= Input::get('equipment_others');
		$pembanding->equipment_economic			= Input::get('equipment_economic');
		$pembanding->equipment_non				= Input::get('equipment_non');
		$pembanding->offering_year				= Input::get('offering_year');
		$pembanding->offering_transacted_price	= Input::get('offering_transacted_price');
		$pembanding->offering_highest			= Input::get('offering_highest');
		$pembanding->offering_discount			= Input::get('offering_discount');
		$pembanding->offering_indicative_land_building	= Input::get('offering_indicative_land_building');
		$pembanding->offering_indicative_building		= Input::get('offering_indicative_building');
		$pembanding->offering_indicative_land			= Input::get('offering_indicative_land');
		$pembanding->offering_indicative_land_qsm		= Input::get('offering_indicative_land_qsm');
		$pembanding->offering_jual		= Input::get('offering_jual');
		$pembanding->offering_sewa		= Input::get('offering_sewa');
		$pembanding->offering_gim		= Input::get('offering_gim');
		$pembanding->property			= Input::get('property');
		$pembanding->save();
		return Redirect::to('aset')->with('message', 'DATA PEMBANDING BERHASIL DISIMPAN');
	}

	public function detail($id)	{
		$this->layout->title = $this->title;
		$this->layout->url = 'pembanding';	
		$view = View::make('pembanding.detail');
		$view->action_title = 'Detail Data Pembanding';
		$pembanding = Pembanding::find($id);
		$view->pembanding = $pembanding;
		$this->layout->content = $view;
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url = 'pembanding';	
		$view = View::make('pembanding.form');
		$view->form_action = url('pembanding/update/'.$id.'');
		$view->action_title = 'Ubah Data Pembanding';
		$view->form_title = 'edit';
		
		$pembanding = Pembanding::find($id);
		
		$type_datas = array('Pembanding 1' => 'Pembanding 1','Pembanding 2' => 'Pembanding 2', 'Pembanding 3' => 'Pembanding 3');
		$types 		= array('Tanah' => 'Tanah','Bangunan' => 'Bangunan', 'Mesin' => 'Mesin');

		$document_statuses = array('SHM'=>'SHM',
								   'HGB'=>'HGB',
								   'HPL'=>'HPL',
								   'HJB'=>'HJB',
								   'Girik'=>'Girik',
								   'HP'=>'HP',
								   'Lainnya'=>'Lainnya'
								  );
		$terms = array('Tunai' => 'Tunai', 'Tunai Bertahap' => 'Tunai Bertahap', 'KPR' => 'KPR');
		$sale_conditions = array('Normal' => 'Normal', 'Terpaksa' => 'Terpaksa', 'Butuh Cepat' => 'Butuh Cepat');

		$availabilities = array('Ada'=>'Ada','Tidak Ada','Tidak Ada');
		$market_conditions = array('Normal'=>'Normal','Tidak Normal','Tidak Normal');

		$positions = array('Tengah'=>'Tengah',
							'Pojok'=>'Pojok',
							'Tusuk Sate'=>'Tusuk Sate',
							'Buntu'=>'Buntu'
							 );

		$treads  = array('Persegi'=>'Persegi',
						 'Bujur Sangkar'=>'Bujur Sangkar',
						 'Trapesium'=>'Trapesium',
						 'Tidak Beraturan'=>'Tidak Beraturan'
						);

		$topography  = array('Datar'=>'Datar',
							'Bergelombang'=>'Bergelombang',
							'Bukit'=>'Bukit',
							'Curam'=>'Curam'
							 );

		$orientations = array('Barat'=>'Barat',
							  'Timur'=>'Timur',
							  'Utara'=>'Utara',
							  'Selatan'=>'Selatan'
							 );

		$zonings = array('Perumahan' => 'Perumahan',
						 'Ritel' => 'Ritel',
						 'Perkantoran' => 'Perkantoran',
						 'Industri' => 'Industri',
						 'Lainnya' => 'Lainnya'
						);

		$foundation = array('Tidak Ada' => 'Tidak Ada',
							'Lajur Batu Kali' => 'Lajur Batu Kali',
							'Beton Plat Setempat (telapak)' => 'Beton Plat Setempat (telapak)', 
							'Tiang Pancang Kayu' => 'Tiang Pancang Kayu',
							'Tiang Pancang Beton' => 'Tiang Pancang Beton'
							);

		$structure = array('Tidak Ada' => 'Tidak Ada',
							'Struktur Beton Cor 1 lantai' => 'Struktur Beton Cor 1 lantai',
							'Struktur Beton Cor 2 lantai' => 'Struktur Beton Cor 2 lantai', 
							'Struktur Baja' => 'Struktur Baja',
							'Struktur Beton' => 'Struktur Beton',
							'Komposit' => 'Komposit'
						  );

		$walls = array( 'Tidak ada'=>'Tidak ada',
						'Batu bata di plester dan di cat'=>'Batu bata di plester dan di cat',
						'Bata hebel di plester dan di cat'=>'Bata hebel di plester dan di cat',
						'Batako di plester dan di cat'=>'Batako di plester dan di cat',
						'Gipsum rangka kayu dicat'=>'Gipsum rangka kayu dicat',
						'Batu bata di plester'=>'Batu bata di plester',
						'Bata hebel di plester'=>'Bata hebel di plester',
						'Batako di plester'=>'Batako di plester',
						'Kawat besi'=>'Kawat besi',
						'Metal Zinkalum'=>'Metal Zinkalum',
						'Double metal zinkalum'=> 'Double metal zinkalum',
						'Kaca rangka Alumunium'=> 'Kaca rangka Alumunium',
						'Kaca Rangka Kayu'=> 'Kaca Rangka Kayu',
						'Dinding Geser'=> 'Dinding Geser'
					);

		$doors = array( "Tidak ada"=>'Tidak ada',
						"Panel Kaca Rayban rangka kayu"=>'Panel Kaca Rayban rangka kayu',
						"Panel Kaca rangka kayu"=>'Panel Kaca rangka kayu',
						"Panel kaca ice rangka kayu"=>'Panel kaca ice rangka kayu',
						"Pintu dan jendela dari kayu kelas 1"=>'Pintu dan jendela dari kayu kelas 1',
						"Pintu dan jendela dari kayu kelas 2"=>'Pintu dan jendela dari kayu kelas 2',
						"Pintu dan jendela dari kayu kelas 3"=>'Pintu dan jendela dari kayu kelas 3',
						"Pintu dan jendela dari triplek"=>'Pintu dan jendela dari triplek',
						"Kusen pintu dan jendela Aluminium "=>'Kusen pintu dan jendela Aluminium ',
						"Pintu dan jendela Besi "=>'Pintu dan jendela Besi',
						"Pintu dan Jendela Kaca Gravir"=>'Pintu dan Jendela Kaca Gravir'
					);

		$floors = array(
						"Tidak ada"=>"Tidak ada",
						"Beton Bertulang"=>"Beton Bertulang",
						"Beton Bertulang+epoxi"=>"Beton Bertulang+epoxi",
						"Keramik 20x20"=>"Keramik 20x20",
						"Keramik 30x30"=>"Keramik 30x30",
						"Keramik 40x40"=>"Keramik 40x40",
						"Granit"=>"Granit",
						"Marmer"=>"Marmer",
						"Parquete"=>"Parquete",
						"Teraso"=>"Teraso",
						"Semen/Plur"=>"Semen/Plur",
						"Semen + pvc vinyl"=>"Semen + pvc vinyl",
						"Semen/Plur+epoxi"=>"Semen/Plur+epoxi",
						"Homogenus Tile"=>"Homogenus Tile",
						"Papan Kayu"=>"Papan Kayu"
					);

		$skies = array(
						"Tidak ada"=>"Tidak ada",
						"Gipsum rangka hollow di cat"=>"Gipsum rangka hollow di cat",
						"Triplek Rangka Kayu di cat"=>"Triplek Rangka Kayu di cat",
						"Gipsum rangka kayu di cat"=>"Gipsum rangka kayu di cat",
						"Eternit"=>"Eternit",
						"Metal alumunium"=>"Metal alumunium",
						"Double metal zinkalum"=>"Double metal zinkalum",
						"Glasswool dilapis alumniumfoil"=>"Glasswool dilapis alumniumfoil"
					);

		$roof_truss  = array(
							"Tidak ada"=>'Tidak ada',
							"Kayu Kelas I"=>'Kayu Kelas I',
							"Kayu Kelas II"=>'Kayu Kelas II',
							"Baja Ringan"=>'Baja Ringan',
							"Baja"=>'Baja');
		$roof_cover = array(
							"Tidak ada"=>"Tidak ada",
							"Dak Beton"=>"Dak Beton",
							"Fibreglass"=>"Fibreglass",
							"Genteng Keramik"=>"Genteng Keramik",
							"Genteng Kodok"=>"Genteng Kodok",
							"GentengMetal"=>"GentengMetal",
							"Genteng Beton"=>"Genteng Beton",
							"Genteng Tegola"=>"Genteng Tegola",
							"Fiber Cement Slab Harflex"=>"Fiber Cement Slab Harflex",
							"Spandek"=>"Spandek",
							"Sirap"=>"Sirap",
							"Asbes"=>"Asbes"
						   );
		
		foreach ($type_datas as $key=>$val) {
			$type_data[''] = '-- Pilih Tipe Data --';
			$type_data[$key] = $val;
		}

		foreach ($types as $key=>$val) {
			$type[''] = '-- Pilih Jenis Properti --';
			$type[$key] = $val;
		}

		foreach ($document_statuses as $key=>$val) {
			$status_dokumen_tanah[''] = '-- Pilih Status Dokumen Tanah --';
			$status_dokumen_tanah[$key] = $val;
		}

		foreach ($terms as $key=>$val) {
			$term[''] = '-- Pilih Syarat Pembiayaan --';
			$term[$key] = $val;
		}

		foreach ($sale_conditions as $key=>$val) {
			$sales_condition[''] = '-- Pilih Kondisi Penjualan --';
			$sales_condition[$key] = $val;
		}

		foreach ($availabilities as $key=>$val) {
			$other_expenses[''] = '-- Pilih Opsi --';
			$other_expenses[$key] = $val;
		}

		foreach ($market_conditions as $key=>$val) {
			$market_condition[''] = '-- Pilih Kondisi Pasar --';
			$market_condition[$key] = $val;
		}

		foreach ($positions as $key=>$val) {
			$land_position[''] = '-- Pilih Posisi Tanah --';
			$land_position[$key] = $val;
		}

		foreach ($treads as $key=>$val) {
			$land_shape[''] = '-- Pilih Bentuk Tanah --';
			$land_shape[$key] = $val;
		}

		foreach ($topography as $key=>$val) {
			$land_topography[''] = '-- Pilih Kondisi Topografi --';
			$land_topography[$key] = $val;
		}

		foreach ($orientations as $key=>$val) {
			$land_orientation[''] = '-- Pilih Orientasi --';
			$land_orientation[$key] = $val;
		}

		foreach ($zonings as $key=>$val) {
			$land_zoning[''] = '-- Pilih Zoning --';
			$land_zoning[$key] = $val;
		}

		foreach ($foundation as $key=>$val) {
			$pondasi[''] = '-- Pilih Pondasi --';
			$pondasi[$key] = $val;
		}

		foreach ($structure as $key=>$val) {
			$struktur[''] = '-- Pilih Struktur --';
			$struktur[$key] = $val;
		}

		foreach ($walls as $key=>$val) {
			$dinding[''] = '-- Pilih Dinding --';
			$dinding[$key] = $val;
		}

		foreach ($floors as $key=>$val) {
			$lantai[''] = '-- Pilih Lantai --';
			$lantai[$key] = $val;
		}

		foreach ($doors as $key=>$val) {
			$pintu[''] = '-- Pilih Pintu --';
			$pintu[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$langit[''] = '-- Pilih Langit-langit --';
			$langit[$key] = $val;
		}

		foreach ($roof_truss as $key=>$val) {
			$rangka_atap[''] = '-- Pilih Rangkat Atap --';
			$rangka_atap[$key] = $val;
		}

		foreach ($skies as $key=>$val) {
			$penutup_atap[''] = '-- Pilih Penutup Atap --';
			$penutup_atap[$key] = $val;
		}

		$view->type_data = $type_data;
		$view->type = $type;
		$view->title_particular  = $status_dokumen_tanah;
		$view->term  = $term;
		$view->sales_condition  = $sales_condition;
		$view->other_expenses  = $other_expenses;
		$view->market_condition  = $market_condition;
		$view->land_position  = $land_position;
		$view->land_shape  = $land_shape;
		$view->land_topography  = $land_topography;
		$view->land_orientation  = $land_orientation;
		$view->land_zoning  = $land_zoning;
		$view->pondasi  = $pondasi;
		$view->struktur  = $struktur;
		$view->lantai  = $lantai;
		$view->dinding  = $dinding;
		$view->pintu  = $pintu;
		$view->langit  = $langit;
		$view->rangka_atap  = $rangka_atap;
		$view->penutup_atap  = $penutup_atap;

		$view->pembanding = $pembanding;
		$this->layout->content = $view;
		Session::flash('aset_id', $id);
	}

	public function update($id) {
		$pembanding = Pembanding::find($id);
		$pembanding->id_asset			= Input::get('id_asset');
		$pembanding->name				= Input::get('name');
		$pembanding->phone				= Input::get('phone');
		$pembanding->address			= Input::get('address');
		$pembanding->type 				= Input::get('type');
		$pembanding->type_data			= Input::get('type_data');
		$pembanding->latitude			= Input::get('latitude');
		$pembanding->longitude			= Input::get('longitude');
		$pembanding->title_particular	= Input::get('title_particular');
		$pembanding->term				= Input::get('term');
		$pembanding->sales_condition	= Input::get('sales_condition');
		$pembanding->other_expenses		= Input::get('other_expenses');
		$pembanding->market_condition	= Input::get('market_condition');
		$pembanding->land_area			= Input::get('land_area');
		$pembanding->land_position		= Input::get('land_position');
		$pembanding->land_shape			= Input::get('land_shape');
		$pembanding->land_topography	= Input::get('land_topography');
		$pembanding->land_orientation	= Input::get('land_orientation');
		$pembanding->land_frontage		= Input::get('land_frontage');
		$pembanding->land_zoning		= Input::get('land_zoning');
		$pembanding->land_perkiraan		= Input::get('land_perkiraan');
		$pembanding->land_indikasi		= Input::get('land_indikasi');
		$pembanding->building_area		= Input::get('building_area');
		$pembanding->building_storey			= Input::get('building_storey');
		$pembanding->building_year_constructed	= Input::get('building_year_constructed');
		$pembanding->building_year_renovated	= Input::get('building_year_renovated');
		$pembanding->pondasi					= Input::get('pondasi');
		$pembanding->struktur					= Input::get('struktur');
		$pembanding->lantai						= Input::get('lantai');
		$pembanding->dinding					= Input::get('dinding');
		$pembanding->pintu						= Input::get('pintu');
		$pembanding->langit						= Input::get('langit');
		$pembanding->rangka_atap				= Input::get('rangka_atap');
		$pembanding->penutup_atap				= Input::get('penutup_atap');
		$pembanding->equipment_electricity		= Input::get('equipment_electricity');
		$pembanding->equipment_ac				= Input::get('equipment_ac');
		$pembanding->equipment_water			= Input::get('equipment_water');
		$pembanding->equipment_telephone		= Input::get('equipment_telephone');
		$pembanding->equipment_others			= Input::get('equipment_others');
		$pembanding->equipment_economic			= Input::get('equipment_economic');
		$pembanding->equipment_non				= Input::get('equipment_non');
		$pembanding->offering_year				= Input::get('offering_year');
		$pembanding->offering_transacted_price	= Input::get('offering_transacted_price');
		$pembanding->offering_highest			= Input::get('offering_highest');
		$pembanding->offering_discount			= Input::get('offering_discount');
		$pembanding->offering_indicative_land_building	= Input::get('offering_indicative_land_building');
		$pembanding->offering_indicative_building		= Input::get('offering_indicative_building');
		$pembanding->offering_indicative_land			= Input::get('offering_indicative_land');
		$pembanding->offering_indicative_land_qsm		= Input::get('offering_indicative_land_qsm');
		$pembanding->offering_jual		= Input::get('offering_jual');
		$pembanding->offering_sewa		= Input::get('offering_sewa');
		$pembanding->offering_gim		= Input::get('offering_gim');
		$pembanding->property			= Input::get('property');
		$pembanding->save();
		return Redirect::to('pembanding')->with('message', 'DATA PEMBANDING BERHASIL DIUBAH');
	}

	public function destroy($id) {
		$pembanding = Pembanding::find($id);
		$pembanding->delete();
		return Redirect::to('pembanding')->with('message', 'DATA PEMBANDING BERHASIL DIHAPUS');
	}
}