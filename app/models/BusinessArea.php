<?php

class BusinessArea extends Eloquent 
{
	protected $table = 'business_area';
	public $timestamps = FALSE;

	public function assetsurvey() {
		return $this->hasOne('AssetSurvey');
	}
}