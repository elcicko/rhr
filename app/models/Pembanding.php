<?php

class Pembanding extends Eloquent 
{
	
	protected $table = 'pembanding';
	public $timestamps = FALSE;
	
	public function scopeSearchName($query, $name) {
	  if ($name) return $query->where('name','LIKE', '%'.$name.'%');
	}

	public function scopeSearchPhone($query, $phone) {
	  if ($phone) return $query->where('phone','LIKE', '%'.$phone.'%');
	}

	public function scopeSearchAddress($query, $address) {
	  if ($address) return $query->where('address','LIKE', '%'.$address.'%');
	}

	public function scopeSearchType($query, $type) {
	  if ($type) return $query->where('type', $type);
	}

	public function scopeSearchTypeData($query, $type_data) {
	  if ($type_data) return $query->where('type_data', $type_data);
	}

	public function scopeSearchOfferingJual($query, $offering_jual) {
	  if ($offering_jual) return $query->where('offering_jual','LIKE', '%'.$offering_jual.'%');
	}

	public function scopeSearchOfferingTransaction($query, $offering_transacted_price) {
	  if ($offering_transacted_price) return $query->where('offering_transacted_price','LIKE', '%'.$offering_transacted_price.'%');
	}
}