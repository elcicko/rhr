<?php

class Kontrak extends Eloquent 
{
	protected $table = 'kontrak';
	public $timestamps = FALSE;
	
	public function scopeSearchNomorKontrak($query, $nomor_kontrak) {
	  if ($nomor_kontrak) return $query->where('nomor_kontrak','LIKE', '%'.$nomor_kontrak.'%');
	}

	public function scopeSearchTanggalKontrak($query, $tanggal_kontrak) {
	  if ($tanggal_kontrak) return $query->where('tanggal_kontrak', $tanggal_kontrak);
	}

	public function scopeSearchAlamat($query, $lokasi) {
		if ($lokasi) return $query->where('lokasi','LIKE', '%'.$lokasi.'%');
	}

	public function scopeSearchJenisAset($query, $jenis_asset) {
		if ($jenis_asset) return $query->where('jenis_asset', $jenis_asset);
	}

	public function scopeSearchTujuanPenilaian($query, $tujuan_penilaian) {
		if ($tujuan_penilaian) return $query->where('tujuan_penilaian','LIKE', '%'.$tujuan_penilaian.'%');
	}

	public function scopeSearchFee($query, $fee) {
		if ($fee) return $query->where('fee','LIKE', '%'.$fee.'%');
	}

	public function scopeSearchPimpinanProyek($query, $pimpinan_project) {
		if ($pimpinan_project) return $query->where('pimpinan_proyek', $pimpinan_project);
	}

	public function scopeSearchJakarta($query) {
		return $query->where('lokasi', 'Jakarta');
	}

	public function scopeSearchSurabaya($query) {
		return $query->where('lokasi', 'Surabaya');
	}

	public function scopeSearchMedan($query) {
		return $query->where('lokasi', 'Medan');
	}

	public function scopeSearchBali($query) {
		return $query->where('lokasi', 'Bali');
	}

	public function scopeSearchMakassar($query) {
		return $query->where('lokasi', 'Makassar');
	}

	public function scopeSearchSolo($query) {
		return $query->where('lokasi', 'Solo');
	}
}