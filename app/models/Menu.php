<?php

class Menu extends Eloquent 
{
	protected $table = 'menu';
	public $timestamps = FALSE;
	
	public function scopeSearchNameRoute($query, $q) {
	  if ($q) return $query->where('menu_name','LIKE', '%'.$q.'%')->orwhere('route','LIKE', '%'.$q.'%');
	}
}