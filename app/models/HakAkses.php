<?php

class HakAkses extends Eloquent
{

	protected $table = 'akses_menu';
	public $timestamps = FALSE;
	
	public function menu() {
		return $this->belongsTo('Menu');
	}

	public function level() {
		return $this->belongsTo('Level');
	}

	public function scopeSearchMenu($query, $menu) {
		if ($menu) return $query->where('menu_id', $menu);
	}

	public function scopeSearchLevel($query, $level) {
		if ($level) return $query->where('level_id', $level);
	}

}
