<?php

class AssetSurvey extends Eloquent 
{
	protected $table = 'asset_survey';

	public function businessarea() {
		return $this->belongsTo('BusinessArea', 'business_area_id');
	}

	public function asset() {
		return $this->belongsTo('Asset', 'asset_id');
	}

	public function scopeSearchNamaAndNomorAset($query, $q) {
	  if ($q) return $query->where('nama_aset','LIKE', '%'.$q.'%')->orWhere('nomor_aset','LIKE', '%'.$q.'%');
	}
}