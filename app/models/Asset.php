<?php

class Asset extends Eloquent 
{
	protected $table = 'aset';
	public $timestamps = FALSE;
	
	public function scopeSearchAset($query, $q) {
	  if ($q) return $query->where('id','LIKE', '%'.$q.'%')->orWhere('nama_asset_1','LIKE', '%'.$q.'%');
	}

	public function scopeSearchAlamat($query, $alamat) {
	  if ($alamat) return $query->where('alamat','LIKE', '%'.$alamat.'%');
	}

	public function scopeSearchLuas($query, $luas) {
	  if ($luas) return $query->where('luas','LIKE', '%'.$luas.'%');
	}

	/*public function scopeSearchSatuan($query, $satuan) {
	  if ($jenis_asset) return $query->where('satuan', $jenis_asset);
	}*/

	public function scopeSearchPIC($query, $pic) {
	  if ($pic) return $query->where('pic', $pic);
	}


	public function scopeSearchJenisAset($query, $jenis_asset) {
	  if ($jenis_asset) return $query->where('jenis_asset', $jenis_asset);
	}

	public function scopeSearchProvinsi($query, $provinsi) {
	  if ($provinsi) return $query->where('provinsi', $provinsi);
	}

	public function scopeSearchKota($query, $kabkota) {
	  if ($kabkota) return $query->where('kota', $kabkota);
	}

	public function scopeSearchKecamatan($query, $kecamatan) {
	  if ($kecamatan) return $query->where('kecamatan', $kecamatan);
	}

	public function scopeSearchDesa($query, $desa) {
	  if ($desa) return $query->where('desa', $desa);
	}

	public function scopeSearchATL($query, $atl) {
	  if ($atl) return $query->where('atl', $atl);
	}

	public function scopeSearchPembangkit($query, $jenis_pembangkit) {
	  if ($jenis_pembangkit) return $query->where('jenis_pembangkit', $jenis_pembangkit);
	}

	public function scopeSearchIMV($query, $imv_psm) {
	  if ($imv_psm) return $query->where('imv_psm','LIKE', '%'.$imv_psm.'%');
	}

	public function scopeSearchCompany($query, $company) {
	  if ($company) return $query->where('kode_company', $company);
	}

	public function scopeSearchBusinessArea($query, $businessarea) {
		if ($businessarea) return $query->where('kode_bisnis', $businessarea);
	}

	public function scopeSearchNomorKontrak($query, $nomor_kontrak) {
		if ($nomor_kontrak) return $query->where('nomor_kontrak', $nomor_kontrak);
	}

}