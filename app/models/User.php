<?php

class User extends \Eloquent {

	protected $table = 'user';
	public $timestamps = FALSE;

	public function level() {
		return $this->belongsTo('Level', 'level_id');
	}

	public function scopeSearchUsername($query, $username) {
	  if ($username) return $query->where('username','LIKE', '%'.$username.'%');
	}

	public function scopeSearchLevel($query, $level) {
	  if ($level) return $query->where('level_id', $level);
	}

	public function scopeSearchNotAdmin($query) {
	 	return $query->where('level_id','!=', 4);
	}
}