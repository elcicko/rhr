<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'LoginController@index');
Route::get('login', 'LoginController@index');
Route::post('validate', 'LoginController@validate');
Route::get('logout', 'LoginController@logout');

Route::get('getkabkota/{id}', 'AjaxController@getkabkota');
Route::get('getkabkotabyprov/{id}', 'AjaxController@getkabkotabyprov');
Route::get('getkecamatanbykabkota/{id}', 'AjaxController@getkecamatanbykabkota');
Route::get('getkecamatanbykabkota2/{id}', 'AjaxController@getkecamatanbykabkota2');
Route::get('getkelurahanbykecamatan/{id}', 'AjaxController@getkelurahanbykecamatan');
Route::get('getaset', 'AjaxController@getallaset');
Route::post('searchallaset', 'AjaxController@searchallaset');
Route::get('getallpembanding', 'AjaxController@getallpembanding');
Route::post('searchallpembanding', 'AjaxController@searchallpembanding');

Route::post('validate', 'LoginController@validate');
Route::get('dashboard', 'BackendController@index');

// Aset
Route::get('aset', 'AsetController@index');
Route::get('aset/index_peta', 'AsetController@index_peta');
Route::get('aset/detail/{id}', 'AsetController@detail');
Route::get('searchaset', 'AsetController@searchaset');
Route::post('aset/search', 'AsetController@search');
Route::get('aset/search', 'AsetController@search');
Route::get('aset/destroy/{id}', 'AsetController@destroy');
Route::get('aset/tanah/{id}', 'AsetController@tanah');
Route::get('aset/bangunan/{id}', 'AsetController@bangunan');
Route::get('aset/edit_tanah/{id}', 'AsetController@edit_tanah');
Route::post('aset/update_tanah/{id}', 'AsetController@update_tanah');
Route::get('aset/edit_bangunan/{id}', 'AsetController@edit_bangunan');
Route::post('aset/update_bangunan/{id}', 'AsetController@update_bangunan');
Route::get('aset/cari_pembanding/{asset_id}/{tipe_data}', 'AsetController@cari_pembanding');
Route::post('aset/update_pembanding/{id}', 'AsetController@update_pembanding');
Route::get('aset/create_asetkontrak/{lokasi}/{nomor_kontrak}', 'AsetController@create_asetkontrak');
Route::post('aset/store_asetkontrak/{lokasi}', 'AsetController@store_asetkontrak');
Route::get('aset/edit_asetkontrak/{lokasi}/{nomor_kontrak}', 'AsetController@edit_asetkontrak');
Route::post('aset/update_asetkontrak/{lokasi}', 'AsetController@update_asetkontrak');

// Pembanding
Route::get('pembanding', 'PembandingController@index');
Route::get('pembanding/index_peta', 'PembandingController@index_peta');
Route::get('pembanding/detail/{id}', 'PembandingController@detail');
Route::get('searchPembanding', 'PembandingController@searchPembanding');
Route::get('pembanding/create', 'PembandingController@create');
Route::get('pembanding/create/{id}', 'PembandingController@create2');
Route::post('pembanding/store', 'PembandingController@store');
Route::post('pembanding/search', 'PembandingController@search');
Route::get('pembanding/search', 'PembandingController@search');
Route::get('pembanding/edit/{id}', 'PembandingController@edit');
Route::post('pembanding/update/{id}', 'PembandingController@update');
Route::get('pembanding/destroy/{id}', 'PembandingController@destroy');
Route::get('pembanding/survey/{id}', 'PembandingController@survey');
Route::post('pembanding/process_survey/{id}', 'PembandingController@process_survey');
Route::get('pembanding/edit_survey/{id}', 'PembandingController@edit_survey');
Route::post('pembanding/update_survey/{asset_id}/{id}', 'PembandingController@update_survey');

// Kontrak Jakarta
Route::get('kontrak/jakarta', 'KontrakController@jakarta');
Route::get('kontrak/jakarta/detail/{id}', 'KontrakController@detail_jakarta');
Route::get('kontrak/jakarta/create', 'KontrakController@create_jakarta');
Route::post('kontrak/jakarta/store', 'KontrakController@store_jakarta');
Route::get('kontrak/jakarta/edit/{id}', 'KontrakController@edit_jakarta');
Route::post('kontrak/jakarta/update/{id}', 'KontrakController@update_jakarta');
Route::get('kontrak/jakarta/destroy/{id}', 'KontrakController@destroy_jakarta');

Route::get('kontrak/surabaya', 'KontrakController@surabaya');
Route::get('kontrak/surabaya/detail/{id}', 'KontrakController@detail_surabaya');
Route::get('kontrak/surabaya/create', 'KontrakController@create_surabaya');
Route::post('kontrak/surabaya/store', 'KontrakController@store_surabaya');
Route::get('kontrak/surabaya/edit/{id}', 'KontrakController@edit_surabaya');
Route::post('kontrak/surabaya/update/{id}', 'KontrakController@update_surabaya');
Route::get('kontrak/surabaya/destroy/{id}', 'KontrakController@destroy_surabaya');

Route::get('kontrak/medan', 'KontrakController@medan');
Route::get('kontrak/medan/detail/{id}', 'KontrakController@detail_medan');
Route::get('kontrak/medan/create', 'KontrakController@create_medan');
Route::post('kontrak/medan/store', 'KontrakController@store_medan');
Route::get('kontrak/medan/edit/{id}', 'KontrakController@edit_medan');
Route::post('kontrak/medan/update/{id}', 'KontrakController@update_medan');
Route::get('kontrak/medan/destroy/{id}', 'KontrakController@destroy_medan');

Route::get('kontrak/bali', 'KontrakController@bali');
Route::get('kontrak/bali/detail/{id}', 'KontrakController@detail_bali');
Route::get('kontrak/bali/create', 'KontrakController@create_bali');
Route::post('kontrak/bali/store', 'KontrakController@store_bali');
Route::get('kontrak/bali/edit/{id}', 'KontrakController@edit_bali');
Route::post('kontrak/bali/update/{id}', 'KontrakController@update_bali');
Route::get('kontrak/bali/destroy/{id}', 'KontrakController@destroy_bali');

Route::get('kontrak/makassar', 'KontrakController@makassar');
Route::get('kontrak/makassar/detail/{id}', 'KontrakController@detail_makassar');
Route::get('kontrak/makassar/create', 'KontrakController@create_makassar');
Route::post('kontrak/makassar/store', 'KontrakController@store_makassar');
Route::get('kontrak/makassar/edit/{id}', 'KontrakController@edit_makassar');
Route::post('kontrak/makassar/update/{id}', 'KontrakController@update_makassar');
Route::get('kontrak/makassar/destroy/{id}', 'KontrakController@destroy_makassar');

Route::get('kontrak/solo', 'KontrakController@solo');
Route::get('kontrak/solo/detail/{id}', 'KontrakController@detail_solo');
Route::get('kontrak/solo/create', 'KontrakController@create_solo');
Route::post('kontrak/solo/store', 'KontrakController@store_solo');
Route::get('kontrak/solo/edit/{id}', 'KontrakController@edit_solo');
Route::post('kontrak/solo/update/{id}', 'KontrakController@update_solo');
Route::get('kontrak/solo/destroy/{id}', 'KontrakController@destroy_solo');

Route::get('user', 'UserController@index');
Route::get('user/create', 'UserController@create');
Route::get('user/edit/{id}', 'UserController@edit');
Route::post('user/update/{id}', 'UserController@update');
Route::post('user/store', 'UserController@store');
Route::get('user/destroy/{id}', 'UserController@destroy');

// Menu
Route::get('menu', 'MenuController@index');
Route::get('menu/create', 'MenuController@create');
Route::post('menu/store', 'MenuController@store');
Route::get('menu/edit/{id}', 'MenuController@edit');
Route::get('menu/destroy/{id}', 'MenuController@destroy');
Route::post('menu/update/{id}', 'MenuController@update');

// Import Data
Route::get('import', 'ImportController@index');
Route::post('import/import_process', 'MenuController@import_process');

Route::get('hakakses', 'HakAksesController@index');
Route::get('hakakses/create', 'HakAksesController@create');
Route::post('hakakses/store', 'HakAksesController@store');
Route::post('hakakses/search', 'HakAksesController@search');
Route::get('hakakses/edit/{id}', 'HakAksesController@edit');
Route::post('hakakses/update/{id}', 'HakAksesController@update');
Route::get('hakakses/destroy/{id}', 'HakAksesController@destroy');

// Datatable
Route::get('datatable/hakakses/{update}/{delete}', array('as'=>'datatable.hakakses', 'uses'=>'DatatableController@getHakAkses'));
Route::get('datatable/menu/{update}/{delete}', array('as'=>'datatable.menu', 'uses'=>'DatatableController@getMenu'));
Route::get('datatable/user/{update}/{delete}', array('as'=>'datatable.user', 'uses'=>'DatatableController@getUser'));
Route::get('datatable/aset/{session_user}/{update}/{delete}', array('as'=>'datatable.aset', 'uses'=>'DatatableController@getAset'));
Route::get('datatable/pembanding/{update}/{delete}', array('as'=>'datatable.pembanding', 'uses'=>'DatatableController@getPembanding'));

Route::get('datatable/kontrak_jakarta/{session_user}', array('as'=>'datatable.kontrak_jakarta', 'uses'=>'DatatableController@getKontrakJakarta'));
Route::get('datatable/kontrak_surabaya/{session_user}', array('as'=>'datatable.kontrak_surabaya', 'uses'=>'DatatableController@getKontrakSurabaya'));
Route::get('datatable/kontrak_medan/{session_user}', array('as'=>'datatable.kontrak_medan', 'uses'=>'DatatableController@getKontrakMedan'));
Route::get('datatable/kontrak_bali/{session_user}', array('as'=>'datatable.kontrak_bali', 'uses'=>'DatatableController@getKontrakBali'));
Route::get('datatable/kontrak_makassar/{session_user}', array('as'=>'datatable.kontrak_makassar', 'uses'=>'DatatableController@getKontrakMakassar'));
Route::get('datatable/kontrak_solo/{session_user}', array('as'=>'datatable.kontrak_solo', 'uses'=>'DatatableController@getKontrakSolo'));


Route::get('datatable/asetkontrak/{session_user}/{update}/{delete}/{nomor_kontrak}', array('as'=>'datatable.asetkontrak', 'uses'=>'DatatableController@getAsetKontrak'));
